<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Debugger</name>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="311"/>
        <source>Debugger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="314"/>
        <source>Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="315"/>
        <source>Next Instruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="316"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="319"/>
        <source>Disassembler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="321"/>
        <location filename="../source/ui/debugger_ui.cpp" line="331"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="323"/>
        <source>Instruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="325"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="328"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="329"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="332"/>
        <source>Memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="334"/>
        <location filename="../source/ui/debugger_ui.cpp" line="339"/>
        <location filename="../source/ui/debugger_ui.cpp" line="343"/>
        <location filename="../source/ui/debugger_ui.cpp" line="346"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="337"/>
        <source>CPU Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="340"/>
        <source>Other Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="341"/>
        <source>Audio Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="344"/>
        <source>Registers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="347"/>
        <source>Video Registers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="350"/>
        <source>Break Point Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="351"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="352"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="353"/>
        <source>Break Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="356"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DebuggerWindow</name>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="562"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/debugger_ui.cpp" line="567"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameBoyWindow</name>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="506"/>
        <source>Open ROM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="508"/>
        <source>ROM Files (*.gb *.gbc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="378"/>
        <location filename="../source/ui/gameboy_ui.cpp" line="469"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="378"/>
        <source>Failed to start the emulator timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="456"/>
        <source>Show gamepad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="469"/>
        <source>Could not load ROM file : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="525"/>
        <source>Save State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="527"/>
        <location filename="../source/ui/gameboy_ui.cpp" line="542"/>
        <source>State Files (*.sav)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="540"/>
        <source>Load State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="555"/>
        <source>About GameBoy Emulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="556"/>
        <source>GameBoy Emulator made by mhouppin &amp; fgalaup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="562"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gameboy</name>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="266"/>
        <source>Gameboy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="267"/>
        <location filename="../source/ui/gameboy_ui.cpp" line="268"/>
        <source>No game loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="269"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="270"/>
        <source>Debugger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="271"/>
        <source>Hide Gamepad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="273"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="274"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="275"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="276"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="277"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="278"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="279"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="280"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="281"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="283"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="284"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="285"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="286"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="287"/>
        <source>Emulation Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="289"/>
        <source>Open rom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="290"/>
        <source>Load state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="291"/>
        <source>Save state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="292"/>
        <source>Save game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="293"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="294"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="295"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="296"/>
        <source>0.5x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="297"/>
        <source>1x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="298"/>
        <source>2x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="299"/>
        <source>4x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="300"/>
        <source>GameBoy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/ui/gameboy_ui.cpp" line="301"/>
        <source>GameBoy Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
