#pragma once

#include "gameboy.h"
#include "memory.h"

class Event
{
  public:
    enum GB_Key
    {
        GB_KEY_SELECT,
        GB_KEY_START,
        GB_KEY_A,
        GB_KEY_B,
        GB_KEY_UP,
        GB_KEY_DOWN,
        GB_KEY_RIGHT,
        GB_KEY_LEFT,
        GB_KEY_COUNT,
        GB_KEY_NONE
    };

  private:
    Gameboy *gb;
    MemoryMap *map;
    bool keyStatus[GB_KEY_COUNT] = {false};
    bool buttonStatus[GB_KEY_COUNT] = {false};

  public:
    Event(Gameboy *gb, MemoryMap *map) : gb(gb), map(map){};
    void set_key_state(GB_Key key, bool state);
    void set_button_state(GB_Key button, bool state);
    void check_normal(void);
    void check_halted(void);
};