#pragma once
#include "types.h"

enum ScreenStatus
{
    OAM_READ,
    OAM_VRAM_READ,
    HZ_BLANKING
};

class Gameboy;
class MemoryMap;

enum TileType
{
    OBJECT,
    WINDOW,
    BACKGROUND,
};

struct Tile
{
    Reg8 y;
    Reg8 x;
    Reg8 code;
    Reg8 attributes;
    TileType type;
    unsigned prior;
    unsigned prior2;

    bool operator<(const Tile &t) const
    {
        return prior != t.prior     ? prior < t.prior
               : prior2 != t.prior2 ? prior2 < t.prior2
                                    : type < t.type;
    }
};

struct ScreenState
{
    Cycle cycles;
    ScreenStatus status;
    Reg8 curLine;
    Reg8 quantity;
    Reg8 blockIdx;
    Tile tileBuffer[104];
    bool objectsLoaded;
    bool updateImage;
};

class Screen
{
  public:
    Screen(Gameboy *_gb, MemoryMap *_map, int32_t *_winPixels);
    Screen(const Screen &other) = delete;
    Screen &operator=(const Screen &other) = delete;

    void reset(void);
    void load_state(const ScreenState &state);
    void save_state(ScreenState &state) const;
    void update_pixel_ptr(int32_t *_winPixels) { winPixels = _winPixels; }
    void update(Cycle elapsed);
    void load_oam(void);
    void draw_line(void);
    Reg8 load_bg_window_tiles(void);
    void render(void);
    bool can_update_image(void) const;
    void image_updated(void);
    void hblank_dma_init(void);
    void hblank_dma_stop(void);

  private:
    int32_t *winPixels;
    Cycle cycles;
    ScreenStatus status;
    Reg8 curLine;
    Reg8 quantity;
    Reg8 blockIdx;
    Tile tileBuffer[104];
    bool objectsLoaded;
    Gameboy *gb;
    MemoryMap *map;
    bool updateImage;

    void draw_line_dmg(Reg8 tileCount, int32_t *linePixels);
    void draw_line_cgb(Reg8 tileCount, int32_t *linePixels);
    void hblank_dma_tick(void);
};
