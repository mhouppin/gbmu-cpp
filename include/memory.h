#pragma once

#include "types.h"
#include <vector>

enum : Reg16
{
    P1_ADDRESS = 0xFF00u,
    SB_ADDRESS = 0xFF01u,
    SC_ADDRESS = 0xFF02u,
    DIV_ADDRESS = 0xFF04u,
    TIMA_ADDRESS = 0xFF05u,
    TMA_ADDRESS = 0xFF06u,
    TAC_ADDRESS = 0xFF07u,
    IF_ADDRESS = 0xFF0Fu,
    NR10_ADDRESS = 0xFF10u,
    NR11_ADDRESS = 0xFF11u,
    NR12_ADDRESS = 0xFF12u,
    NR13_ADDRESS = 0xFF13u,
    NR14_ADDRESS = 0xFF14u,
    NR21_ADDRESS = 0xFF16u,
    NR22_ADDRESS = 0xFF17u,
    NR23_ADDRESS = 0xFF18u,
    NR24_ADDRESS = 0xFF19u,
    NR30_ADDRESS = 0xFF1Au,
    NR31_ADDRESS = 0xFF1Bu,
    NR32_ADDRESS = 0xFF1Cu,
    NR33_ADDRESS = 0xFF1Du,
    NR34_ADDRESS = 0xFF1Eu,
    NR41_ADDRESS = 0xFF20u,
    NR42_ADDRESS = 0xFF21u,
    NR43_ADDRESS = 0xFF22u,
    NR44_ADDRESS = 0xFF23u,
    NR50_ADDRESS = 0xFF24u,
    NR51_ADDRESS = 0xFF25u,
    NR52_ADDRESS = 0xFF26u,
    LCDC_ADDRESS = 0xFF40u,
    STAT_ADDRESS = 0xFF41u,
    SCY_ADDRESS = 0xFF42u,
    SCX_ADDRESS = 0xFF43u,
    LY_ADDRESS = 0xFF44u,
    LYC_ADDRESS = 0xFF45u,
    DMA_ADDRESS = 0xFF46u,
    BGP_ADDRESS = 0xFF47u,
    OBP0_ADDRESS = 0xFF48u,
    OBP1_ADDRESS = 0xFF49u,
    WY_ADDRESS = 0xFF4Au,
    WX_ADDRESS = 0xFF4Bu,
    KEY1_ADDRESS = 0xFF4Du,
    VBK_ADDRESS = 0xFF4Fu,
    HDMA1_ADDRESS = 0xFF51u,
    HDMA2_ADDRESS = 0xFF52u,
    HDMA3_ADDRESS = 0xFF53u,
    HDMA4_ADDRESS = 0xFF54u,
    HDMA5_ADDRESS = 0xFF55u,
    RP_ADDRESS = 0xFF56u,
    BCPS_ADDRESS = 0xFF68u,
    BCPD_ADDRESS = 0xFF69u,
    OCPS_ADDRESS = 0xFF6Au,
    OCPD_ADDRESS = 0xFF6Bu,
    SVBK_ADDRESS = 0xFF70u,
    IE_ADDRESS = 0xFFFFu,
};

class Gameboy;

struct MemoryState
{
    std::vector<Reg8> ram;
    Reg8 colorPalettes[128];
    Reg8 oamData[160];
    Reg8 ioRegisters[256];
    uint32_t offsetTable[16];
    Reg8 romRegisters[8];
    bool biosRunning;
    bool IME;
};

class MemoryMap
{
  public:
    MemoryMap(Gameboy* _gb);
    MemoryMap(const MemoryMap& other) = delete;
    MemoryMap& operator=(const MemoryMap& other) = delete;
    ~MemoryMap(void);

    void load_cartridge(const std::vector<char>& content);
    void reset(void);

    void load_state(const MemoryState& state);
    void save_state(MemoryState& state) const;

    Reg8* external_ram_begin() const { return fullBlock + offsetExternalRAM; }
    Reg8* external_ram_end() const { return fullBlock + offsetInternalRAM; }

    bool bios_is_running() const { return biosRunning; }

    void store(Reg16 address, Reg8 value);
    void load(Reg16 address, Reg8& value);
    void register_load(Reg16 address, Reg8& value);
    void register_store(Reg16 address, Reg8 value);
    void vram_load(Reg16 offset, bool bank1, Reg8& value);

    void set_interrupt(Reg8 interruptBit);

    void write_ctrl_register(Reg8 loAddr, Reg8 value);

    void write_mbc1(Reg8 hiAddr, Reg8 value);
    void write_mbc2(Reg8 hiAddr, Reg8 value);
    void write_mbc3(Reg8 hiAddr, Reg8 value);
    void write_mbc5(Reg8 hiAddr, Reg8 value);
    void write_mbc_register(Reg8 hiAddr, Reg8 value);

    void cgb_dma_transfer();

    bool IME;

  private:
    Reg8* fullBlock;
    uint32_t fullSize;
    uint32_t offsetVRAM;
    uint32_t offsetExternalRAM;
    uint32_t offsetExternalClock;
    uint32_t offsetInternalRAM;
    uint32_t offsetROM;
    uint32_t offsetTable[16];
    Reg8 romRegisters[8];
    bool biosRunning;
    Gameboy* gb;

    void set_external_clock(uint64_t extClock);
    uint64_t get_external_clock(void);
};
