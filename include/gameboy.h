#pragma once

#include "cpu.h"
#include "event.h"
#include "memory.h"
#include "screen.h"
#include "sound.h"
#include <set>
#include <string>

enum RunError
{
    NO_RUN_ERROR,
    INVALID_OPCODE
};

enum Status
{
    STATUS_NORMAL,
    STATUS_STOPPED,
    STATUS_HALTED
};

enum SupportCode
{
    CGB_INCOMPATIBLE,
    CGB_COMPATIBLE,
    CGB_EXCLUSIVE
};

enum RunningMode
{
    DMG_MODE,
    CGB_MODE,
};

struct CartInfo
{
    std::string gameTitle;
    std::string gameCode;
    SupportCode suppCode;
    RunningMode mode;
    int MBC;
    bool hasRTC;
    bool hasSRAM;
    uint32_t sizeROM;
    uint32_t sizeExternalRAM;
};

struct InternClock
{
    Cycle timerCycles;
    Cycle divCycles;
    Cycle eventCycles;
};

struct InternalState
{
    RunError error;
    Status status;
    InternClock clock;
};

struct GameboyState
{
    InternalState internalState;
    CpuState cpuState;
    MemoryState memoryState;
    ApuState apuState;
    ScreenState screenState;
    bool inUse;
};

class Event;

class Gameboy
{
  public:
    Gameboy(void);
    Gameboy(const Gameboy& other) = delete;
    ~Gameboy(void);
    Gameboy& operator=(const Gameboy& other) = delete;

    // void        reset(void); // TODO: implement
    Cycle next_frame(void);
    Cycle next_it(void);
    Cycle run_multiple_cycle(Cycle cycles);
    Cycle execute_one_instruction(void);
    Reg16 disassemble_instruction(Reg16 addr, std::string& insnString);
    void open_game(const std::string& filename, int32_t* windowPixels);
    void read_game_save(void);
    void write_game_save(void);
    bool load_state(int stateIdx);
    bool save_state(int stateIdx);
    void load_internal_state(const InternalState& internalState);
    void save_internal_state(InternalState& internalState);
    void reset_game(void);
    void close_game(void);
    bool breakpoint_add(Reg16 address) { return (breakpointList.insert(address).second); }
    void breakpoint_remove(Reg16 address) { breakpointList.erase(address); }
    bool get_cgb_priority() const { return prioritizeCgb; }
    void set_cgb_priority(bool prioCgb) { prioritizeCgb = prioCgb; }
    bool breakpoint_reach() const { return breakpointReach; }
    void reset_breakpoint_reach() { breakpointReach = false; }

    const CartInfo& get_cartinfo(void) const { return cartInfo; }
    Status get_status(void) const { return status; }
    bool can_update_image(void) const { return screen->can_update_image(); }
    void update_pixel_ptr(int32_t* _winPixels) { screen->update_pixel_ptr(_winPixels); }
    void hblank_dma_init(void) { screen->hblank_dma_init(); }
    void hblank_dma_stop(void) { screen->hblank_dma_stop(); }

    APU& get_apu(void) { return *apu; }
    bool is_loaded(void) const { return gameLoaded; }
    const std::string& get_load_error(void) const { return gameLoadError; }
    void image_updated(void) { screen->image_updated(); }
    void mem_load(Reg16 address, Reg8& value)
    {
        if (address >= 0xFF00u)
            map->register_load(address, value);
        else
            map->load(address, value);
    }
    void mem_store(Reg16 address, Reg8 value) { map->store(address, value); }
    Reg16 cpu_load_register(CpuRegister address) { return cpu->load_cpu_register(address); }

    // void        check_normal_events(void);

    Event* event;

  private:
    CPU* cpu;
    MemoryMap* map;
    APU* apu;
    Screen* screen;
    RunError error;
    Status status;
    InternClock clock;
    std::set<Reg16> breakpointList;
    std::vector<GameboyState> saveStates;

    bool gameLoaded;
    std::string gamePath;
    std::string gameLoadError;
    CartInfo cartInfo;
    static bool prioritizeCgb;
    bool breakpointReach;

    Reg16 disassemble_top(Reg16 addr, Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string& insnString);
    Reg16 disassemble_load(Reg8 opcode, std::string& insnString);
    Reg16 disassemble_acc(Reg8 opcode, std::string& insnString);
    Reg16 disassemble_cb(Reg8 opcodeCb, std::string& insnString);
    Reg16 disassemble_bot(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string& insnString);
    Reg16 disassemble_opcode_cd(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string& insnString);
    Reg16 disassemble_opcode_ef(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string& insnString);

    Cycle execute_bitop(Reg8 opcode);
    void push_stack(Reg8 hi, Reg8 lo);
    void pop_stack(Reg8& hi, Reg8& lo);
    Cycle call_if(Reg16 a16, bool cond);
    Cycle ret_if(bool cond);
    Cycle rst(Reg16 a16);
    void update_timers(Cycle elapsed);
    void check_control_events(Cycle elapsed);
    void check_interrupts(void);
    void auto_sri(Reg16 newPC, Reg8 ifMask);
};
