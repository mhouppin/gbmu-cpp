#pragma once
#include "types.h"

enum CpuRegister
{
    AF,
    BC,
    DE,
    HL,
    SP,
    PC // Program Counter is address of code
};

struct CpuState
{
    Reg8 A, F, B, C, D, E, H, L;
    Reg16 SP, PC;
};

struct CPU
{
    Reg8 A, F, B, C, D, E, H, L;
    Reg16 SP, PC;

    CPU(bool cgb);
    CPU(const CPU& other) = delete;
    ~CPU(void) {}
    CPU& operator=(const CPU& other) = delete;

    void reset(bool cgb);

    void load_state(const CpuState& state);
    void save_state(CpuState& state) const;

    Reg16 load_cpu_register(CpuRegister reg);

    Cycle load_r8_d8(Reg8& r, Reg8 d);
    Cycle load_r16_d16(Reg8& hi, Reg8& lo, Reg8 dhi, Reg8 dlo);
    Cycle load_r8_r8(Reg8& dst, Reg8 src);
    Cycle load_hl_sp_r8(Reg8 r);

    Cycle inc_r8(Reg8& r);
    Cycle dec_r8(Reg8& r);
    Cycle inc_r16(Reg8& hi, Reg8& lo);
    Cycle dec_r16(Reg8& hi, Reg8& lo);

    Cycle add_a_r8(Reg8 r);
    Cycle sub_a_r8(Reg8 r);
    Cycle add_hl_r16(Reg16 r);
    Cycle add_sp_r8(Reg8 r);

    Cycle adc_a_r8(Reg8 r);
    Cycle sbc_a_r8(Reg8 r);
    Cycle and_a_r8(Reg8 r);
    Cycle xor_a_r8(Reg8 r);
    Cycle or_a_r8(Reg8 r);
    Cycle cmp_a_r8(Reg8 r);

    Cycle rlc_r8(Reg8& r);
    Cycle rrc_r8(Reg8& r);
    Cycle rl_r8(Reg8& r);
    Cycle rr_r8(Reg8& r);
    Cycle sla_r8(Reg8& r);
    Cycle sra_r8(Reg8& r);
    Cycle swap_r8(Reg8& r);
    Cycle srl_r8(Reg8& r);
    Cycle test_bit_r8(Reg8 r, Reg8 bit);
    Cycle reset_bit_r8(Reg8& r, Reg8 bit);
    Cycle set_bit_r8(Reg8& r, Reg8 bit);

    Cycle jump_relative_if(Reg8 r8, bool cond);
    Cycle jump_absolute_if(Reg16 a16, bool cond);

    Cycle daa(void);
};
