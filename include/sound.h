#ifndef SOUND_H
#define SOUND_H

#include "memory.h"
#include "types.h"

struct ApuState
{
    Reg8 divAPU;

    // CH1 - Square wave with envelope and sweep
    Reg8 sq1SwpPace;
    Reg8 sq1SwpDirection;
    Reg8 sq1SwpSlope;
    Reg8 sq1EvpVolume;
    Reg8 sq1EvpDirection;
    Reg8 sq1EvpSweepPace;
    Reg16 sq1Wavelength;
    Reg8 sq1Length;

    bool sq1HasCounter;
    Reg8 sq1SwpCounter;

    // CH2 - Square wave with envelope
    Reg8 sq2EvpVolume;
    Reg8 sq2EvpDirection;
    Reg8 sq2EvpSweepPace;
    Reg16 sq2Wavelength;
    Reg8 sq2Length;

    bool sq2HasCounter;

    // CH3 - Programmable wave table
    Reg8 pwtWavelength;
    Reg16 pwtLength;

    bool pwtHasCounter;

    // CH4 - Noise generator
    Reg8 noiseEvpVolume;
    Reg8 noiseEvpDirection;
    Reg8 noiseEvpSweepPace;
    Reg8 noiseClockShift;
    Reg8 noiseLfsrWidth;
    Reg8 noiseClockDivider;
    Reg8 noiseLength;

    bool noiseHasCounter;
};

class APU
{
  public:
    APU(MemoryMap* map);

    void update(Cycle elapsed);

    void load_state(const ApuState& state);
    void save_state(ApuState& state) const;

    void initialize_square1(void);
    void initialize_square2(void);
    void initialize_wave(void);
    void initialize_noise(void);

    void increase_div_apu(void);

  private:
    MemoryMap* map;
    Reg8 divAPU;

    // CH1 - Square wave with envelope and sweep
    Reg8 sq1SwpPace;
    Reg8 sq1SwpDirection;
    Reg8 sq1SwpSlope;
    Reg8 sq1EvpVolume;
    Reg8 sq1EvpDirection;
    Reg8 sq1EvpSweepPace;
    Reg16 sq1Wavelength;
    Reg8 sq1Length;

    bool sq1HasCounter;
    Reg8 sq1SwpCounter;

    // CH2 - Square wave with envelope
    Reg8 sq2EvpVolume;
    Reg8 sq2EvpDirection;
    Reg8 sq2EvpSweepPace;
    Reg16 sq2Wavelength;
    Reg8 sq2Length;

    bool sq2HasCounter;

    // CH3 - Programmable wave table
    Reg8 pwtWavelength;
    Reg16 pwtLength;

    bool pwtHasCounter;

    // CH4 - Noise generator
    Reg8 noiseEvpVolume;
    Reg8 noiseEvpDirection;
    Reg8 noiseEvpSweepPace;
    Reg8 noiseClockShift;
    Reg8 noiseLfsrWidth;
    Reg8 noiseClockDivider;
    Reg8 noiseLength;

    bool noiseHasCounter;

    void trigger_envelope_sweep(void);
    void trigger_sound_length(void);
    void trigger_sq1_freq_sweep(void);
};

#endif
