#pragma once
#include <cstdint>

enum Cycle : uint64_t
{
    ZERO_CYCLE = 0,
    OAM_READ_TIME = 80,
    OAM_VRAM_READ_TIME = 200,
    HZ_BLANKING_TIME = 176,
    VT_BLANKING_LINE_TIME = 456,
    ONE_CYCLE = 4,
    ONE_SECOND = 4194304,
};

constexpr Cycle operator+(Cycle c, uint64_t i) { return Cycle(uint64_t(c) + i); }
constexpr Cycle operator-(Cycle c, uint64_t i) { return Cycle(uint64_t(c) - i); }
inline Cycle& operator+=(Cycle& c, uint64_t i) { return c = c + i; }
inline Cycle& operator-=(Cycle& c, uint64_t i) { return c = c - i; }

constexpr Cycle operator*(Cycle c, uint64_t i) { return Cycle(uint64_t(c) * i); }
constexpr Cycle operator*(Cycle c, int i) { return Cycle(uint64_t(c) * uint64_t(i)); }
constexpr Cycle operator*(uint64_t i, Cycle c) { return Cycle(i * uint64_t(c)); }
constexpr Cycle operator*(int i, Cycle c) { return Cycle(uint64_t(i) * uint64_t(c)); }
constexpr Cycle operator/(Cycle c, uint64_t i) { return Cycle(uint64_t(c) / i); }

inline Cycle& operator*=(Cycle& c, uint64_t i) { return c = Cycle(uint64_t(c) * i); }
inline Cycle& operator/=(Cycle& c, uint64_t i) { return c = Cycle(uint64_t(c) / i); }

typedef uint8_t Reg8;
typedef uint16_t Reg16;

enum : Reg8
{
    BIT_0 = 0x01,
    BIT_1 = 0x02,
    BIT_2 = 0x04,
    BIT_3 = 0x08,
    BIT_4 = 0x10,
    BIT_5 = 0x20,
    BIT_6 = 0x40,
    BIT_7 = 0x80,

    FLAG_CY = BIT_4,
    FLAG_H = BIT_5,
    FLAG_N = BIT_6,
    FLAG_Z = BIT_7,

    BIT4_LO = 0x0F,
    BIT4_HI = 0xF0
};

enum : Reg16
{
    BIT12_LO = 0x0FFF,
    WRAM_BANK_SIZE = 0x1000,
    SRAM_BANK_SIZE = 0x2000,
    ROM_BANK_SIZE = 0x4000,
};

constexpr Reg16 make_reg16(Reg8 hi, Reg8 lo) { return (Reg16(hi) << 8) + lo; }

inline Reg8 reg16_lo(Reg16 r) { return Reg8(r); }

inline Reg8 reg16_hi(Reg16 r) { return Reg8(r >> 8); }
