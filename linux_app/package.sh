#!/bin/sh

set -e

if [ ! -e gbmu ]
then
    echo "Cannot find the executable file in the current directory."
    exit 1
fi

convert linux_app/gbmu.xpm gbmu.png

cp linux_app/install.sh linux_app/uninstall.sh .
tar -cvf gbmu.tar gbmu gbmu.png install.sh uninstall.sh
rm -v gbmu.png install.sh uninstall.sh
