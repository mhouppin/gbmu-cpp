#!/bin/sh

set -e

install -v -D -m 755 gbmu $HOME/.local/bin/gbmu
install -v -D -m 644 gbmu.png $HOME/.local/share/icons/hicolor/48x48/maps/gbmu.png

cat > gbmu.desktop << EOF
[Desktop Entry]

Type=Application

Name=GBmu

Comment=A DMG/CGB emulator and debugger

Path=$HOME

Exec=$HOME/.local/bin/gbmu

Icon=$HOME/.local/share/icons/hicolor/48x48/maps/gbmu.png

Terminal=false

MimeType=application/x-gameboy-rom;application/x-gameboy-color-rom;
EOF

desktop-file-install --dir=$HOME/.local/share/applications gbmu.desktop
update-desktop-database --verbose $HOME/.local/share/applications
