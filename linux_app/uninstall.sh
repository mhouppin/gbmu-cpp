#!/bin/sh

rm -vf $HOME/.local/bin/gbmu
rm -vf $HOME/.local/share/icons/hicolor/48x48/maps/gbmu.png
rm -vf $HOME/.local/share/applications/gbmu.desktop
update-desktop-database --verbose $HOME/.local/share/applications
