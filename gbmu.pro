QT       += core gui widgets

CONFIG += c++17 debug_and_release

TARGET = gbmu

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    source/cpu.cpp \
    source/gameboy.cpp \
    source/gameboy_bitops.cpp \
    source/gameboy_disassemble.cpp \
    source/gameboy_event.cpp \
    source/gameboy_execute.cpp \
    source/gameboy_misc.cpp \
    source/memory.cpp \
    source/screen.cpp \
    source/sound.cpp \
    source/ui/debugger_ui.cpp \
    source/ui/gameboy_ui.cpp \
    source/ui/main.cpp \
    source/ui/memory_image_widget.cpp

INCLUDEPATH += include

HEADERS += \
    include/cpu.h \
    include/event.h \
    include/gameboy.h \
    include/memory.h \
    include/screen.h \
    include/sound.h \
    include/types.h \
    source/ui/gameboy_ui.hpp \
    source/ui/debugger_ui.hpp \
    source/ui/memory_image_widget.hpp

TRANSLATIONS = i18n/gbmu_en.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
