#include "gameboy.h"

void Gameboy::push_stack(Reg8 hi, Reg8 lo)
{
    map->store(--cpu->SP, hi);
    map->store(--cpu->SP, lo);
}

void Gameboy::pop_stack(Reg8& hi, Reg8& lo)
{
    map->load(cpu->SP++, lo);
    map->load(cpu->SP++, hi);
}

Cycle Gameboy::call_if(Reg16 a16, bool cond)
{
    cpu->PC += 3;
    if (cond)
    {
        push_stack(reg16_hi(cpu->PC), reg16_lo(cpu->PC));
        cpu->PC = a16;
        return 6 * ONE_CYCLE;
    }
    return 3 * ONE_CYCLE;
}

Cycle Gameboy::ret_if(bool cond)
{
    Reg8 PCh, PCl;

    if (cond)
    {
        pop_stack(PCh, PCl);
        cpu->PC = make_reg16(PCh, PCl);
        return 5 * ONE_CYCLE;
    }
    cpu->PC += 1;
    return 2 * ONE_CYCLE;
}

Cycle Gameboy::rst(Reg16 a16)
{
    cpu->PC += 1;
    push_stack(reg16_hi(cpu->PC), reg16_lo(cpu->PC));
    cpu->PC = a16;
    return 4 * ONE_CYCLE;
}
