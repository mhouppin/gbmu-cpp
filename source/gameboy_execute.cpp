#include "gameboy.h"
#include <iomanip>
#include <iostream>

Cycle Gameboy::execute_one_instruction(void)
{
    Reg8 opcode, imm8, imm8h;

    map->load(cpu->PC + 0, opcode);
    map->load(cpu->PC + 1, imm8);
    map->load(cpu->PC + 2, imm8h);

    switch (opcode)
    {
        // NOP
        // LD B, B
        // LD C, C
        // LD D, D
        // LD E, E
        // LD H, H
        // LD L, L
        // LD A, A
        case 0x00:
        case 0x40:
        case 0x49:
        case 0x52:
        case 0x5B:
        case 0x64:
        case 0x6D:
        case 0x7F: cpu->PC += 1; return ONE_CYCLE;

        // LD BC,d16
        case 0x01: return cpu->load_r16_d16(cpu->B, cpu->C, imm8h, imm8);

        // LD (BC),A
        case 0x02:
            map->store(make_reg16(cpu->B, cpu->C), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC BC
        case 0x03: return cpu->inc_r16(cpu->B, cpu->C);

        // INC B
        case 0x04: return cpu->inc_r8(cpu->B);

        // DEC B
        case 0x05: return cpu->dec_r8(cpu->B);

        // LD B,d8
        case 0x06: return cpu->load_r8_d8(cpu->B, imm8);

        // RLCA
        case 0x07:
            imm8 = (cpu->A & BIT_7) >> 7;
            cpu->F = imm8 << 4;
            cpu->A = (cpu->A << 1) | imm8;
            cpu->PC += 1;
            return ONE_CYCLE;

        // LD (a16),SP
        case 0x08:
            map->store(make_reg16(imm8h, imm8) + 0, reg16_lo(cpu->SP));
            map->store(make_reg16(imm8h, imm8) + 1, reg16_hi(cpu->SP));
            cpu->PC += 3;
            return 5 * ONE_CYCLE;

        // ADD HL,BC
        case 0x09: return cpu->add_hl_r16(make_reg16(cpu->B, cpu->C));

        // LD A,(BC)
        case 0x0A:
            map->load(make_reg16(cpu->B, cpu->C), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // DEC BC
        case 0x0B: return cpu->dec_r16(cpu->B, cpu->C);

        // INC C
        case 0x0C: return cpu->inc_r8(cpu->C);

        // DEC C
        case 0x0D: return cpu->dec_r8(cpu->C);

        // LD C,d8
        case 0x0E: return cpu->load_r8_d8(cpu->C, imm8);

        // RRCA
        case 0x0F:
            imm8 = (cpu->A & BIT_0) << 7;
            cpu->F = imm8 >> 3;
            cpu->A = (cpu->A >> 1) | imm8;
            cpu->PC += 1;
            return ONE_CYCLE;

        // STOP 0
        case 0x10:
            status = STATUS_STOPPED;
            map->load(IF_ADDRESS, imm8);
            imm8 &= ~BIT_4;
            map->store(IF_ADDRESS, imm8);

            // Check for CPU speed switch
            map->load(KEY1_ADDRESS, imm8);
            if (!!(imm8 & BIT_0) && get_cartinfo().mode == CGB_MODE)
            {
                imm8 = ((imm8 & BIT_7) ^ BIT_7) | 0x7Eu;
                map->register_store(KEY1_ADDRESS, imm8);
                status = STATUS_NORMAL;
            }
            cpu->PC += 1;
            return ONE_CYCLE;

        // LD DE,d16
        case 0x11: return cpu->load_r16_d16(cpu->D, cpu->E, imm8h, imm8);

        // LD (DE),A
        case 0x12:
            map->store(make_reg16(cpu->D, cpu->E), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC DE
        case 0x13: return cpu->inc_r16(cpu->D, cpu->E);

        // INC D
        case 0x14: return cpu->inc_r8(cpu->D);

        // DEC D
        case 0x15: return cpu->dec_r8(cpu->D);

        // LD D,d8
        case 0x16: return cpu->load_r8_d8(cpu->D, imm8);

        // RLA
        case 0x17:
            imm8 = (cpu->F & FLAG_CY) >> 4;
            cpu->F = (cpu->A & BIT_7) >> 3;
            cpu->A = (cpu->A << 1) | imm8;
            cpu->PC += 1;
            return ONE_CYCLE;

        // JR r8
        case 0x18: return cpu->jump_relative_if(imm8, true);

        // ADD HL,DE
        case 0x19: return cpu->add_hl_r16(make_reg16(cpu->D, cpu->E));

        // LD A,(DE)
        case 0x1A:
            map->load(make_reg16(cpu->D, cpu->E), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // DEC DE
        case 0x1B: return cpu->dec_r16(cpu->D, cpu->E);

        // INC E
        case 0x1C: return cpu->inc_r8(cpu->E);

        // DEC E
        case 0x1D: return cpu->dec_r8(cpu->E);

        // LD E,d8
        case 0x1E: return cpu->load_r8_d8(cpu->E, imm8);

        // RRA
        case 0x1F:
            imm8 = (cpu->F & FLAG_CY) << 3;
            cpu->F = (cpu->A & BIT_0) << 4;
            cpu->A = (cpu->A >> 1) | imm8;
            cpu->PC += 1;
            return ONE_CYCLE;

        // JR NZ,r8
        case 0x20: return cpu->jump_relative_if(imm8, !(cpu->F & FLAG_Z));

        // LD HL,d16
        case 0x21: return cpu->load_r16_d16(cpu->H, cpu->L, imm8h, imm8);

        // LD (HL+),A
        case 0x22:
            map->store(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->L += 1;
            cpu->H += !cpu->L;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC HL
        case 0x23: return cpu->inc_r16(cpu->H, cpu->L);

        // INC H
        case 0x24: return cpu->inc_r8(cpu->H);

        // DEC H
        case 0x25: return cpu->dec_r8(cpu->H);

        // LD H,d8
        case 0x26: return cpu->load_r8_d8(cpu->H, imm8);

        // DAA
        case 0x27: return cpu->daa();

        // JR Z,r8
        case 0x28: return cpu->jump_relative_if(imm8, !!(cpu->F & FLAG_Z));

        // ADD HL,HL
        case 0x29: return cpu->add_hl_r16(make_reg16(cpu->H, cpu->L));

        // LD A,(HL+)
        case 0x2A:
            map->load(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->L += 1;
            cpu->H += !cpu->L;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // DEC HL
        case 0x2B: return cpu->dec_r16(cpu->H, cpu->L);

        // INC L
        case 0x2C: return cpu->inc_r8(cpu->L);

        // DEC L
        case 0x2D: return cpu->dec_r8(cpu->L);

        // LD L,d8
        case 0x2E: return cpu->load_r8_d8(cpu->L, imm8);

        // CPL
        case 0x2F:
            cpu->F |= FLAG_N | FLAG_H;
            cpu->A = ~cpu->A;
            cpu->PC += 1;
            return ONE_CYCLE;

        // JR NC,r8
        case 0x30: return cpu->jump_relative_if(imm8, !(cpu->F & FLAG_CY));

        // LD SP,d16
        case 0x31:
            cpu->SP = make_reg16(imm8h, imm8);
            cpu->PC += 3;
            return 3 * ONE_CYCLE;

        // LD (HL-),A
        case 0x32:
            map->store(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->H -= !cpu->L;
            cpu->L -= 1;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC SP
        case 0x33:
            cpu->SP += 1;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC (HL)
        case 0x34:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->inc_r8(imm8);
            map->store(make_reg16(cpu->H, cpu->L), imm8);
            return 3 * ONE_CYCLE;

        // DEC (HL)
        case 0x35:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->dec_r8(imm8);
            map->store(make_reg16(cpu->H, cpu->L), imm8);
            return 3 * ONE_CYCLE;

        // LD (HL),d8
        case 0x36:
            map->store(make_reg16(cpu->H, cpu->L), imm8);
            cpu->PC += 2;
            return 3 * ONE_CYCLE;

        // SCF
        case 0x37:
            cpu->F &= FLAG_Z;
            cpu->F |= FLAG_CY;
            cpu->PC += 1;
            return ONE_CYCLE;

        // JR C,r8
        case 0x38: return cpu->jump_relative_if(imm8, !!(cpu->F & FLAG_CY));

        // ADD HL,SP
        case 0x39: return cpu->add_hl_r16(cpu->SP);

        // LD A,(HL-)
        case 0x3A:
            map->load(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->H -= !cpu->L;
            cpu->L -= 1;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // DEC SP
        case 0x3B:
            cpu->SP -= 1;
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // INC A
        case 0x3C: return cpu->inc_r8(cpu->A);

        // DEC A
        case 0x3D: return cpu->dec_r8(cpu->A);

        // LD A,d8
        case 0x3E: return cpu->load_r8_d8(cpu->A, imm8);

        // CCF
        case 0x3F:
            cpu->F ^= FLAG_CY;
            cpu->F &= FLAG_Z | FLAG_CY;
            cpu->PC += 1;
            return ONE_CYCLE;

        // LD B,C
        case 0x41: return cpu->load_r8_r8(cpu->B, cpu->C);

        // LD B,D
        case 0x42: return cpu->load_r8_r8(cpu->B, cpu->D);

        // LD B,E
        case 0x43: return cpu->load_r8_r8(cpu->B, cpu->E);

        // LD B,H
        case 0x44: return cpu->load_r8_r8(cpu->B, cpu->H);

        // LD B,L
        case 0x45: return cpu->load_r8_r8(cpu->B, cpu->L);

        // LD B,(HL)
        case 0x46:
            map->load(make_reg16(cpu->H, cpu->L), cpu->B);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD B,A
        case 0x47: return cpu->load_r8_r8(cpu->B, cpu->A);

        // LD C,B
        case 0x48: return cpu->load_r8_r8(cpu->C, cpu->B);

        // LD C,D
        case 0x4A: return cpu->load_r8_r8(cpu->C, cpu->D);

        // LD C,E
        case 0x4B: return cpu->load_r8_r8(cpu->C, cpu->E);

        // LD C,H
        case 0x4C: return cpu->load_r8_r8(cpu->C, cpu->H);

        // LD C,L
        case 0x4D: return cpu->load_r8_r8(cpu->C, cpu->L);

        // LD C,(HL)
        case 0x4E:
            map->load(make_reg16(cpu->H, cpu->L), cpu->C);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD C,A
        case 0x4F: return cpu->load_r8_r8(cpu->C, cpu->A);

        // LD D,B
        case 0x50: return cpu->load_r8_r8(cpu->D, cpu->B);

        // LD D,C
        case 0x51: return cpu->load_r8_r8(cpu->D, cpu->C);

        // LD D,E
        case 0x53: return cpu->load_r8_r8(cpu->D, cpu->E);

        // LD D,H
        case 0x54: return cpu->load_r8_r8(cpu->D, cpu->H);

        // LD D,L
        case 0x55: return cpu->load_r8_r8(cpu->D, cpu->L);

        // LD D,(HL)
        case 0x56:
            map->load(make_reg16(cpu->H, cpu->L), cpu->D);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD D,A
        case 0x57: return cpu->load_r8_r8(cpu->D, cpu->A);

        // LD E,B
        case 0x58: return cpu->load_r8_r8(cpu->E, cpu->B);

        // LD E,C
        case 0x59: return cpu->load_r8_r8(cpu->E, cpu->C);

        // LD E,D
        case 0x5A: return cpu->load_r8_r8(cpu->E, cpu->D);

        // LD E,H
        case 0x5C: return cpu->load_r8_r8(cpu->E, cpu->H);

        // LD E,L
        case 0x5D: return cpu->load_r8_r8(cpu->E, cpu->L);

        // LD E,(HL)
        case 0x5E:
            map->load(make_reg16(cpu->H, cpu->L), cpu->E);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD E,A
        case 0x5F: return cpu->load_r8_r8(cpu->E, cpu->A);

        // LD H,B
        case 0x60: return cpu->load_r8_r8(cpu->H, cpu->B);

        // LD H,C
        case 0x61: return cpu->load_r8_r8(cpu->H, cpu->C);

        // LD H,D
        case 0x62: return cpu->load_r8_r8(cpu->H, cpu->D);

        // LD H,E
        case 0x63: return cpu->load_r8_r8(cpu->H, cpu->E);

        // LD H,L
        case 0x65: return cpu->load_r8_r8(cpu->H, cpu->L);

        // LD H,(HL)
        case 0x66:
            map->load(make_reg16(cpu->H, cpu->L), cpu->H);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD H,A
        case 0x67: return cpu->load_r8_r8(cpu->H, cpu->A);

        // LD L,B
        case 0x68: return cpu->load_r8_r8(cpu->L, cpu->B);

        // LD L,C
        case 0x69: return cpu->load_r8_r8(cpu->L, cpu->C);

        // LD L,D
        case 0x6A: return cpu->load_r8_r8(cpu->L, cpu->D);

        // LD L,E
        case 0x6B: return cpu->load_r8_r8(cpu->L, cpu->E);

        // LD L,H
        case 0x6C: return cpu->load_r8_r8(cpu->L, cpu->H);

        // LD L,(HL)
        case 0x6E:
            map->load(make_reg16(cpu->H, cpu->L), cpu->L);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD L,A
        case 0x6F: return cpu->load_r8_r8(cpu->L, cpu->A);

        // LD (HL),B
        case 0x70:
            map->store(make_reg16(cpu->H, cpu->L), cpu->B);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD (HL),C
        case 0x71:
            map->store(make_reg16(cpu->H, cpu->L), cpu->C);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD (HL),D
        case 0x72:
            map->store(make_reg16(cpu->H, cpu->L), cpu->D);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD (HL),E
        case 0x73:
            map->store(make_reg16(cpu->H, cpu->L), cpu->E);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD (HL),H
        case 0x74:
            map->store(make_reg16(cpu->H, cpu->L), cpu->H);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD (HL),L
        case 0x75:
            map->store(make_reg16(cpu->H, cpu->L), cpu->L);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // HALT
        case 0x76:
            status = STATUS_HALTED;
            map->load(IF_ADDRESS, imm8);
            imm8 &= ~BIT_4;
            map->store(IF_ADDRESS, imm8);
            cpu->PC += 1;
            return ONE_CYCLE;

        // LD (HL),A
        case 0x77:
            map->store(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD A,B
        case 0x78: return cpu->load_r8_r8(cpu->A, cpu->B);

        // LD A,C
        case 0x79: return cpu->load_r8_r8(cpu->A, cpu->C);

        // LD A,D
        case 0x7A: return cpu->load_r8_r8(cpu->A, cpu->D);

        // LD A,E
        case 0x7B: return cpu->load_r8_r8(cpu->A, cpu->E);

        // LD A,H
        case 0x7C: return cpu->load_r8_r8(cpu->A, cpu->H);

        // LD A,L
        case 0x7D: return cpu->load_r8_r8(cpu->A, cpu->L);

        // LD A,(HL)
        case 0x7E:
            map->load(make_reg16(cpu->H, cpu->L), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // ADD A,B
        case 0x80: return cpu->add_a_r8(cpu->B);

        // ADD A,C
        case 0x81: return cpu->add_a_r8(cpu->C);

        // ADD A,D
        case 0x82: return cpu->add_a_r8(cpu->D);

        // ADD A,E
        case 0x83: return cpu->add_a_r8(cpu->E);

        // ADD A,H
        case 0x84: return cpu->add_a_r8(cpu->H);

        // ADD A,L
        case 0x85: return cpu->add_a_r8(cpu->L);

        // ADD A,(HL)
        case 0x86:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->add_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // ADD A,A
        case 0x87: return cpu->add_a_r8(cpu->A);

        // ADC A,B
        case 0x88: return cpu->adc_a_r8(cpu->B);

        // ADC A,C
        case 0x89: return cpu->adc_a_r8(cpu->C);

        // ADC A,D
        case 0x8A: return cpu->adc_a_r8(cpu->D);

        // ADC A,E
        case 0x8B: return cpu->adc_a_r8(cpu->E);

        // ADC A,H
        case 0x8C: return cpu->adc_a_r8(cpu->H);

        // ADC A,L
        case 0x8D: return cpu->adc_a_r8(cpu->L);

        // ADC A,(HL)
        case 0x8E:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->adc_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // ADC A,A
        case 0x8F: return cpu->adc_a_r8(cpu->A);

        // SUB A,B
        case 0x90: return cpu->sub_a_r8(cpu->B);

        // SUB A,C
        case 0x91: return cpu->sub_a_r8(cpu->C);

        // SUB A,D
        case 0x92: return cpu->sub_a_r8(cpu->D);

        // SUB A,E
        case 0x93: return cpu->sub_a_r8(cpu->E);

        // SUB A,H
        case 0x94: return cpu->sub_a_r8(cpu->H);

        // SUB A,L
        case 0x95: return cpu->sub_a_r8(cpu->L);

        // SUB A,(HL)
        case 0x96:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->sub_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // SUB A,A
        case 0x97: return cpu->sub_a_r8(cpu->A);

        // SBC A,B
        case 0x98: return cpu->sbc_a_r8(cpu->B);

        // SBC A,C
        case 0x99: return cpu->sbc_a_r8(cpu->C);

        // SBC A,D
        case 0x9A: return cpu->sbc_a_r8(cpu->D);

        // SBC A,E
        case 0x9B: return cpu->sbc_a_r8(cpu->E);

        // SBC A,H
        case 0x9C: return cpu->sbc_a_r8(cpu->H);

        // SBC A,L
        case 0x9D: return cpu->sbc_a_r8(cpu->L);

        // SBC A,(HL)
        case 0x9E:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->sbc_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // SBC A,A
        case 0x9F: return cpu->sbc_a_r8(cpu->A);

        // AND A,B
        case 0xA0: return cpu->and_a_r8(cpu->B);

        // AND A,C
        case 0xA1: return cpu->and_a_r8(cpu->C);

        // AND A,D
        case 0xA2: return cpu->and_a_r8(cpu->D);

        // AND A,E
        case 0xA3: return cpu->and_a_r8(cpu->E);

        // AND A,H
        case 0xA4: return cpu->and_a_r8(cpu->H);

        // AND A,L
        case 0xA5: return cpu->and_a_r8(cpu->L);

        // AND A,(HL)
        case 0xA6:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->and_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // AND A,A
        case 0xA7: return cpu->and_a_r8(cpu->A);

        // XOR A,B
        case 0xA8: return cpu->xor_a_r8(cpu->B);

        // XOR A,C
        case 0xA9: return cpu->xor_a_r8(cpu->C);

        // XOR A,D
        case 0xAA: return cpu->xor_a_r8(cpu->D);

        // XOR A,E
        case 0xAB: return cpu->xor_a_r8(cpu->E);

        // XOR A,H
        case 0xAC: return cpu->xor_a_r8(cpu->H);

        // XOR A,L
        case 0xAD: return cpu->xor_a_r8(cpu->L);

        // XOR A,(HL)
        case 0xAE:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->xor_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // XOR A,A
        case 0xAF: return cpu->xor_a_r8(cpu->A);

        // OR A,B
        case 0xB0: return cpu->or_a_r8(cpu->B);

        // OR A,C
        case 0xB1: return cpu->or_a_r8(cpu->C);

        // OR A,D
        case 0xB2: return cpu->or_a_r8(cpu->D);

        // OR A,E
        case 0xB3: return cpu->or_a_r8(cpu->E);

        // OR A,H
        case 0xB4: return cpu->or_a_r8(cpu->H);

        // OR A,L
        case 0xB5: return cpu->or_a_r8(cpu->L);

        // OR A,(HL)
        case 0xB6:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->or_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // OR A,A
        case 0xB7: return cpu->or_a_r8(cpu->A);

        // CMP A,B
        case 0xB8: return cpu->cmp_a_r8(cpu->B);

        // CMP A,C
        case 0xB9: return cpu->cmp_a_r8(cpu->C);

        // CMP A,D
        case 0xBA: return cpu->cmp_a_r8(cpu->D);

        // CMP A,E
        case 0xBB: return cpu->cmp_a_r8(cpu->E);

        // CMP A,H
        case 0xBC: return cpu->cmp_a_r8(cpu->H);

        // CMP A,L
        case 0xBD: return cpu->cmp_a_r8(cpu->L);

        // CMP A,(HL)
        case 0xBE:
            map->load(make_reg16(cpu->H, cpu->L), imm8);
            cpu->cmp_a_r8(imm8);
            return 2 * ONE_CYCLE;

        // CMP A,A
        case 0xBF: return cpu->cmp_a_r8(cpu->A);

        // RET NZ
        case 0xC0: return ret_if(!(cpu->F & FLAG_Z));

        // POP BC
        case 0xC1:
            pop_stack(cpu->B, cpu->C);
            cpu->PC += 1;
            return 3 * ONE_CYCLE;

        // JP NZ,a16
        case 0xC2: return cpu->jump_absolute_if(make_reg16(imm8h, imm8), !(cpu->F & FLAG_Z));

        // JP a16
        case 0xC3: return cpu->jump_absolute_if(make_reg16(imm8h, imm8), true);

        // CALL NZ,a16
        case 0xC4: return call_if(make_reg16(imm8h, imm8), !(cpu->F & FLAG_Z));

        // PUSH BC
        case 0xC5:
            push_stack(cpu->B, cpu->C);
            cpu->PC += 1;
            return 4 * ONE_CYCLE;

        // ADD A,d8
        case 0xC6:
            cpu->add_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 00H
        case 0xC7: return rst(0x00);

        // RET Z
        case 0xC8: return ret_if(!!(cpu->F & FLAG_Z));

        // RET
        case 0xC9: ret_if(true); return 4 * ONE_CYCLE;

        // JP Z,a16
        case 0xCA: return cpu->jump_absolute_if(make_reg16(imm8h, imm8), !!(cpu->F & FLAG_Z));

        // PREFIX CB
        case 0xCB: cpu->PC += 2; return execute_bitop(imm8);

        // CALL Z,a16
        case 0xCC: return call_if(make_reg16(imm8h, imm8), !!(cpu->F & FLAG_Z));

        // CALL a16
        case 0xCD: return call_if(make_reg16(imm8h, imm8), true);

        // ADC A,d8
        case 0xCE:
            cpu->adc_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 08H
        case 0xCF: return rst(0x08);

        // RET NC
        case 0xD0: return ret_if(!(cpu->F & FLAG_CY));

        // POP DE
        case 0xD1:
            pop_stack(cpu->D, cpu->E);
            cpu->PC += 1;
            return 3 * ONE_CYCLE;

        // JP NC,a16
        case 0xD2: return cpu->jump_absolute_if(make_reg16(imm8h, imm8), !(cpu->F & FLAG_CY));

        // CALL NC,a16
        case 0xD4: return call_if(make_reg16(imm8h, imm8), !(cpu->F & FLAG_CY));

        // PUSH DE
        case 0xD5:
            push_stack(cpu->D, cpu->E);
            cpu->PC += 1;
            return 4 * ONE_CYCLE;

        // SUB A,d8
        case 0xD6:
            cpu->sub_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 10H
        case 0xD7: return rst(0x10);

        // RET C
        case 0xD8: return ret_if(!!(cpu->F & FLAG_CY));

        // RETI
        case 0xD9:
            ret_if(true);
            map->IME = true;
            return 4 * ONE_CYCLE;

        // JP C,a16
        case 0xDA: return cpu->jump_absolute_if(make_reg16(imm8h, imm8), !!(cpu->F & FLAG_CY));

        // CALL C,a16
        case 0xDC: return call_if(make_reg16(imm8h, imm8), !!(cpu->F & FLAG_CY));

        // SBC A,d8
        case 0xDE:
            cpu->sbc_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 18H
        case 0xDF: return rst(0x18);

        // LDH (a8),a
        case 0xE0:
            map->store(make_reg16(0xFF, imm8), cpu->A);
            cpu->PC += 2;
            return 3 * ONE_CYCLE;

        // POP HL
        case 0xE1:
            pop_stack(cpu->H, cpu->L);
            cpu->PC += 1;
            return 3 * ONE_CYCLE;

        // LD (C),a
        case 0xE2:
            map->store(make_reg16(0xFF, cpu->C), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // PUSH HL
        case 0xE5:
            push_stack(cpu->H, cpu->L);
            cpu->PC += 1;
            return 4 * ONE_CYCLE;

        // AND A,d8
        case 0xE6:
            cpu->and_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 20H
        case 0xE7: return rst(0x20);

        // ADD SP,r8
        case 0xE8: return cpu->add_sp_r8(imm8);

        // JP (HL)
        case 0xE9: cpu->PC = make_reg16(cpu->H, cpu->L); return ONE_CYCLE;

        // LD (a16),A
        case 0xEA:
            map->store(make_reg16(imm8h, imm8), cpu->A);
            cpu->PC += 3;
            return 4 * ONE_CYCLE;

        // XOR A,d8
        case 0xEE:
            cpu->xor_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 28H
        case 0xEF: return rst(0x28);

        // LDH A,(a8)
        case 0xF0:
            map->load(make_reg16(0xFF, imm8), cpu->A);
            cpu->PC += 2;
            return 3 * ONE_CYCLE;

        // POP AF
        case 0xF1:
            pop_stack(cpu->A, cpu->F);
            cpu->PC += 1;
            return 3 * ONE_CYCLE;

        // LD A,(C)
        case 0xF2:
            map->load(make_reg16(0xFF, cpu->C), cpu->A);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // DI
        case 0xF3:
            map->IME = false;
            cpu->PC += 1;
            return ONE_CYCLE;

        // PUSH AF
        case 0xF5:
            push_stack(cpu->A, cpu->F & 0xF0u);
            cpu->PC += 1;
            return 4 * ONE_CYCLE;

        // OR A,d8
        case 0xF6:
            cpu->or_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 30H
        case 0xF7: return rst(0x30);

        // LD HL,SP+r8
        case 0xF8: return cpu->load_hl_sp_r8(imm8);

        // LD SP,HL
        case 0xF9:
            cpu->SP = make_reg16(cpu->H, cpu->L);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // LD A,(a16)
        case 0xFA:
            map->load(make_reg16(imm8h, imm8), cpu->A);
            cpu->PC += 3;
            return 4 * ONE_CYCLE;

        // EI
        case 0xFB:
            map->IME = true;
            cpu->PC += 1;
            return ONE_CYCLE;

        // CMP A,d8
        case 0xFE:
            cpu->cmp_a_r8(imm8);
            cpu->PC += 1;
            return 2 * ONE_CYCLE;

        // RST 38H
        case 0xFF: return rst(0x38);
    }

    // Invalid opcode
    error = INVALID_OPCODE;
    cpu->PC += 1;
    return ONE_CYCLE;
}
