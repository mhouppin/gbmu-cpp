#include "memory.h"
#include "gameboy.h"
#include <algorithm>
#include <ctime>

const uint8_t DMG_BIOS[] = "\x31\xfe\xff\xaf\x21\xff\x9f\x32\xcb\x7c\x20\xfb\x21\x26\xff\x0e"
                           "\x11\x3e\x80\x32\xe2\x0c\x3e\xf3\xe2\x32\x3e\x77\x77\x3e\xfc\xe0"
                           "\x47\x11\x04\x01\x21\x10\x80\x1a\xcd\x95\x00\xcd\x96\x00\x13\x7b"
                           "\xfe\x34\x20\xf3\x11\xd8\x00\x06\x08\x1a\x13\x22\x23\x05\x20\xf9"
                           "\x3e\x19\xea\x10\x99\x21\x2f\x99\x0e\x0c\x3d\x28\x08\x32\x0d\x20"
                           "\xf9\x2e\x0f\x18\xf3\x67\x3e\x64\x57\xe0\x42\x3e\x91\xe0\x40\x04"
                           "\x1e\x02\x0e\x0c\xf0\x44\xfe\x90\x20\xfa\x0d\x20\xf7\x1d\x20\xf2"
                           "\x0e\x13\x24\x7c\x1e\x83\xfe\x62\x28\x06\x1e\xc1\xfe\x64\x20\x06"
                           "\x7b\xe2\x0c\x3e\x87\xe2\xf0\x42\x90\xe0\x42\x15\x20\xd2\x05\x20"
                           "\x4f\x16\x20\x18\xcb\x4f\x06\x04\xc5\xcb\x11\x17\xc1\xcb\x11\x17"
                           "\x05\x20\xf5\x22\x23\x22\x23\xc9\xce\xed\x66\x66\xcc\x0d\x00\x0b"
                           "\x03\x73\x00\x83\x00\x0c\x00\x0d\x00\x08\x11\x1f\x88\x89\x00\x0e"
                           "\xdc\xcc\x6e\xe6\xdd\xdd\xd9\x99\xbb\xbb\x67\x63\x6e\x0e\xec\xcc"
                           "\xdd\xdc\x99\x9f\xbb\xb9\x33\x3e\x3c\x42\xb9\xa5\xb9\xa5\x42\x3c"
                           "\x21\x04\x01\x11\xa8\x00\x1a\x13\xbe\x20\xfe\x23\x7d\xfe\x34\x20"
                           "\xf5\x06\x19\x78\x86\x23\x05\x20\xfb\x86\x20\xfe\x3e\x01\xe0\x50";

const uint8_t CGB_BIOS[] = "\x31\xfe\xff\x3e\x02\xc3\x7c\x00\xd3\x00\x98\xa0\x12\xd3\x00\x80"
                           "\x00\x40\x1e\x53\xd0\x00\x1f\x42\x1c\x00\x14\x2a\x4d\x19\x8c\x7e"
                           "\x00\x7c\x31\x6e\x4a\x45\x52\x4a\x00\x00\xff\x53\x1f\x7c\xff\x03"
                           "\x1f\x00\xff\x1f\xa7\x00\xef\x1b\x1f\x00\xef\x1b\x00\x7c\x00\x00"
                           "\xff\x03\xce\xed\x66\x66\xcc\x0d\x00\x0b\x03\x73\x00\x83\x00\x0c"
                           "\x00\x0d\x00\x08\x11\x1f\x88\x89\x00\x0e\xdc\xcc\x6e\xe6\xdd\xdd"
                           "\xd9\x99\xbb\xbb\x67\x63\x6e\x0e\xec\xcc\xdd\xdc\x99\x9f\xbb\xb9"
                           "\x33\x3e\x3c\x42\xb9\xa5\xb9\xa5\x42\x3c\x58\x43\xe0\x70\x3e\xfc"
                           "\xe0\x47\xcd\x75\x02\xcd\x00\x02\x26\xd0\xcd\x03\x02\x21\x00\xfe"
                           "\x0e\xa0\xaf\x22\x0d\x20\xfc\x11\x04\x01\x21\x10\x80\x4c\x1a\xe2"
                           "\x0c\xcd\xc6\x03\xcd\xc7\x03\x13\x7b\xfe\x34\x20\xf1\x11\x72\x00"
                           "\x06\x08\x1a\x13\x22\x23\x05\x20\xf9\xcd\xf0\x03\x3e\x01\xe0\x4f"
                           "\x3e\x91\xe0\x40\x21\xb2\x98\x06\x4e\x0e\x44\xcd\x91\x02\xaf\xe0"
                           "\x4f\x0e\x80\x21\x42\x00\x06\x18\xf2\x0c\xbe\x20\xfe\x23\x05\x20"
                           "\xf7\x21\x34\x01\x06\x19\x78\x86\x2c\x05\x20\xfb\x86\x20\xfe\xcd"
                           "\x1c\x03\x18\x02\x00\x00\xcd\xd0\x05\xaf\xe0\x70\x3e\x11\xe0\x50"
                           "\x21\x00\x80\xaf\x22\xcb\x6c\x28\xfb\xc9\x2a\x12\x13\x0d\x20\xfa"
                           "\xc9\xe5\x21\x0f\xff\xcb\x86\xcb\x46\x28\xfc\xe1\xc9\x11\x00\xff"
                           "\x21\x03\xd0\x0e\x0f\x3e\x30\x12\x3e\x20\x12\x1a\x2f\xa1\xcb\x37"
                           "\x47\x3e\x10\x12\x1a\x2f\xa1\xb0\x4f\x7e\xa9\xe6\xf0\x47\x2a\xa9"
                           "\xa1\xb0\x32\x47\x79\x77\x3e\x30\x12\xc9\x3e\x80\xe0\x68\xe0\x6a"
                           "\x0e\x6b\x2a\xe2\x05\x20\xfb\x4a\x09\x43\x0e\x69\x2a\xe2\x05\x20"
                           "\xfb\xc9\xc5\xd5\xe5\x21\x00\xd8\x06\x01\x16\x3f\x1e\x40\xcd\x4a"
                           "\x02\xe1\xd1\xc1\xc9\x3e\x80\xe0\x26\xe0\x11\x3e\xf3\xe0\x12\xe0"
                           "\x25\x3e\x77\xe0\x24\x21\x30\xff\xaf\x0e\x10\x22\x2f\x0d\x20\xfb"
                           "\xc9\xcd\x11\x02\xcd\x62\x02\x79\xfe\x38\x20\x14\xe5\xaf\xe0\x4f"
                           "\x21\xa7\x99\x3e\x38\x22\x3c\xfe\x3f\x20\xfa\x3e\x01\xe0\x4f\xe1"
                           "\xc5\xe5\x21\x43\x01\xcb\x7e\xcc\x89\x05\xe1\xc1\xcd\x11\x02\x79"
                           "\xd6\x30\xd2\x06\x03\x79\xfe\x01\xca\x06\x03\x7d\xfe\xd1\x28\x21"
                           "\xc5\x06\x03\x0e\x01\x16\x03\x7e\xe6\xf8\xb1\x22\x15\x20\xf8\x0c"
                           "\x79\xfe\x06\x20\xf0\x11\x11\x00\x19\x05\x20\xe7\x11\xa1\xff\x19"
                           "\xc1\x04\x78\x1e\x83\xfe\x62\x28\x06\x1e\xc1\xfe\x64\x20\x07\x7b"
                           "\xe0\x13\x3e\x87\xe0\x14\xfa\x02\xd0\xfe\x00\x28\x0a\x3d\xea\x02"
                           "\xd0\x79\xfe\x01\xca\x91\x02\x0d\xc2\x91\x02\xc9\x0e\x26\xcd\x4a"
                           "\x03\xcd\x11\x02\xcd\x62\x02\x0d\x20\xf4\xcd\x11\x02\x3e\x01\xe0"
                           "\x4f\xcd\x3e\x03\xcd\x41\x03\xaf\xe0\x4f\xcd\x3e\x03\xc9\x21\x08"
                           "\x00\x11\x51\xff\x0e\x05\xcd\x0a\x02\xc9\xc5\xd5\xe5\x21\x40\xd8"
                           "\x0e\x20\x7e\xe6\x1f\xfe\x1f\x28\x01\x3c\x57\x2a\x07\x07\x07\xe6"
                           "\x07\x47\x3a\x07\x07\x07\xe6\x18\xb0\xfe\x1f\x28\x01\x3c\x0f\x0f"
                           "\x0f\x47\xe6\xe0\xb2\x22\x78\xe6\x03\x5f\x7e\x0f\x0f\xe6\x1f\xfe"
                           "\x1f\x28\x01\x3c\x07\x07\xb3\x22\x0d\x20\xc7\xe1\xd1\xc1\xc9\x0e"
                           "\x00\x1a\xe6\xf0\xcb\x49\x28\x02\xcb\x37\x47\x23\x7e\xb0\x22\x1a"
                           "\xe6\x0f\xcb\x49\x20\x02\xcb\x37\x47\x23\x7e\xb0\x22\x13\xcb\x41"
                           "\x28\x0d\xd5\x11\xf8\xff\xcb\x49\x28\x03\x11\x08\x00\x19\xd1\x0c"
                           "\x79\xfe\x18\x20\xcc\xc9\x47\xd5\x16\x04\x58\xcb\x10\x17\xcb\x13"
                           "\x17\x15\x20\xf6\xd1\x22\x23\x22\x23\xc9\x3e\x19\xea\x10\x99\x21"
                           "\x2f\x99\x0e\x0c\x3d\x28\x08\x32\x0d\x20\xf9\x2e\x0f\x18\xf3\xc9"
                           "\x3e\x01\xe0\x4f\xcd\x00\x02\x11\x07\x06\x21\x80\x80\x0e\xc0\x1a"
                           "\x22\x23\x22\x23\x13\x0d\x20\xf7\x11\x04\x01\xcd\x8f\x03\x01\xa8"
                           "\xff\x09\xcd\x8f\x03\x01\xf8\xff\x09\x11\x72\x00\x0e\x08\x23\x1a"
                           "\x22\x13\x0d\x20\xf9\x21\xc2\x98\x06\x08\x3e\x08\x0e\x10\x22\x0d"
                           "\x20\xfc\x11\x10\x00\x19\x05\x20\xf3\xaf\xe0\x4f\x21\xc2\x98\x3e"
                           "\x08\x22\x3c\xfe\x18\x20\x02\x2e\xe2\xfe\x28\x20\x03\x21\x02\x99"
                           "\xfe\x38\x20\xed\x21\xd8\x08\x11\x40\xd8\x06\x08\x3e\xff\x12\x13"
                           "\x12\x13\x0e\x02\xcd\x0a\x02\x3e\x00\x12\x13\x12\x13\x13\x13\x05"
                           "\x20\xea\xcd\x62\x02\x21\x4b\x01\x7e\xfe\x33\x20\x0b\x2e\x44\x1e"
                           "\x30\x2a\xbb\x20\x49\x1c\x18\x04\x2e\x4b\x1e\x01\x2a\xbb\x20\x3e"
                           "\x2e\x34\x01\x10\x00\x2a\x80\x47\x0d\x20\xfa\xea\x00\xd0\x21\xc7"
                           "\x06\x0e\x00\x2a\xb8\x28\x08\x0c\x79\xfe\x4f\x20\xf6\x18\x1f\x79"
                           "\xd6\x41\x38\x1c\x21\x16\x07\x16\x00\x5f\x19\xfa\x37\x01\x57\x7e"
                           "\xba\x28\x0d\x11\x0e\x00\x19\x79\x83\x4f\xd6\x5e\x38\xed\x0e\x00"
                           "\x21\x33\x07\x06\x00\x09\x7e\xe6\x1f\xea\x08\xd0\x7e\xe6\xe0\x07"
                           "\x07\x07\xea\x0b\xd0\xcd\xe9\x04\xc9\x11\x91\x07\x21\x00\xd9\xfa"
                           "\x0b\xd0\x47\x0e\x1e\xcb\x40\x20\x02\x13\x13\x1a\x22\x20\x02\x1b"
                           "\x1b\xcb\x48\x20\x02\x13\x13\x1a\x22\x13\x13\x20\x02\x1b\x1b\xcb"
                           "\x50\x28\x05\x1b\x2b\x1a\x22\x13\x1a\x22\x13\x0d\x20\xd7\x21\x00"
                           "\xd9\x11\x00\xda\xcd\x64\x05\xc9\x21\x12\x00\xfa\x05\xd0\x07\x07"
                           "\x06\x00\x4f\x09\x11\x40\xd8\x06\x08\xe5\x0e\x02\xcd\x0a\x02\x13"
                           "\x13\x13\x13\x13\x13\xe1\x05\x20\xf0\x11\x42\xd8\x0e\x02\xcd\x0a"
                           "\x02\x11\x4a\xd8\x0e\x02\xcd\x0a\x02\x2b\x2b\x11\x44\xd8\x0e\x02"
                           "\xcd\x0a\x02\xc9\x0e\x60\x2a\xe5\xc5\x21\xe8\x07\x06\x00\x4f\x09"
                           "\x0e\x08\xcd\x0a\x02\xc1\xe1\x0d\x20\xec\xc9\xfa\x08\xd0\x11\x18"
                           "\x00\x3c\x3d\x28\x03\x19\x20\xfa\xc9\xcd\x1d\x02\x78\xe6\xff\x28"
                           "\x0f\x21\xe4\x08\x06\x00\x2a\xb9\x28\x08\x04\x78\xfe\x0c\x20\xf6"
                           "\x18\x2d\x78\xea\x05\xd0\x3e\x1e\xea\x02\xd0\x11\x0b\x00\x19\x56"
                           "\x7a\xe6\x1f\x5f\x21\x08\xd0\x3a\x22\x7b\x77\x7a\xe6\xe0\x07\x07"
                           "\x07\x5f\x21\x0b\xd0\x3a\x22\x7b\x77\xcd\xe9\x04\xcd\x28\x05\xc9"
                           "\xcd\x11\x02\xfa\x43\x01\xcb\x7f\x28\x04\xe0\x4c\x18\x28\x3e\x04"
                           "\xe0\x4c\x3e\x01\xe0\x6c\x21\x00\xda\xcd\x7b\x05\x06\x10\x16\x00"
                           "\x1e\x08\xcd\x4a\x02\x21\x7a\x00\xfa\x00\xd0\x47\x0e\x02\x2a\xb8"
                           "\xcc\xda\x03\x0d\x20\xf8\xc9\x01\x0f\x3f\x7e\xff\xff\xc0\x00\xc0"
                           "\xf0\xf1\x03\x7c\xfc\xfe\xfe\x03\x07\x07\x0f\xe0\xe0\xf0\xf0\x1e"
                           "\x3e\x7e\xfe\x0f\x0f\x1f\x1f\xff\xff\x00\x00\x01\x01\x01\x03\xff"
                           "\xff\xe1\xe0\xc0\xf0\xf9\xfb\x1f\x7f\xf8\xe0\xf3\xfd\x3e\x1e\xe0"
                           "\xf0\xf9\x7f\x3e\x7c\xf8\xe0\xf8\xf0\xf0\xf8\x00\x00\x7f\x7f\x07"
                           "\x0f\x9f\xbf\x9e\x1f\xff\xff\x0f\x1e\x3e\x3c\xf1\xfb\x7f\x7f\xfe"
                           "\xde\xdf\x9f\x1f\x3f\x3e\x3c\xf8\xf8\x00\x00\x03\x03\x07\x07\xff"
                           "\xff\xc1\xc0\xf3\xe7\xf7\xf3\xc0\xc0\xc0\xc0\x1f\x1f\x1e\x3e\x3f"
                           "\x1f\x3e\x3e\x80\x00\x00\x00\x7c\x1f\x07\x00\x0f\xff\xfe\x00\x7c"
                           "\xf8\xf0\x00\x1f\x0f\x0f\x00\x7c\xf8\xf8\x00\x3f\x3e\x1c\x00\x0f"
                           "\x0f\x0f\x00\x7c\xff\xff\x00\x00\xf8\xf8\x00\x07\x0f\x0f\x00\x81"
                           "\xff\xff\x00\xf3\xe1\x80\x00\xe0\xff\x7f\x00\xfc\xf0\xc0\x00\x3e"
                           "\x7c\x7c\x00\x00\x00\x00\x00\x00\x88\x16\x36\xd1\xdb\xf2\x3c\x8c"
                           "\x92\x3d\x5c\x58\xc9\x3e\x70\x1d\x59\x69\x19\x35\xa8\x14\xaa\x75"
                           "\x95\x99\x34\x6f\x15\xff\x97\x4b\x90\x17\x10\x39\xf7\xf6\xa2\x49"
                           "\x4e\x43\x68\xe0\x8b\xf0\xce\x0c\x29\xe8\xb7\x86\x9a\x52\x01\x9d"
                           "\x71\x9c\xbd\x5d\x6d\x67\x3f\x6b\xb3\x46\x28\xa5\xc6\xd3\x27\x61"
                           "\x18\x66\x6a\xbf\x0d\xf4\x42\x45\x46\x41\x41\x52\x42\x45\x4b\x45"
                           "\x4b\x20\x52\x2d\x55\x52\x41\x52\x20\x49\x4e\x41\x49\x4c\x49\x43"
                           "\x45\x20\x52\x7c\x08\x12\xa3\xa2\x07\x87\x4b\x20\x12\x65\xa8\x16"
                           "\xa9\x86\xb1\x68\xa0\x87\x66\x12\xa1\x30\x3c\x12\x85\x12\x64\x1b"
                           "\x07\x06\x6f\x6e\x6e\xae\xaf\x6f\xb2\xaf\xb2\xa8\xab\x6f\xaf\x86"
                           "\xae\xa2\xa2\x12\xaf\x13\x12\xa1\x6e\xaf\xaf\xad\x06\x4c\x6e\xaf"
                           "\xaf\x12\x7c\xac\xa8\x6a\x6e\x13\xa0\x2d\xa8\x2b\xac\x64\xac\x6d"
                           "\x87\xbc\x60\xb4\x13\x72\x7c\xb5\xae\xae\x7c\x7c\x65\xa2\x6c\x64"
                           "\x85\x80\xb0\x40\x88\x20\x68\xde\x00\x70\xde\x20\x78\x20\x20\x38"
                           "\x20\xb0\x90\x20\xb0\xa0\xe0\xb0\xc0\x98\xb6\x48\x80\xe0\x50\x1e"
                           "\x1e\x58\x20\xb8\xe0\x88\xb0\x10\x20\x00\x10\x20\xe0\x18\xe0\x18"
                           "\x00\x18\xe0\x20\xa8\xe0\x20\x18\xe0\x00\x20\x18\xd8\xc8\x18\xe0"
                           "\x00\xe0\x40\x28\x28\x28\x18\xe0\x60\x20\x18\xe0\x00\x00\x08\xe0"
                           "\x18\x30\xd0\xd0\xd0\x20\xe0\xe8\xff\x7f\xbf\x32\xd0\x00\x00\x00"
                           "\x9f\x63\x79\x42\xb0\x15\xcb\x04\xff\x7f\x31\x6e\x4a\x45\x00\x00"
                           "\xff\x7f\xef\x1b\x00\x02\x00\x00\xff\x7f\x1f\x42\xf2\x1c\x00\x00"
                           "\xff\x7f\x94\x52\x4a\x29\x00\x00\xff\x7f\xff\x03\x2f\x01\x00\x00"
                           "\xff\x7f\xef\x03\xd6\x01\x00\x00\xff\x7f\xb5\x42\xc8\x3d\x00\x00"
                           "\x74\x7e\xff\x03\x80\x01\x00\x00\xff\x67\xac\x77\x13\x1a\x6b\x2d"
                           "\xd6\x7e\xff\x4b\x75\x21\x00\x00\xff\x53\x5f\x4a\x52\x7e\x00\x00"
                           "\xff\x4f\xd2\x7e\x4c\x3a\xe0\x1c\xed\x03\xff\x7f\x5f\x25\x00\x00"
                           "\x6a\x03\x1f\x02\xff\x03\xff\x7f\xff\x7f\xdf\x01\x12\x01\x00\x00"
                           "\x1f\x23\x5f\x03\xf2\x00\x09\x00\xff\x7f\xea\x03\x1f\x01\x00\x00"
                           "\x9f\x29\x1a\x00\x0c\x00\x00\x00\xff\x7f\x7f\x02\x1f\x00\x00\x00"
                           "\xff\x7f\xe0\x03\x06\x02\x20\x01\xff\x7f\xeb\x7e\x1f\x00\x00\x7c"
                           "\xff\x7f\xff\x3f\x00\x7e\x1f\x00\xff\x7f\xff\x03\x1f\x00\x00\x00"
                           "\xff\x03\x1f\x00\x0c\x00\x00\x00\xff\x7f\x3f\x03\x93\x01\x00\x00"
                           "\x00\x00\x00\x42\x7f\x03\xff\x7f\xff\x7f\x8c\x7e\x00\x7c\x00\x00"
                           "\xff\x7f\xef\x1b\x80\x61\x00\x00\xff\x7f\x00\x7c\xe0\x03\x1f\x7c"
                           "\x1f\x00\xff\x03\x40\x41\x42\x20\x21\x22\x80\x81\x82\x10\x11\x12"
                           "\x12\xb0\x79\xb8\xad\x16\x17\x07\xba\x05\x7c\x13\x00\x00\x00\x00";

MemoryMap::MemoryMap(Gameboy* _gb)
{
    fullBlock = nullptr;
    offsetVRAM = 0;
    offsetExternalRAM = 0;
    offsetInternalRAM = 0;
    offsetROM = 0;
    IME = false;
    gb = _gb;
    std::fill(&offsetTable[0], &offsetTable[16], 0);
    std::fill(&romRegisters[0], &romRegisters[8], 0);
}

MemoryMap::~MemoryMap(void)
{
    if (fullBlock != nullptr)
    {
        delete[] fullBlock;
        fullBlock = nullptr;
    }
}

void MemoryMap::set_external_clock(uint64_t extClock)
{
    for (size_t i = 0; i < 8; ++i) fullBlock[offsetExternalClock + i] = Reg8(extClock >> (i * 8));
}

uint64_t MemoryMap::get_external_clock(void)
{
    uint64_t extClock = 0;

    for (size_t i = 0; i < 8; ++i) extClock |= fullBlock[offsetExternalClock + i] << (i * 8);

    return extClock;
}

void MemoryMap::load_cartridge(const std::vector<char>& content)
{
    const CartInfo& info = gb->get_cartinfo();

    fullSize = 0x10000u;
    offsetVRAM = fullSize;

    fullSize += 0x4000u;
    offsetExternalRAM = fullSize;

    fullSize += info.sizeExternalRAM;
    offsetExternalClock = fullSize;

    // Reserve some space next to the external RAM for the MBC3 clock
    fullSize += (info.MBC == 3) ? 13 : 0;
    offsetInternalRAM = fullSize;

    fullSize += 0x8000u;
    offsetROM = fullSize;

    fullSize += info.sizeROM;
    fullBlock = new Reg8[fullSize];

    std::fill(fullBlock, fullBlock + offsetROM, 0);
    std::copy(content.begin(), content.end(), &fullBlock[offsetROM]);

    // Initialize the MBC3 clock if needed
    if (info.MBC == 3)
    {
        uint64_t extClock = uint64_t(time(NULL));
        set_external_clock(extClock);
        for (size_t i = 0; i < 5; ++i) fullBlock[offsetExternalClock + 8 + i] = 0;
    }

    reset();
}

void MemoryMap::reset(void)
{
    const CartInfo& info = gb->get_cartinfo();

    std::fill(fullBlock + offsetVRAM, fullBlock + offsetExternalRAM, 0);
    std::fill(fullBlock + offsetInternalRAM, fullBlock + offsetROM, 0);
    std::fill(fullBlock + 0xE000u, fullBlock + 0x10000u, 0);

    IME = false;
    biosRunning = true;

    // Reset the offset table
    for (uint32_t i = 0; i < 8; ++i) offsetTable[i] = offsetROM + i * 0x1000u;

    offsetTable[0x8] = offsetVRAM;
    offsetTable[0x9] = offsetVRAM + 0x1000u;

    if (info.sizeExternalRAM)
    {
        offsetTable[0xA] = offsetExternalRAM;
        offsetTable[0xB] = offsetExternalRAM + 0x1000u;
    }
    else
    {
        offsetTable[0xA] = 0xA000u;
        offsetTable[0xB] = 0xB000u;
    }

    offsetTable[0xC] = offsetInternalRAM;
    offsetTable[0xD] = offsetInternalRAM + 0x1000u;
    offsetTable[0xE] = 0xE000u;
    offsetTable[0xF] = 0xF000u;

    // Initialize GBC registers

    register_store(P1_ADDRESS, 0x3Fu);
    register_store(SC_ADDRESS, 0x02u);
    register_store(TIMA_ADDRESS, 0x00u);
    register_store(TAC_ADDRESS, 0x00u);
    register_store(IE_ADDRESS, 0x00u);
    register_store(NR11_ADDRESS, 0x80u);
    register_store(NR12_ADDRESS, 0xF3u);
    register_store(SC_ADDRESS, 0x02u);
    register_store(NR50_ADDRESS, 0x77u);
    register_store(NR51_ADDRESS, 0xF3u);
    register_store(NR52_ADDRESS, 0x81u);
    register_store(LCDC_ADDRESS, 0x83u);
    register_store(SCY_ADDRESS, 0x00u);
    register_store(SCX_ADDRESS, 0x00u);
    register_store(LYC_ADDRESS, 0x00u);
    register_store(WY_ADDRESS, 0x00u);
    register_store(WX_ADDRESS, 0x00u);
    register_store(IF_ADDRESS, 0x01u);
    register_store(IE_ADDRESS, 0x00u);
    register_store(DMA_ADDRESS, 0xFFu);
    register_store(BGP_ADDRESS, 0xFCu);
    register_store(OBP0_ADDRESS, 0xFFu);
    register_store(OBP1_ADDRESS, 0xFFu);
    register_store(KEY1_ADDRESS, 0x81u);
    register_store(HDMA5_ADDRESS, 0xFFu);
    register_store(RP_ADDRESS, 0xC3u);
}

void MemoryMap::load_state(const MemoryState& state)
{
    std::copy(state.ram.begin(), state.ram.end(), fullBlock + offsetVRAM);
    std::copy(&state.colorPalettes[0], &state.colorPalettes[64], fullBlock + 0xE000u);
    std::copy(&state.colorPalettes[64], &state.colorPalettes[128], fullBlock + 0xE100u);
    std::copy(&state.oamData[0], &state.oamData[160], fullBlock + 0xFE00u);
    std::copy(&state.ioRegisters[0], &state.ioRegisters[256], fullBlock + 0xFF00u);
    std::copy(&state.offsetTable[0], &state.offsetTable[16], &offsetTable[0]);
    std::copy(&state.romRegisters[0], &state.romRegisters[8], &romRegisters[0]);
    biosRunning = state.biosRunning;
    IME = state.IME;
}

void MemoryMap::save_state(MemoryState& state) const
{
    state.ram.assign(fullBlock + offsetVRAM, fullBlock + offsetROM);
    std::copy(fullBlock + 0xE000u, fullBlock + 0xE040u, &state.colorPalettes[0]);
    std::copy(fullBlock + 0xE100u, fullBlock + 0xE140u, &state.colorPalettes[64]);
    std::copy(fullBlock + 0xFE00u, fullBlock + 0xFEA0u, &state.oamData[0]);
    std::copy(fullBlock + 0xFF00u, fullBlock + 0x10000u, &state.ioRegisters[0]);
    std::copy(&offsetTable[0], &offsetTable[16], &state.offsetTable[0]);
    std::copy(&romRegisters[0], &romRegisters[8], &state.romRegisters[0]);
    state.biosRunning = biosRunning;
    state.IME = IME;
}

void MemoryMap::store(Reg16 address, Reg8 value)
{
    if (address < 0x8000u)
        write_mbc_register(reg16_hi(address), value);

    else if (address >= 0xFF00u)
        write_ctrl_register(reg16_lo(address), value);

    else if (address >= 0xA000u && address <= 0xBFFFu && gb->get_cartinfo().MBC == 3
             && romRegisters[2] >= 0x8u)
    {
        if (romRegisters[2] == 0x0Cu)
        {
            // Don't allow all bits to be set for the RTC_DH register
            value &= 0xC1u;

            // If the HALT bit gets unset, update the timestamp without updating the RTC
            if (!(value & BIT_6) && (fullBlock[offsetExternalClock + 0xC] & BIT_6))
            {
                uint64_t extClock = uint64_t(time(NULL));
                set_external_clock(extClock);
            }
        }

        fullBlock[offsetExternalClock + romRegisters[2]] = value;
    }

    else
    {
        Reg8 bank = address >> 12;
        Reg16 bankAddr = address & 0xFFFu;

        fullBlock[offsetTable[bank] + bankAddr] = value;
    }
}

const Reg8 ControlRegisterRMask[256] = {
    0xC0u,
    0x00u,
    0x7Cu,
    0xFFu,
    0x00u,
    0x00u,
    0x00u,
    0xF8u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xE0u,
    0x80u,
    0x3Fu,
    0x00u,
    0xFFu,
    0xBFu,
    0xFFu,
    0x3Fu,
    0x00u,
    0xFFu,
    0xBFu,
    0x7Fu,
    0xFFu,
    0x9Fu,
    0xFFu,
    0xBFu,
    0xFFu,
    0xFFu,
    0x00u,
    0x00u,
    0xBFu,
    0x00u,
    0x00u,
    0x70u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x80u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xFFu,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xFFu,
    0x7Eu,
    0xFFu,
    0xFEu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x00u,
    0x3Cu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x40u,
    0x00u,
    0x40u,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xF8u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xE0u,
};

void MemoryMap::load(Reg16 address, Reg8& value)
{
    // Special handling when the BIOS is mapped onto the existing ROM
    if (biosRunning)
    {
        bool cgb = gb->get_cartinfo().mode == CGB_MODE;

        if (address <= 0x0100u)
        {
            value = cgb ? CGB_BIOS[address] : DMG_BIOS[address];
            return;
        }
        else if (cgb && address >= 0x0200u && address <= 0x08FFu)
        {
            value = CGB_BIOS[address - 0x0100u];
            return;
        }
    }

    // Special case for RTC register reading in MBC3 cartridges
    if (address >= 0xA000u && address <= 0xBFFFu && gb->get_cartinfo().MBC == 3
        && romRegisters[2] >= 0x8u)
    {
        value = fullBlock[offsetExternalClock + romRegisters[2]];
        return;
    }

    if (address >= 0xFF00u)
    {
        value = fullBlock[address] | ControlRegisterRMask[reg16_lo(address)];
        return;
    }

    Reg8 bank = address >> 12;
    Reg16 bankAddr = address & 0xFFFu;

    value = fullBlock[offsetTable[bank] + bankAddr];
}

void MemoryMap::register_store(Reg16 address, Reg8 value) { fullBlock[address] = value; }

void MemoryMap::register_load(Reg16 address, Reg8& value) { value = fullBlock[address]; }

void MemoryMap::vram_load(Reg16 offset, bool bank1, Reg8& value)
{
    value = fullBlock[offsetVRAM + 0x2000 * bank1 + offset];
}

void MemoryMap::set_interrupt(Reg8 interruptBit)
{
    Reg8 ifReg;

    load(IF_ADDRESS, ifReg);
    ifReg |= interruptBit;
    store(IF_ADDRESS, ifReg);
}

const Reg8 ControlRegisterWMask[256] = {
    0x3Fu,
    0xFFu,
    0x83u,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0x07u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x1Fu,
    0x7Fu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xC7u,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xC7u,
    0x80u,
    0xFFu,
    0x60u,
    0xFFu,
    0xC7u,
    0x00u,
    0x3Fu,
    0xFFu,
    0xFFu,
    0xC0u,
    0xFFu,
    0xFFu,
    0x80u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x7Fu,
    0xFFu,
    0xFFu,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x00u,
    0x01u,
    0x00u,
    0x01u,
    0x00u,
    0xFFu,
    0xF0u,
    0x1Fu,
    0xF0u,
    0xFFu,
    0xC3u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xBFu,
    0x00u,
    0xBFu,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x07u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0x00u,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0xFFu,
    0x1Fu,
};

void MemoryMap::write_ctrl_register(Reg8 loAddr, Reg8 value)
{
    uint32_t where;
    Reg8 oldValue = fullBlock[0xFF00u + loAddr];

    fullBlock[0xFF00u + loAddr] = (fullBlock[0xFF00u + loAddr] & ~ControlRegisterWMask[loAddr])
                                  | (value & ControlRegisterWMask[loAddr]);

    switch (loAddr)
    {
        case 0x00u: // P1 register
            gb->event->check_normal();
            break;

        case 0x04u: // DIV register
            fullBlock[0xFF00u + loAddr] = 0;
            break;

        case 0x14u: // NR14 register
            if (value & BIT_7) gb->get_apu().initialize_square1();
            break;

        case 0x19u: // NR24 register
            if (value & BIT_7) gb->get_apu().initialize_square2();
            break;

        case 0x1Au: // NR30 register
            if (!(value & BIT_7))
            {
                Reg8 nr52;

                register_load(NR52_ADDRESS, nr52);
                nr52 &= ~BIT_2;
                register_store(NR52_ADDRESS, nr52);
            }
            break;

        case 0x1Eu: // NR34 register
            if (value & BIT_7) gb->get_apu().initialize_wave();
            break;

        case 0x21u: // NR42 register
            if (!(value & 0xF8))
            {
                Reg8 nr52;

                register_load(NR52_ADDRESS, nr52);
                nr52 &= ~BIT_3;
                register_store(NR52_ADDRESS, nr52);
            }
            break;

        case 0x23u: // NR44 register
            if (value & BIT_7) gb->get_apu().initialize_noise();
            break;

        case 0x46u: // DMA register
            // You cannot use DMA transfer on ROM addresses in DMG, or addresses above
            // 0xE000 at any time.
            if (gb->get_cartinfo().mode == DMG_MODE && value < 0x80u) break;
            if (value >= 0xE0u) break;
            where = offsetTable[value >> 4] + make_reg16(value & 0x0Fu, 0);
            std::copy(&fullBlock[where], &fullBlock[where + 160], &fullBlock[0xFE00u]);
            break;

        case 0x4Fu: // VBK register
            if (gb->get_cartinfo().mode == CGB_MODE)
            {
                Reg8 bank = value & BIT_0;

                offsetTable[0x8] = offsetVRAM + 0x2000u * bank;
                offsetTable[0x9] = offsetVRAM + 0x2000u * bank + 0x1000u;
            }
            break;

        case 0x50u: // BANK register, only used by the BIOS
            biosRunning = false;
            break;

        case 0x55u: // HDMA5 register
            if (gb->get_cartinfo().mode == CGB_MODE)
            {
                if (!(value & BIT_7))
                {
                    if (oldValue & BIT_7)
                        cgb_dma_transfer();
                    else
                        gb->hblank_dma_stop();
                }
                else
                    gb->hblank_dma_init();
            }
            break;

        case 0x68u: // BCPS register
            {
                Reg8 paletteIndex = value & 0x3Fu;
                register_store(BCPD_ADDRESS, fullBlock[0xE000u + paletteIndex]);
            }
            break;

        case 0x69u: // BCPD register
            if (gb->get_cartinfo().mode == CGB_MODE)
            {
                Reg8 bcpsReg;
                register_load(BCPS_ADDRESS, bcpsReg);
                Reg8 paletteIndex = bcpsReg & 0x3Fu;
                fullBlock[0xE000u + paletteIndex] = value;
                if (bcpsReg & BIT_7)
                {
                    paletteIndex = (paletteIndex + 1) & 0x3Fu;
                    register_store(BCPS_ADDRESS, paletteIndex | BIT_7);
                }
                fullBlock[0xFF00u + loAddr] = fullBlock[0xE000u + paletteIndex];
            }
            break;

        case 0x6Au: // OCPS register
            {
                Reg8 paletteIndex = value & 0x3Fu;
                register_store(OCPD_ADDRESS, fullBlock[0xE100u + paletteIndex]);
            }
            break;

        case 0x6Bu: // OCPD register
            if (gb->get_cartinfo().mode == CGB_MODE)
            {
                Reg8 ocpsReg;
                register_load(OCPS_ADDRESS, ocpsReg);
                Reg8 paletteIndex = ocpsReg & 0x3Fu;
                fullBlock[0xE100u + paletteIndex] = value;
                if (ocpsReg & BIT_7)
                {
                    paletteIndex = (paletteIndex + 1) & 0x3Fu;
                    register_store(OCPS_ADDRESS, paletteIndex | BIT_7);
                }
                fullBlock[0xFF00u + loAddr] = fullBlock[0xE100u + paletteIndex];
            }
            break;

        case 0x70u: // SVBK register
            if (gb->get_cartinfo().mode == CGB_MODE)
            {
                Reg8 internalBank = value & 0x07u;

                // Bank 0 cannot be selected for the 0xD000-0xDFFF address range.
                internalBank += !internalBank;

                offsetTable[0xD] = offsetInternalRAM + 0x1000u * internalBank;
            }
            break;

        default: break;
    }
}

void MemoryMap::write_mbc1(Reg8 hiAddr, Reg8 value)
{
    if (hiAddr < 0x20u)
        romRegisters[0] = (value & 0x0Fu) == 0x0Au;

    else if (hiAddr < 0x40u)
    {
        romRegisters[1] = value & 0x1Fu;
        goto __switch;
    }
    else if (hiAddr < 0x60u)
    {
        uint32_t romBank;

        romRegisters[2] = value & 0x03u;

__switch:
        romBank = romRegisters[1] | (romRegisters[2] << 5);
        romBank += romRegisters[1] == 0;
        romBank %= gb->get_cartinfo().sizeROM / ROM_BANK_SIZE;

        if (romRegisters[3] == 1)
        {
            offsetTable[0xA] = !(gb->get_cartinfo().sizeExternalRAM)
                                   ? 0xA000u // Just to avoid crashes
                                   : offsetExternalRAM + SRAM_BANK_SIZE * romRegisters[2];

            offsetTable[0x4] = offsetROM + (romBank & 0x1Fu) * ROM_BANK_SIZE;
        }
        else
        {
            offsetTable[0xA] = !(gb->get_cartinfo().sizeExternalRAM)
                                   ? 0xA000u // Just to avoid crashes
                                   : offsetExternalRAM;

            offsetTable[0x4] = offsetROM + romBank * ROM_BANK_SIZE;
        }

        offsetTable[0xB] = offsetTable[0xA] + 0x1000u;
        offsetTable[0x5] = offsetTable[0x4] + 0x1000u;
        offsetTable[0x6] = offsetTable[0x4] + 0x2000u;
        offsetTable[0x7] = offsetTable[0x4] + 0x3000u;
    }
    else
        romRegisters[3] = value & 0x01u;
}

void MemoryMap::write_mbc2(Reg8 hiAddr, Reg8 value)
{
    if (hiAddr < 0x20u)
        romRegisters[0] = (value & 0x0Fu) == 0xAu;

    else if (hiAddr < 0x40u)
    {
        uint32_t romBank;

        romRegisters[1] = value & 0x0Fu;
        romBank = romRegisters[1] + (romRegisters[1] == 0);
        romBank %= gb->get_cartinfo().sizeROM / ROM_BANK_SIZE;

        offsetTable[0x4] = offsetROM + romBank * ROM_BANK_SIZE;
        offsetTable[0x5] = offsetTable[0x4] + 0x1000u;
        offsetTable[0x6] = offsetTable[0x4] + 0x2000u;
        offsetTable[0x7] = offsetTable[0x4] + 0x3000u;
    }
}

void MemoryMap::write_mbc3(Reg8 hiAddr, Reg8 value)
{
    if (hiAddr < 0x20u)
        romRegisters[0] = (value & 0x0Fu) == 0xAu;

    else if (hiAddr < 0x40u)
    {
        uint32_t romBank;

        romRegisters[1] = value & 0x7Fu;
        romBank = romRegisters[1] + (romRegisters[1] == 0);
        romBank %= gb->get_cartinfo().sizeROM / ROM_BANK_SIZE;

        offsetTable[0x4] = offsetROM + romBank * ROM_BANK_SIZE;
        offsetTable[0x5] = offsetTable[0x4] + 0x1000u;
        offsetTable[0x6] = offsetTable[0x4] + 0x2000u;
        offsetTable[0x7] = offsetTable[0x4] + 0x3000u;
    }

    else if (hiAddr < 0x60u)
    {
        if (value <= 0x03u)
        {
            romRegisters[2] = value;
            offsetTable[0xA] = !(gb->get_cartinfo().sizeExternalRAM)
                                   ? 0xA000u // Just to avoid crashes
                                   : offsetExternalRAM + romRegisters[2] * SRAM_BANK_SIZE;
            offsetTable[0xB] = offsetTable[0xA] + 0x1000u;
        }
        else if (value >= 0x08u && value <= 0x0Cu)
        {
            // Note: this is necessary for selecting the RTC register during
            // a later read/write at addresses 0xA000-0xBFFF.
            romRegisters[2] = value;
        }
    }
    else
    {
        if (romRegisters[3] == 0 && value == 1 && !(fullBlock[offsetExternalClock + 0xC] & BIT_6))
        {
            uint64_t extClock = get_external_clock();
            uint64_t curClock = uint64_t(time(NULL));
            bool carry;

            // Update all RTC registers.
            {
                uint64_t seconds = fullBlock[offsetExternalClock + 0x8];

                seconds += (curClock - extClock) % 60;
                fullBlock[offsetExternalClock + 0x8] = seconds % 60;
                carry = (seconds >= 60);
            }

            {
                uint64_t minutes = fullBlock[offsetExternalClock + 0x9];

                minutes += (curClock - extClock) / 60 % 60 + uint64_t(carry);
                fullBlock[offsetExternalClock + 0x9] = minutes % 60;
                carry = (minutes >= 60);
            }

            {
                uint64_t hours = fullBlock[offsetExternalClock + 0xA];

                hours += (curClock - extClock) / 3600 % 60 + uint64_t(carry);
                fullBlock[offsetExternalClock + 0xA] = hours % 60;
                carry = (hours >= 60);
            }

            {
                uint64_t days = fullBlock[offsetExternalClock + 0xB];

                days += uint64_t(fullBlock[offsetExternalClock + 0xC] & BIT_0) << 8;
                days += (curClock - extClock) / 86400 + uint64_t(carry);
                fullBlock[offsetExternalClock + 0xB] = Reg8(days);

                fullBlock[offsetExternalClock + 0xC] &= BIT_7;
                fullBlock[offsetExternalClock + 0xC] |= (days & 0x100u) >> 8;
                if (days >= 0x200u) fullBlock[offsetExternalClock + 0xC] |= BIT_7;
            }

            set_external_clock(curClock);
        }
        romRegisters[3] = value;
    }
}

void MemoryMap::write_mbc5(Reg8 hiAddr, Reg8 value)
{
    if (hiAddr < 0x20u)
        romRegisters[0] = (value & 0x0Fu) == 0x0Au;

    else if (hiAddr < 0x30u)
    {
        romRegisters[1] = value;
        goto __switch_rom;
    }
    else if (hiAddr < 0x40u)
    {
        romRegisters[2] = value & 0x01u;
        goto __switch_rom;
    }
    else if (hiAddr < 0x60)
    {
        romRegisters[3] = value & 0x0Fu;

        offsetTable[0xA] = (gb->get_cartinfo().sizeExternalRAM == 0)
                               ? 0xA000u // Just to avoid crashes
                               : offsetExternalRAM + romRegisters[3] * SRAM_BANK_SIZE;
        offsetTable[0xB] = offsetTable[0xA] + 0x1000u;
        return;
    }

__switch_rom:
    {
        uint32_t romBank;

        romBank = romRegisters[1] | (romRegisters[2] << 8);
        romBank %= gb->get_cartinfo().sizeROM / ROM_BANK_SIZE;

        offsetTable[0x4] = offsetROM + romBank * ROM_BANK_SIZE;
        offsetTable[0x5] = offsetTable[0x4] + 0x1000u;
        offsetTable[0x6] = offsetTable[0x4] + 0x2000u;
        offsetTable[0x7] = offsetTable[0x4] + 0x3000u;
    }
}

void MemoryMap::write_mbc_register(Reg8 hiAddr, Reg8 value)
{
    switch (gb->get_cartinfo().MBC)
    {
        case 1: write_mbc1(hiAddr, value); break;

        case 2: write_mbc2(hiAddr, value); break;

        case 3: write_mbc3(hiAddr, value); break;

        case 5: write_mbc5(hiAddr, value); break;
    }
}

void MemoryMap::cgb_dma_transfer()
{
    Reg8 hiSrcAddr, loSrcAddr;
    Reg8 hiDstAddr, loDstAddr;
    Reg8 quantity;

    register_load(HDMA1_ADDRESS, hiSrcAddr);
    register_load(HDMA2_ADDRESS, loSrcAddr);
    register_load(HDMA3_ADDRESS, hiDstAddr);
    register_load(HDMA4_ADDRESS, loDstAddr);
    register_load(HDMA5_ADDRESS, quantity);

    loSrcAddr &= 0xF0u;
    hiDstAddr &= 0x1Fu;
    loDstAddr &= 0xF0u;
    quantity = (quantity & 0x7Fu) + 1;

    Reg16 srcAddr = make_reg16(hiSrcAddr, loSrcAddr);
    Reg16 dstAddr = make_reg16(hiDstAddr, loDstAddr);
    Reg16 byteCount = Reg16(quantity) * 16;

    for (Reg16 i = 0; i < byteCount; ++i)
    {
        Reg8 value;

        load(srcAddr + i, value);
        store(0x8000u + dstAddr + i, value);
    }

    register_store(HDMA5_ADDRESS, 0xFFu);
}
