#include "event.h"
#include <QDebug>

void Event::set_key_state(GB_Key key, bool state)
{
    if (key > GB_KEY_COUNT) return;
    keyStatus[key] = state;
}

void Event::set_button_state(GB_Key button, bool state)
{
    if (button > GB_KEY_COUNT) return;
    buttonStatus[button] = state;
}

void Event::check_normal(void)
{
    bool keyInterrupt = false;
    Reg8 p1Reg;

    // Clear all input bits of the P1 register

    map->register_load(P1_ADDRESS, p1Reg);
    p1Reg |= BIT4_LO;

    // Check for directional inputs

    if ((p1Reg & (BIT_4 | BIT_5)) == BIT_5)
    {
        if (keyStatus[GB_KEY_UP] || buttonStatus[GB_KEY_UP])
        {
            p1Reg &= ~BIT_2;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_DOWN] || buttonStatus[GB_KEY_DOWN])
        {
            p1Reg &= ~BIT_3;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_LEFT] || buttonStatus[GB_KEY_LEFT])
        {
            p1Reg &= ~BIT_1;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_RIGHT] || buttonStatus[GB_KEY_RIGHT])
        {
            p1Reg &= ~BIT_0;
            keyInterrupt = true;
        }
    }

    // Check for button inputs

    if ((p1Reg & (BIT_4 | BIT_5)) == BIT_4)
    {
        if (keyStatus[GB_KEY_START] || buttonStatus[GB_KEY_START])
        {
            p1Reg &= ~BIT_3;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_SELECT] || buttonStatus[GB_KEY_SELECT])
        {
            p1Reg &= ~BIT_2;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_A] || buttonStatus[GB_KEY_A])
        {
            p1Reg &= ~BIT_0;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_B] || buttonStatus[GB_KEY_B])
        {
            p1Reg &= ~BIT_1;
            keyInterrupt = true;
        }
    }

    // Set interrupt flag if a key has been pressed
    if (keyInterrupt) map->set_interrupt(BIT_4);

    map->register_store(P1_ADDRESS, p1Reg);
}

void Event::check_halted(void)
{
    bool keyInterrupt = false;
    Reg8 p1Reg;

    // Clear all input bits of the P1 register

    map->register_load(P1_ADDRESS, p1Reg);
    p1Reg |= BIT4_LO;

    // Check for directional inputs

    if ((p1Reg & (BIT_4 | BIT_5)) == BIT_5)
    {
        if (keyStatus[GB_KEY_UP] || buttonStatus[GB_KEY_UP])
        {
            p1Reg &= ~BIT_2;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_DOWN] || buttonStatus[GB_KEY_DOWN])
        {
            p1Reg &= ~BIT_3;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_LEFT] || buttonStatus[GB_KEY_LEFT])
        {
            p1Reg &= ~BIT_1;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_RIGHT] || buttonStatus[GB_KEY_RIGHT])
        {
            p1Reg &= ~BIT_0;
            keyInterrupt = true;
        }
    }

    // Check for button inputs

    if ((p1Reg & (BIT_4 | BIT_5)) == BIT_4)
    {
        if (keyStatus[GB_KEY_START] || buttonStatus[GB_KEY_START])
        {
            p1Reg &= ~BIT_3;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_SELECT] || buttonStatus[GB_KEY_SELECT])
        {
            p1Reg &= ~BIT_2;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_A] || buttonStatus[GB_KEY_A])
        {
            p1Reg &= ~BIT_0;
            keyInterrupt = true;
        }
        if (keyStatus[GB_KEY_B] || buttonStatus[GB_KEY_B])
        {
            p1Reg &= ~BIT_1;
            keyInterrupt = true;
        }
    }

    map->register_store(P1_ADDRESS, p1Reg);

    // Set interrupt flag if a key has been pressed
    if (keyInterrupt) map->set_interrupt(BIT_4);
}
