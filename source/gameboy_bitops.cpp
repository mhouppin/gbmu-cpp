#include "gameboy.h"

Cycle Gameboy::execute_bitop(Reg8 opcode)
{
    Reg8 memByte;

    switch (opcode)
    {
        case 0x00: return cpu->rlc_r8(cpu->B);

        case 0x01: return cpu->rlc_r8(cpu->C);

        case 0x02: return cpu->rlc_r8(cpu->D);

        case 0x03: return cpu->rlc_r8(cpu->E);

        case 0x04: return cpu->rlc_r8(cpu->H);

        case 0x05: return cpu->rlc_r8(cpu->L);

        case 0x06:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->rlc_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x07: return cpu->rlc_r8(cpu->A);

        case 0x08: return cpu->rrc_r8(cpu->B);

        case 0x09: return cpu->rrc_r8(cpu->C);

        case 0x0A: return cpu->rrc_r8(cpu->D);

        case 0x0B: return cpu->rrc_r8(cpu->E);

        case 0x0C: return cpu->rrc_r8(cpu->H);

        case 0x0D: return cpu->rrc_r8(cpu->L);

        case 0x0E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->rrc_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x0F: return cpu->rrc_r8(cpu->A);

        case 0x10: return cpu->rl_r8(cpu->B);

        case 0x11: return cpu->rl_r8(cpu->C);

        case 0x12: return cpu->rl_r8(cpu->D);

        case 0x13: return cpu->rl_r8(cpu->E);

        case 0x14: return cpu->rl_r8(cpu->H);

        case 0x15: return cpu->rl_r8(cpu->L);

        case 0x16:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->rl_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x17: return cpu->rl_r8(cpu->A);

        case 0x18: return cpu->rr_r8(cpu->B);

        case 0x19: return cpu->rr_r8(cpu->C);

        case 0x1A: return cpu->rr_r8(cpu->D);

        case 0x1B: return cpu->rr_r8(cpu->E);

        case 0x1C: return cpu->rr_r8(cpu->H);

        case 0x1D: return cpu->rr_r8(cpu->L);

        case 0x1E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->rr_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x1F: return cpu->rr_r8(cpu->A);

        case 0x20: return cpu->sla_r8(cpu->B);

        case 0x21: return cpu->sla_r8(cpu->C);

        case 0x22: return cpu->sla_r8(cpu->D);

        case 0x23: return cpu->sla_r8(cpu->E);

        case 0x24: return cpu->sla_r8(cpu->H);

        case 0x25: return cpu->sla_r8(cpu->L);

        case 0x26:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->sla_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x27: return cpu->sla_r8(cpu->A);

        case 0x28: return cpu->sra_r8(cpu->B);

        case 0x29: return cpu->sra_r8(cpu->C);

        case 0x2A: return cpu->sra_r8(cpu->D);

        case 0x2B: return cpu->sra_r8(cpu->E);

        case 0x2C: return cpu->sra_r8(cpu->H);

        case 0x2D: return cpu->sra_r8(cpu->L);

        case 0x2E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->sra_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x2F: return cpu->sra_r8(cpu->A);

        case 0x30: return cpu->swap_r8(cpu->B);

        case 0x31: return cpu->swap_r8(cpu->C);

        case 0x32: return cpu->swap_r8(cpu->D);

        case 0x33: return cpu->swap_r8(cpu->E);

        case 0x34: return cpu->swap_r8(cpu->H);

        case 0x35: return cpu->swap_r8(cpu->L);

        case 0x36:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->swap_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x37: return cpu->swap_r8(cpu->A);

        case 0x38: return cpu->srl_r8(cpu->B);

        case 0x39: return cpu->srl_r8(cpu->C);

        case 0x3A: return cpu->srl_r8(cpu->D);

        case 0x3B: return cpu->srl_r8(cpu->E);

        case 0x3C: return cpu->srl_r8(cpu->H);

        case 0x3D: return cpu->srl_r8(cpu->L);

        case 0x3E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->srl_r8(memByte);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x3F: return cpu->srl_r8(cpu->A);

        case 0x40: return cpu->test_bit_r8(cpu->B, BIT_0);

        case 0x41: return cpu->test_bit_r8(cpu->C, BIT_0);

        case 0x42: return cpu->test_bit_r8(cpu->D, BIT_0);

        case 0x43: return cpu->test_bit_r8(cpu->E, BIT_0);

        case 0x44: return cpu->test_bit_r8(cpu->H, BIT_0);

        case 0x45: return cpu->test_bit_r8(cpu->L, BIT_0);

        case 0x46:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_0);
            return 3 * ONE_CYCLE;

        case 0x47: return cpu->test_bit_r8(cpu->A, BIT_0);

        case 0x48: return cpu->test_bit_r8(cpu->B, BIT_1);

        case 0x49: return cpu->test_bit_r8(cpu->C, BIT_1);

        case 0x4A: return cpu->test_bit_r8(cpu->D, BIT_1);

        case 0x4B: return cpu->test_bit_r8(cpu->E, BIT_1);

        case 0x4C: return cpu->test_bit_r8(cpu->H, BIT_1);

        case 0x4D: return cpu->test_bit_r8(cpu->L, BIT_1);

        case 0x4E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_1);
            return 3 * ONE_CYCLE;

        case 0x4F: return cpu->test_bit_r8(cpu->A, BIT_1);

        case 0x50: return cpu->test_bit_r8(cpu->B, BIT_2);

        case 0x51: return cpu->test_bit_r8(cpu->C, BIT_2);

        case 0x52: return cpu->test_bit_r8(cpu->D, BIT_2);

        case 0x53: return cpu->test_bit_r8(cpu->E, BIT_2);

        case 0x54: return cpu->test_bit_r8(cpu->H, BIT_2);

        case 0x55: return cpu->test_bit_r8(cpu->L, BIT_2);

        case 0x56:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_2);
            return 3 * ONE_CYCLE;

        case 0x57: return cpu->test_bit_r8(cpu->A, BIT_2);

        case 0x58: return cpu->test_bit_r8(cpu->B, BIT_3);

        case 0x59: return cpu->test_bit_r8(cpu->C, BIT_3);

        case 0x5A: return cpu->test_bit_r8(cpu->D, BIT_3);

        case 0x5B: return cpu->test_bit_r8(cpu->E, BIT_3);

        case 0x5C: return cpu->test_bit_r8(cpu->H, BIT_3);

        case 0x5D: return cpu->test_bit_r8(cpu->L, BIT_3);

        case 0x5E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_3);
            return 3 * ONE_CYCLE;

        case 0x5F: return cpu->test_bit_r8(cpu->A, BIT_3);

        case 0x60: return cpu->test_bit_r8(cpu->B, BIT_4);

        case 0x61: return cpu->test_bit_r8(cpu->C, BIT_4);

        case 0x62: return cpu->test_bit_r8(cpu->D, BIT_4);

        case 0x63: return cpu->test_bit_r8(cpu->E, BIT_4);

        case 0x64: return cpu->test_bit_r8(cpu->H, BIT_4);

        case 0x65: return cpu->test_bit_r8(cpu->L, BIT_4);

        case 0x66:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_4);
            return 3 * ONE_CYCLE;

        case 0x67: return cpu->test_bit_r8(cpu->A, BIT_4);

        case 0x68: return cpu->test_bit_r8(cpu->B, BIT_5);

        case 0x69: return cpu->test_bit_r8(cpu->C, BIT_5);

        case 0x6A: return cpu->test_bit_r8(cpu->D, BIT_5);

        case 0x6B: return cpu->test_bit_r8(cpu->E, BIT_5);

        case 0x6C: return cpu->test_bit_r8(cpu->H, BIT_5);

        case 0x6D: return cpu->test_bit_r8(cpu->L, BIT_5);

        case 0x6E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_5);
            return 3 * ONE_CYCLE;

        case 0x6F: return cpu->test_bit_r8(cpu->A, BIT_5);

        case 0x70: return cpu->test_bit_r8(cpu->B, BIT_6);

        case 0x71: return cpu->test_bit_r8(cpu->C, BIT_6);

        case 0x72: return cpu->test_bit_r8(cpu->D, BIT_6);

        case 0x73: return cpu->test_bit_r8(cpu->E, BIT_6);

        case 0x74: return cpu->test_bit_r8(cpu->H, BIT_6);

        case 0x75: return cpu->test_bit_r8(cpu->L, BIT_6);

        case 0x76:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_6);
            return 3 * ONE_CYCLE;

        case 0x77: return cpu->test_bit_r8(cpu->A, BIT_6);

        case 0x78: return cpu->test_bit_r8(cpu->B, BIT_7);

        case 0x79: return cpu->test_bit_r8(cpu->C, BIT_7);

        case 0x7A: return cpu->test_bit_r8(cpu->D, BIT_7);

        case 0x7B: return cpu->test_bit_r8(cpu->E, BIT_7);

        case 0x7C: return cpu->test_bit_r8(cpu->H, BIT_7);

        case 0x7D: return cpu->test_bit_r8(cpu->L, BIT_7);

        case 0x7E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->test_bit_r8(memByte, BIT_7);
            return 3 * ONE_CYCLE;

        case 0x7F: return cpu->test_bit_r8(cpu->A, BIT_7);

        case 0x80: return cpu->reset_bit_r8(cpu->B, BIT_0);

        case 0x81: return cpu->reset_bit_r8(cpu->C, BIT_0);

        case 0x82: return cpu->reset_bit_r8(cpu->D, BIT_0);

        case 0x83: return cpu->reset_bit_r8(cpu->E, BIT_0);

        case 0x84: return cpu->reset_bit_r8(cpu->H, BIT_0);

        case 0x85: return cpu->reset_bit_r8(cpu->L, BIT_0);

        case 0x86:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_0);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x87: return cpu->reset_bit_r8(cpu->A, BIT_0);

        case 0x88: return cpu->reset_bit_r8(cpu->B, BIT_1);

        case 0x89: return cpu->reset_bit_r8(cpu->C, BIT_1);

        case 0x8A: return cpu->reset_bit_r8(cpu->D, BIT_1);

        case 0x8B: return cpu->reset_bit_r8(cpu->E, BIT_1);

        case 0x8C: return cpu->reset_bit_r8(cpu->H, BIT_1);

        case 0x8D: return cpu->reset_bit_r8(cpu->L, BIT_1);

        case 0x8E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_1);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x8F: return cpu->reset_bit_r8(cpu->A, BIT_1);

        case 0x90: return cpu->reset_bit_r8(cpu->B, BIT_2);

        case 0x91: return cpu->reset_bit_r8(cpu->C, BIT_2);

        case 0x92: return cpu->reset_bit_r8(cpu->D, BIT_2);

        case 0x93: return cpu->reset_bit_r8(cpu->E, BIT_2);

        case 0x94: return cpu->reset_bit_r8(cpu->H, BIT_2);

        case 0x95: return cpu->reset_bit_r8(cpu->L, BIT_2);

        case 0x96:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_2);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x97: return cpu->reset_bit_r8(cpu->A, BIT_2);

        case 0x98: return cpu->reset_bit_r8(cpu->B, BIT_3);

        case 0x99: return cpu->reset_bit_r8(cpu->C, BIT_3);

        case 0x9A: return cpu->reset_bit_r8(cpu->D, BIT_3);

        case 0x9B: return cpu->reset_bit_r8(cpu->E, BIT_3);

        case 0x9C: return cpu->reset_bit_r8(cpu->H, BIT_3);

        case 0x9D: return cpu->reset_bit_r8(cpu->L, BIT_3);

        case 0x9E:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_3);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0x9F: return cpu->reset_bit_r8(cpu->A, BIT_3);

        case 0xA0: return cpu->reset_bit_r8(cpu->B, BIT_4);

        case 0xA1: return cpu->reset_bit_r8(cpu->C, BIT_4);

        case 0xA2: return cpu->reset_bit_r8(cpu->D, BIT_4);

        case 0xA3: return cpu->reset_bit_r8(cpu->E, BIT_4);

        case 0xA4: return cpu->reset_bit_r8(cpu->H, BIT_4);

        case 0xA5: return cpu->reset_bit_r8(cpu->L, BIT_4);

        case 0xA6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_4);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xA7: return cpu->reset_bit_r8(cpu->A, BIT_4);

        case 0xA8: return cpu->reset_bit_r8(cpu->B, BIT_5);

        case 0xA9: return cpu->reset_bit_r8(cpu->C, BIT_5);

        case 0xAA: return cpu->reset_bit_r8(cpu->D, BIT_5);

        case 0xAB: return cpu->reset_bit_r8(cpu->E, BIT_5);

        case 0xAC: return cpu->reset_bit_r8(cpu->H, BIT_5);

        case 0xAD: return cpu->reset_bit_r8(cpu->L, BIT_5);

        case 0xAE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_5);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xAF: return cpu->reset_bit_r8(cpu->A, BIT_5);

        case 0xB0: return cpu->reset_bit_r8(cpu->B, BIT_6);

        case 0xB1: return cpu->reset_bit_r8(cpu->C, BIT_6);

        case 0xB2: return cpu->reset_bit_r8(cpu->D, BIT_6);

        case 0xB3: return cpu->reset_bit_r8(cpu->E, BIT_6);

        case 0xB4: return cpu->reset_bit_r8(cpu->H, BIT_6);

        case 0xB5: return cpu->reset_bit_r8(cpu->L, BIT_6);

        case 0xB6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_6);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xB7: return cpu->reset_bit_r8(cpu->A, BIT_6);

        case 0xB8: return cpu->reset_bit_r8(cpu->B, BIT_7);

        case 0xB9: return cpu->reset_bit_r8(cpu->C, BIT_7);

        case 0xBA: return cpu->reset_bit_r8(cpu->D, BIT_7);

        case 0xBB: return cpu->reset_bit_r8(cpu->E, BIT_7);

        case 0xBC: return cpu->reset_bit_r8(cpu->H, BIT_7);

        case 0xBD: return cpu->reset_bit_r8(cpu->L, BIT_7);

        case 0xBE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->reset_bit_r8(memByte, BIT_7);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xBF: return cpu->reset_bit_r8(cpu->A, BIT_7);

        case 0xC0: return cpu->set_bit_r8(cpu->B, BIT_0);

        case 0xC1: return cpu->set_bit_r8(cpu->C, BIT_0);

        case 0xC2: return cpu->set_bit_r8(cpu->D, BIT_0);

        case 0xC3: return cpu->set_bit_r8(cpu->E, BIT_0);

        case 0xC4: return cpu->set_bit_r8(cpu->H, BIT_0);

        case 0xC5: return cpu->set_bit_r8(cpu->L, BIT_0);

        case 0xC6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_0);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xC7: return cpu->set_bit_r8(cpu->A, BIT_0);

        case 0xC8: return cpu->set_bit_r8(cpu->B, BIT_1);

        case 0xC9: return cpu->set_bit_r8(cpu->C, BIT_1);

        case 0xCA: return cpu->set_bit_r8(cpu->D, BIT_1);

        case 0xCB: return cpu->set_bit_r8(cpu->E, BIT_1);

        case 0xCC: return cpu->set_bit_r8(cpu->H, BIT_1);

        case 0xCD: return cpu->set_bit_r8(cpu->L, BIT_1);

        case 0xCE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_1);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xCF: return cpu->set_bit_r8(cpu->A, BIT_1);

        case 0xD0: return cpu->set_bit_r8(cpu->B, BIT_2);

        case 0xD1: return cpu->set_bit_r8(cpu->C, BIT_2);

        case 0xD2: return cpu->set_bit_r8(cpu->D, BIT_2);

        case 0xD3: return cpu->set_bit_r8(cpu->E, BIT_2);

        case 0xD4: return cpu->set_bit_r8(cpu->H, BIT_2);

        case 0xD5: return cpu->set_bit_r8(cpu->L, BIT_2);

        case 0xD6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_2);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xD7: return cpu->set_bit_r8(cpu->A, BIT_2);

        case 0xD8: return cpu->set_bit_r8(cpu->B, BIT_3);

        case 0xD9: return cpu->set_bit_r8(cpu->C, BIT_3);

        case 0xDA: return cpu->set_bit_r8(cpu->D, BIT_3);

        case 0xDB: return cpu->set_bit_r8(cpu->E, BIT_3);

        case 0xDC: return cpu->set_bit_r8(cpu->H, BIT_3);

        case 0xDD: return cpu->set_bit_r8(cpu->L, BIT_3);

        case 0xDE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_3);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xDF: return cpu->set_bit_r8(cpu->A, BIT_3);

        case 0xE0: return cpu->set_bit_r8(cpu->B, BIT_4);

        case 0xE1: return cpu->set_bit_r8(cpu->C, BIT_4);

        case 0xE2: return cpu->set_bit_r8(cpu->D, BIT_4);

        case 0xE3: return cpu->set_bit_r8(cpu->E, BIT_4);

        case 0xE4: return cpu->set_bit_r8(cpu->H, BIT_4);

        case 0xE5: return cpu->set_bit_r8(cpu->L, BIT_4);

        case 0xE6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_4);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xE7: return cpu->set_bit_r8(cpu->A, BIT_4);

        case 0xE8: return cpu->set_bit_r8(cpu->B, BIT_5);

        case 0xE9: return cpu->set_bit_r8(cpu->C, BIT_5);

        case 0xEA: return cpu->set_bit_r8(cpu->D, BIT_5);

        case 0xEB: return cpu->set_bit_r8(cpu->E, BIT_5);

        case 0xEC: return cpu->set_bit_r8(cpu->H, BIT_5);

        case 0xED: return cpu->set_bit_r8(cpu->L, BIT_5);

        case 0xEE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_5);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xEF: return cpu->set_bit_r8(cpu->A, BIT_5);

        case 0xF0: return cpu->set_bit_r8(cpu->B, BIT_6);

        case 0xF1: return cpu->set_bit_r8(cpu->C, BIT_6);

        case 0xF2: return cpu->set_bit_r8(cpu->D, BIT_6);

        case 0xF3: return cpu->set_bit_r8(cpu->E, BIT_6);

        case 0xF4: return cpu->set_bit_r8(cpu->H, BIT_6);

        case 0xF5: return cpu->set_bit_r8(cpu->L, BIT_6);

        case 0xF6:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_6);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xF7: return cpu->set_bit_r8(cpu->A, BIT_6);

        case 0xF8: return cpu->set_bit_r8(cpu->B, BIT_7);

        case 0xF9: return cpu->set_bit_r8(cpu->C, BIT_7);

        case 0xFA: return cpu->set_bit_r8(cpu->D, BIT_7);

        case 0xFB: return cpu->set_bit_r8(cpu->E, BIT_7);

        case 0xFC: return cpu->set_bit_r8(cpu->H, BIT_7);

        case 0xFD: return cpu->set_bit_r8(cpu->L, BIT_7);

        case 0xFE:
            map->load(make_reg16(cpu->H, cpu->L), memByte);
            cpu->set_bit_r8(memByte, BIT_7);
            map->store(make_reg16(cpu->H, cpu->L), memByte);
            break;

        case 0xFF: return cpu->set_bit_r8(cpu->A, BIT_7);
    }

    return 4 * ONE_CYCLE;
}
