#include "screen.h"
#include "gameboy.h"
#include <algorithm>

Screen::Screen(Gameboy* _gb, MemoryMap* _map, int32_t* _winPixels)
{
    winPixels = _winPixels;
    cycles = ZERO_CYCLE;
    status = HZ_BLANKING;
    curLine = 0;
    gb = _gb;
    map = _map;
    updateImage = false;
    quantity = 0;

    for (size_t i = 0; i < 160 * 144; ++i)
        winPixels[i] = 0x000000;
}

void Screen::reset(void)
{
    cycles = ZERO_CYCLE;
    status = HZ_BLANKING;
    curLine = 0;
    updateImage = false;
    quantity = 0;

    for (size_t i = 0; i < 160 * 144; ++i)
        winPixels[i] = 0x000000;
}

void Screen::load_state(const ScreenState& state)
{
    cycles = state.cycles;
    status = state.status;
    curLine = state.curLine;
    quantity = state.quantity;
    blockIdx = state.blockIdx;
    objectsLoaded = state.objectsLoaded;
    updateImage = state.updateImage;

    for (size_t i = 0; i < 104; ++i) tileBuffer[i] = state.tileBuffer[i];
}

void Screen::save_state(ScreenState& state) const
{
    state.cycles = cycles;
    state.status = status;
    state.curLine = curLine;
    state.quantity = quantity;
    state.blockIdx = blockIdx;
    state.objectsLoaded = objectsLoaded;
    state.updateImage = updateImage;

    for (size_t i = 0; i < 104; ++i) state.tileBuffer[i] = tileBuffer[i];
}

void Screen::hblank_dma_init(void)
{
    map->register_load(HDMA5_ADDRESS, quantity);
    quantity &= 0x7Fu;
    map->register_store(HDMA5_ADDRESS, quantity);
    ++quantity;
    blockIdx = 0;
}

void Screen::hblank_dma_stop(void)
{
    map->register_store(HDMA5_ADDRESS, 0xFFu);
    quantity = 0;
}

void Screen::hblank_dma_tick(void)
{
    if (!quantity) return;

    Reg8 hiSrcAddr, loSrcAddr;
    Reg8 hiDstAddr, loDstAddr;

    map->register_load(HDMA1_ADDRESS, hiSrcAddr);
    map->register_load(HDMA2_ADDRESS, loSrcAddr);
    map->register_load(HDMA3_ADDRESS, hiDstAddr);
    map->register_load(HDMA4_ADDRESS, loDstAddr);

    loSrcAddr &= 0xF0u;
    hiDstAddr &= 0x1Fu;
    loDstAddr &= 0xF0u;

    Reg16 srcAddr = make_reg16(hiSrcAddr, loSrcAddr) + Reg16(blockIdx) * 16;
    Reg16 dstAddr = make_reg16(hiDstAddr, loDstAddr) + Reg16(blockIdx) * 16;

    for (Reg16 i = 0; i < 16; ++i)
    {
        Reg8 value;

        map->load(srcAddr + i, value);
        map->store(0x8000u + dstAddr + i, value);
    }

    --quantity;
    ++blockIdx;

    if (!quantity) hblank_dma_stop();
}

void Screen::update(Cycle elapsed)
{
    Reg8 lcdcReg, statReg;

    map->register_load(LCDC_ADDRESS, lcdcReg);
    map->register_load(STAT_ADDRESS, statReg);

    if (gb->get_status() == STATUS_STOPPED) return;

    // Reset the LCDC state when it gest disabled.
    if (!(lcdcReg & BIT_7))
    {
        curLine = 0;
        statReg &= ~(BIT_0 | BIT_1);
        map->register_store(STAT_ADDRESS, statReg);
        map->register_store(LY_ADDRESS, 0);
        status = HZ_BLANKING;
        cycles = ZERO_CYCLE;
        return;
    }

    cycles += elapsed;

    // Are we still drawing the screen ?
    if (curLine < 144)
    {
        // Should we draw a line ?
        if (status == OAM_READ && cycles >= OAM_READ_TIME)
        {
            status = OAM_VRAM_READ;
            draw_line();
            cycles -= OAM_READ_TIME;
            statReg |= BIT_0 | BIT_1;
            map->register_store(STAT_ADDRESS, statReg);
        }

        // Did we finish reading VRAM ?
        if (status == OAM_VRAM_READ && cycles >= OAM_VRAM_READ_TIME)
        {
            status = HZ_BLANKING;
            cycles -= OAM_VRAM_READ_TIME;

            if (statReg & BIT_3) map->set_interrupt(BIT_1);

            statReg &= ~(BIT_0 | BIT_1);
            map->register_store(STAT_ADDRESS, statReg);

            // Should we perform a DMA transfer ?
            if (gb->get_cartinfo().mode == CGB_MODE) hblank_dma_tick();
        }

        // Did we finish our horizontal blanking ?
        if (status == HZ_BLANKING && cycles >= HZ_BLANKING_TIME)
        {
            ++curLine;
            statReg = (statReg & ~BIT_0) | BIT_1;
            map->register_store(STAT_ADDRESS, statReg);
            status = OAM_READ;
            load_oam();
            cycles -= HZ_BLANKING_TIME;

            // Did we just finish drawing the screen ?
            if (curLine == 144)
            {
                if (statReg & BIT_4) map->set_interrupt(BIT_1);

                map->set_interrupt(BIT_0);
                statReg = (statReg & ~BIT_1) | BIT_0;
                map->register_store(STAT_ADDRESS, statReg);
                updateImage = true;
            }
            else if (statReg & BIT_5)
                map->set_interrupt(BIT_1);
        }
    }

    // Did we already finish drawing the screen ?
    if (curLine >= 144 && cycles >= VT_BLANKING_LINE_TIME)
    {
        cycles -= VT_BLANKING_LINE_TIME;
        ++curLine;

        if (curLine == 154)
        {
            curLine = 0;
            statReg = (statReg & ~BIT_0) | BIT_1;
            map->register_store(STAT_ADDRESS, statReg);
            status = OAM_READ;

            if (statReg & BIT_5) map->set_interrupt(BIT_1);
        }

        load_oam();
    }
}

void Screen::load_oam(void)
{
    Reg8 lcdcReg;
    Reg8 lyReg = curLine;
    Reg8 lycReg;
    Reg8 statReg;
    Reg8 ieReg;

    map->register_load(LCDC_ADDRESS, lcdcReg);
    if ((lcdcReg & BIT_1) == 0)
        objectsLoaded = false;

    else
    {
        objectsLoaded = true;

        for (Reg16 i = 0; i < 40; ++i)
        {
            Tile& cur = tileBuffer[i];

            map->load(0xFE00u + i * 4, cur.y);
            map->load(0xFE01u + i * 4, cur.x);
            map->load(0xFE02u + i * 4, cur.code);
            map->load(0xFE03u + i * 4, cur.attributes);

            cur.type = OBJECT;
            cur.y -= 16;
            cur.x -= 8;

            cur.prior = (gb->get_cartinfo().mode == DMG_MODE) ? 0xFFu - cur.x : 0;
            cur.prior2 = 0xFFu - Reg8(i);
        }
    }

    map->register_store(LY_ADDRESS, lyReg);
    map->register_load(LYC_ADDRESS, lycReg);
    map->register_load(STAT_ADDRESS, statReg);

    if (lycReg == lyReg)
    {
        statReg |= BIT_2;
        map->register_load(IE_ADDRESS, ieReg);
        if (statReg & BIT_6) map->set_interrupt(BIT_1);
    }
    else
        statReg &= ~BIT_2;

    map->register_store(STAT_ADDRESS, statReg);
}

Reg8 Screen::load_bg_window_tiles(void)
{
    Reg8 lcdcReg;
    Reg8 vbkReg;
    Reg8 wyReg;
    Reg8 scyReg;
    Reg8 objOffset = objectsLoaded ? 40 : 0;

    map->register_load(LCDC_ADDRESS, lcdcReg);
    map->register_load(VBK_ADDRESS, vbkReg);
    map->register_load(WY_ADDRESS, wyReg);
    map->register_load(SCY_ADDRESS, scyReg);
    if (gb->get_cartinfo().mode == DMG_MODE) vbkReg = 0;

    if ((lcdcReg & BIT_5) && wyReg <= curLine)
    {
        Reg16 offset = (lcdcReg & BIT_6) ? 0x1C00u : 0x1800u;
        Reg8 alignWy = (curLine - wyReg) & 0xF8u;

        for (Reg8 x = 0; x < 32; ++x)
        {
            Tile& cur = tileBuffer[objOffset + x];

            cur.type = WINDOW;
            cur.y = curLine;
            cur.x = x << 3;

            map->vram_load(offset + Reg16(alignWy) * 4 + x, false, cur.code);

            if (gb->get_cartinfo().mode == DMG_MODE)
                cur.attributes = 0;
            else
                map->vram_load(offset + Reg16(alignWy) * 4 + x, true, cur.attributes);

            cur.prior = 0;
            cur.prior2 = 0;
        }

        objOffset += 32;
    }
    if (gb->get_cartinfo().mode == CGB_MODE || (lcdcReg & BIT_0))
    {
        Reg16 offset = (lcdcReg & BIT_3) ? 0x1C00u : 0x1800u;
        Reg8 alignScy = (curLine + scyReg) & 0xF8u;

        for (Reg8 x = 0; x < 32; ++x)
        {
            Tile& cur = tileBuffer[objOffset + x];

            cur.type = BACKGROUND;
            cur.y = scyReg + curLine;
            cur.x = x << 3;

            map->vram_load(offset + Reg16(alignScy) * 4 + x, false, cur.code);

            if (gb->get_cartinfo().mode == DMG_MODE)
                cur.attributes = 0;
            else
                map->vram_load(offset + Reg16(alignScy) * 4 + x, true, cur.attributes);

            cur.prior = 0;
            cur.prior2 = 0;
        }

        objOffset += 32;
    }

    return objOffset;
}

void Screen::draw_line(void)
{
    Reg8 tileCount = load_bg_window_tiles();

    std::sort(&tileBuffer[0], &tileBuffer[tileCount]);

    // Reset line pixels
    int32_t* linePixels = winPixels + size_t(curLine) * 160;
    std::fill(linePixels, linePixels + 160, int32_t(0x00FFFFFF));

    if (gb->get_cartinfo().mode == DMG_MODE)
        draw_line_dmg(tileCount, linePixels);
    else
        draw_line_cgb(tileCount, linePixels);

    // Fix rgb order + alpha for Qt
    for (Reg8 i = 0; i < 160; ++i)
    {
        uint32_t r, g, b;
        r = (linePixels[i] & 0xFF0000) >> 16;
        g = (linePixels[i] & 0x00FF00) >> 8;
        b = (linePixels[i] & 0x0000FF) >> 0;
        linePixels[i] = 0xFF000000u | (r << 0) | (g << 8) | (b << 16);
    }
}

void Screen::draw_line_dmg(Reg8 tileCount, int32_t* linePixels)
{
    int32_t bgPxlBuffer[160] = {}, objPxlBuffer[160] = {};
    bool bgPrior[160] = {}, objPrior[160] = {};
    bool bgZero[160], objZero[160];
    bool bgRender[160] = {}, objRender[160] = {};

    for (bool& b : bgZero) { b = true; }
    for (bool& b : objZero) { b = true; }

    int32_t backgroundPalette[4];
    int32_t objectPalette[8];
    Reg8 lcdcReg;
    Reg8 bgpReg;
    Reg8 obp0Reg;
    Reg8 obp1Reg;
    Reg8 wyReg;
    Reg8 wxReg;
    Reg8 scyReg;
    Reg8 scxReg;
    Reg8 objectSize;

    map->register_load(LCDC_ADDRESS, lcdcReg);
    map->register_load(BGP_ADDRESS, bgpReg);
    map->register_load(OBP0_ADDRESS, obp0Reg);
    map->register_load(OBP1_ADDRESS, obp1Reg);
    map->register_load(WY_ADDRESS, wyReg);
    map->register_load(WX_ADDRESS, wxReg);
    map->register_load(SCY_ADDRESS, scyReg);
    map->register_load(SCX_ADDRESS, scxReg);

    // Are we rendering 16x8 object tiles ?
    objectSize = (lcdcReg & BIT_2) ? 16 : 8;

    for (Reg8 i = 0; i < 4; ++i)
    {
        // Load the bits corresponding to the current color palette
        uint32_t value = (bgpReg >> (i * 2)) & 0x3u;

        // Then transform it into a 32-bit RGBA color
        backgroundPalette[i] = int32_t(0xFFFFFFu - value * 0x555555u);
    }

    // Same thing as above for both object palettes.
    for (Reg8 i = 0; i < 4; ++i)
    {
        uint32_t value = (obp0Reg >> (i * 2)) & 0x3u;

        objectPalette[i] = int32_t(0xFFFFFFu - value * 0x555555u);
    }
    for (Reg8 i = 0; i < 4; ++i)
    {
        uint32_t value = (obp1Reg >> (i * 2)) & 0x3u;

        objectPalette[i + 4] = int32_t(0xFFFFFFu - value * 0x555555u);
    }

    for (Reg8 i = 0; i < tileCount; ++i)
    {
        const Tile& cur = tileBuffer[i];

        Reg8 yPos = cur.y;
        Reg8 xPos = cur.x;

        if (cur.type == OBJECT)
        {
            // Verify that the current object pertains to the current line
            yPos = curLine - yPos;
            if (yPos >= objectSize) continue;

            // Is the tile horizontally mirrored ?
            if (cur.attributes & BIT_6) yPos = objectSize - yPos - 1;

            Reg8 dotLo;
            Reg8 dotHi;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + 0, false, dotLo);
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + 1, false, dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos;

                // Is the tile vertically mirrored ?
                xReal += (cur.attributes & BIT_5) ? x : 7 - x;

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && objZero[xReal])
                {
                    Reg8 dotData = (cur.attributes & BIT_4) ? 4 : 0;

                    dotData += (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    objPxlBuffer[xReal] = objectPalette[dotData];
                    objZero[xReal] = ((dotData & 3) == 0);
                    objPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    objRender[xReal] = true;
                }
            }
        }
        else if (cur.type == WINDOW)
        {
            yPos -= wyReg;
            yPos &= 0x7u;
            xPos += wxReg;

            Reg8 dotLo;
            Reg8 dotHi;
            Reg16 dataOffset = (cur.code >= 0x80u) ? 0x0u : 0x1000u;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset, false, dotLo);
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset + 1, false, dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos - x;

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && xReal >= wxReg - 7 && !bgRender[xReal])
                {
                    Reg8 dotData = (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    bgPxlBuffer[xReal] = backgroundPalette[dotData];
                    bgZero[xReal] = (dotData == 0);
                    bgPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    bgRender[xReal] = true;
                }
            }
        }
        else // cur.type == BACKGROUND
        {
            xPos -= scxReg;
            yPos -= (scyReg + curLine) & 0xF8u;

            Reg8 dotLo;
            Reg8 dotHi;
            Reg16 dataOffset = ((lcdcReg & BIT_4) || cur.code >= 0x80u) ? 0x0u : 0x1000u;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset, false, dotLo);
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset + 1, false, dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos + 7 - x;

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && !bgRender[xReal])
                {
                    Reg8 dotData = (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    bgPxlBuffer[xReal] = backgroundPalette[dotData];
                    bgZero[xReal] = (dotData == 0);
                    bgPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    bgRender[xReal] = true;
                }
            }
        }
    }

    for (Reg8 i = 0; i < 160; ++i)
    {
        // No display on screen, pixel should be white.
        if (!bgRender[i] && !objRender[i]) linePixels[i] = 0xFFFFFF;

        // Either BG or OBJ hasn't been rendered, display the other one.
        else if (!bgRender[i] || !objRender[i])
            linePixels[i] = objRender[i] ? objPxlBuffer[i] : bgPxlBuffer[i];

        // Priority to BG, render it except if the pixel is transparent and not the OBJ one.
        else if (bgPrior[i] || objPrior[i])
            linePixels[i] = (bgZero[i] && !objZero[i]) ? objPxlBuffer[i] : bgPxlBuffer[i];

        // Priority to OBJ, render it except if the pixel is transparent.
        else
            linePixels[i] = !objZero[i] ? objPxlBuffer[i] : bgPxlBuffer[i];
    }
}

void Screen::draw_line_cgb(Reg8 tileCount, int32_t* linePixels)
{
    int32_t bgPxlBuffer[160] = {}, objPxlBuffer[160] = {};
    bool bgPrior[160] = {}, objPrior[160] = {};
    bool bgZero[160], objZero[160];
    bool bgRender[160] = {}, objRender[160] = {};

    for (bool& b : bgZero) { b = true; }
    for (bool& b : objZero) { b = true; }

    int32_t backgroundPalette[32];
    int32_t objectPalette[32];
    Reg8 lcdcReg;
    Reg8 bgpReg;
    Reg8 obp0Reg;
    Reg8 obp1Reg;
    Reg8 wyReg;
    Reg8 wxReg;
    Reg8 scyReg;
    Reg8 scxReg;
    Reg8 objectSize;

    map->register_load(LCDC_ADDRESS, lcdcReg);
    map->register_load(BGP_ADDRESS, bgpReg);
    map->register_load(OBP0_ADDRESS, obp0Reg);
    map->register_load(OBP1_ADDRESS, obp1Reg);
    map->register_load(WY_ADDRESS, wyReg);
    map->register_load(WX_ADDRESS, wxReg);
    map->register_load(SCY_ADDRESS, scyReg);
    map->register_load(SCX_ADDRESS, scxReg);

    // Are we rendering 16x8 object tiles ?
    objectSize = (lcdcReg & BIT_2) ? 16 : 8;

    for (Reg8 i = 0; i < 32; ++i)
    {
        Reg8 lo, hi;

        map->load(0xE000 + i * 2, lo);
        map->load(0xE001 + i * 2, hi);

        Reg8 r32 = lo & 0x1Fu;
        Reg8 g32 = (lo >> 5) | ((hi & 0x3u) << 3);
        Reg8 b32 = (hi >> 2) & 0x1Fu;

        r32 = (r32 << 3) + 0x7u;
        g32 = (g32 << 3) + 0x7u;
        b32 = (b32 << 3) + 0x7u;

        backgroundPalette[i] =
            int32_t((uint32_t(r32) << 0) + (uint32_t(g32) << 8) + (uint32_t(b32) << 16));
    }

    // Same thing as above for both object palettes.
    for (Reg8 i = 0; i < 32; ++i)
    {
        Reg8 lo, hi;

        map->load(0xE100 + i * 2, lo);
        map->load(0xE101 + i * 2, hi);

        Reg8 r32 = lo & 0x1Fu;
        Reg8 g32 = (lo >> 5) | ((hi & 0x3u) << 3);
        Reg8 b32 = (hi >> 2) & 0x1Fu;

        r32 = (r32 << 3) + 0x7u;
        g32 = (g32 << 3) + 0x7u;
        b32 = (b32 << 3) + 0x7u;

        objectPalette[i] =
            int32_t((uint32_t(r32) << 0) + (uint32_t(g32) << 8) + (uint32_t(b32) << 16));
    }

    for (Reg8 i = 0; i < tileCount; ++i)
    {
        const Tile& cur = tileBuffer[i];

        Reg8 yPos = cur.y;
        Reg8 xPos = cur.x;

        if (cur.type == OBJECT)
        {
            // Verify that the current object pertains to the current line
            yPos = curLine - yPos;
            if (yPos >= objectSize) continue;

            // Is the tile horizontally mirrored ?
            if (cur.attributes & BIT_6) yPos = objectSize - yPos - 1;

            Reg8 paletteOffset = (cur.attributes & 0x7u) * 4;

            Reg8 dotLo;
            Reg8 dotHi;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(
                Reg16(cur.code) * 16 + Reg16(yPos) * 2 + 0, !!(cur.attributes & BIT_3), dotLo);
            map->vram_load(
                Reg16(cur.code) * 16 + Reg16(yPos) * 2 + 1, !!(cur.attributes & BIT_3), dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos;

                // Is the tile vertically mirrored ?
                xReal += (cur.attributes & BIT_5) ? x : 7 - x;

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && objZero[xReal])
                {
                    Reg8 dotData = 0;

                    dotData += (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    objPxlBuffer[xReal] = objectPalette[paletteOffset + dotData];
                    objZero[xReal] = ((dotData & 3) == 0);
                    objPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    objRender[xReal] = true;
                }
            }
        }
        else if (cur.type == WINDOW)
        {
            yPos -= wyReg;
            yPos &= 0x7u;
            xPos += wxReg;

            // Is the tile horizontally mirrored ?
            if (cur.attributes & BIT_6) yPos = 7 - yPos;

            Reg8 paletteOffset = (cur.attributes & 0x7u) * 4;

            Reg8 dotLo;
            Reg8 dotHi;
            Reg16 dataOffset = (cur.code >= 0x80u) ? 0x0u : 0x1000u;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset,
                !!(cur.attributes & BIT_3), dotLo);
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset + 1,
                !!(cur.attributes & BIT_3), dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos - ((cur.attributes & BIT_5) ? 7 - x : x);

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && xReal >= wxReg - 7 && !bgRender[xReal])
                {
                    Reg8 dotData = (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    bgPxlBuffer[xReal] = backgroundPalette[paletteOffset + dotData];
                    bgZero[xReal] = (dotData == 0);
                    bgPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    bgRender[xReal] = true;
                }
            }
        }
        else // cur.type == BACKGROUND
        {
            xPos -= scxReg;
            yPos -= (scyReg + curLine) & 0xF8u;

            // Is the tile horizontally mirrored ?
            if (cur.attributes & BIT_6) yPos = 7 - yPos;

            Reg8 paletteOffset = (cur.attributes & 0x7u) * 4;

            Reg8 dotLo;
            Reg8 dotHi;
            Reg16 dataOffset = ((lcdcReg & BIT_4) || cur.code >= 0x80u) ? 0x0u : 0x1000u;

            // Preload dot data to avoid doing it for each pixel
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset,
                !!(cur.attributes & BIT_3), dotLo);
            map->vram_load(Reg16(cur.code) * 16 + Reg16(yPos) * 2 + dataOffset + 1,
                !!(cur.attributes & BIT_3), dotHi);

            for (Reg8 x = 0; x < 8; ++x)
            {
                Reg8 xReal = xPos + ((cur.attributes & BIT_5) ? x : 7 - x);

                // Is the pixel appearing on the screen ?
                if (xReal < 160 && !bgRender[xReal])
                {
                    Reg8 dotData = (dotLo >> x) & 1;
                    dotData += ((dotHi >> x) & 1) << 1;

                    bgPxlBuffer[xReal] = backgroundPalette[paletteOffset + dotData];
                    bgZero[xReal] = (dotData == 0);
                    bgPrior[xReal] = (cur.attributes & BIT_7) == BIT_7;
                    bgRender[xReal] = true;
                }
            }
        }
    }

    for (Reg8 i = 0; i < 160; ++i)
    {
        // No display on screen, pixel should be white.
        if (!bgRender[i] && !objRender[i]) linePixels[i] = 0xFFFFFF;

        // Either BG or OBJ hasn't been rendered, display the other one.
        else if (!bgRender[i] || !objRender[i])
            linePixels[i] = objRender[i] ? objPxlBuffer[i] : bgPxlBuffer[i];

        // Priority to BG, render it except if the pixel is transparent and not the OBJ one.
        else if (bgPrior[i] || objPrior[i])
            linePixels[i] =
                (bgZero[i] && !objZero[i] && !bgPrior[i]) ? objPxlBuffer[i] : bgPxlBuffer[i];

        // Priority to OBJ, render it except if the pixel is transparent.
        else
            linePixels[i] = !objZero[i] ? objPxlBuffer[i] : bgPxlBuffer[i];
    }
}

bool Screen::can_update_image(void) const { return updateImage; }

void Screen::image_updated(void) { updateImage = false; }
