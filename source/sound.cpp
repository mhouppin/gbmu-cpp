#include "sound.h"
#include <algorithm>

APU::APU(MemoryMap* map)
{
    this->map = map;
    this->divAPU = 0;
    this->sq1SwpPace = 0;
    this->sq1HasCounter = 0;
}

void APU::update([[maybe_unused]] Cycle elapsed) {}

void APU::load_state(const ApuState& state)
{
    divAPU = state.divAPU;
    sq1SwpPace = state.sq1SwpPace;
    sq1SwpDirection = state.sq1SwpDirection;
    sq1SwpSlope = state.sq1SwpSlope;
    sq1EvpVolume = state.sq1EvpVolume;
    sq1EvpDirection = state.sq1EvpDirection;
    sq1EvpSweepPace = state.sq1EvpSweepPace;
    sq1Wavelength = state.sq1Wavelength;
    sq1Length = state.sq1Length;
    sq1HasCounter = state.sq1HasCounter;
    sq1SwpCounter = state.sq1SwpCounter;
    sq2EvpVolume = state.sq2EvpVolume;
    sq2EvpDirection = state.sq2EvpDirection;
    sq2EvpSweepPace = state.sq2EvpSweepPace;
    sq2Wavelength = state.sq2Wavelength;
    sq2Length = state.sq2Length;
    sq2HasCounter = state.sq2HasCounter;
    pwtWavelength = state.pwtWavelength;
    pwtLength = state.pwtLength;
    pwtHasCounter = state.pwtHasCounter;
    noiseEvpVolume = state.noiseEvpVolume;
    noiseEvpDirection = state.noiseEvpDirection;
    noiseEvpSweepPace = state.noiseEvpSweepPace;
    noiseClockShift = state.noiseClockShift;
    noiseLfsrWidth = state.noiseLfsrWidth;
    noiseClockDivider = state.noiseClockDivider;
    noiseLength = state.noiseLength;
    noiseHasCounter = state.noiseHasCounter;
}

void APU::save_state(ApuState& state) const
{
    state.divAPU = divAPU;
    state.sq1SwpPace = sq1SwpPace;
    state.sq1SwpDirection = sq1SwpDirection;
    state.sq1SwpSlope = sq1SwpSlope;
    state.sq1EvpVolume = sq1EvpVolume;
    state.sq1EvpDirection = sq1EvpDirection;
    state.sq1EvpSweepPace = sq1EvpSweepPace;
    state.sq1Wavelength = sq1Wavelength;
    state.sq1Length = sq1Length;
    state.sq1HasCounter = sq1HasCounter;
    state.sq1SwpCounter = sq1SwpCounter;
    state.sq2EvpVolume = sq2EvpVolume;
    state.sq2EvpDirection = sq2EvpDirection;
    state.sq2EvpSweepPace = sq2EvpSweepPace;
    state.sq2Wavelength = sq2Wavelength;
    state.sq2Length = sq2Length;
    state.sq2HasCounter = sq2HasCounter;
    state.pwtWavelength = pwtWavelength;
    state.pwtLength = pwtLength;
    state.pwtHasCounter = pwtHasCounter;
    state.noiseEvpVolume = noiseEvpVolume;
    state.noiseEvpDirection = noiseEvpDirection;
    state.noiseEvpSweepPace = noiseEvpSweepPace;
    state.noiseClockShift = noiseClockShift;
    state.noiseLfsrWidth = noiseLfsrWidth;
    state.noiseClockDivider = noiseClockDivider;
    state.noiseLength = noiseLength;
    state.noiseHasCounter = noiseHasCounter;
}

void APU::initialize_square1(void)
{
    Reg8 nr10, nr11, nr12, nr13, nr14, nr52;

    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_7)) return;

    nr52 |= BIT_0;

    map->register_load(NR10_ADDRESS, nr10);
    map->register_load(NR11_ADDRESS, nr11);
    map->register_load(NR12_ADDRESS, nr12);
    map->register_load(NR13_ADDRESS, nr13);
    map->register_load(NR14_ADDRESS, nr14);

    sq1SwpPace = (nr10 >> 4) & 0x7u;
    sq1SwpDirection = (nr10 >> 3) & 0x1u;
    sq1SwpSlope = nr10 & 0x7u;

    sq1Length = nr11 & 0x3Fu;

    sq1EvpVolume = nr12 >> 4;
    sq1EvpDirection = (nr12 >> 3) & 0x1u;
    sq1EvpSweepPace = nr12 & 0x7u;

    sq1HasCounter = !!(nr14 & BIT_6);
    sq1Wavelength = make_reg16(nr14 & 0x7u, nr13);

    sq1SwpCounter = 0;
    map->register_store(NR52_ADDRESS, nr52);
}

void APU::initialize_square2(void)
{
    Reg8 nr21, nr22, nr23, nr24, nr52;

    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_7)) return;

    nr52 |= BIT_1;

    map->register_load(NR21_ADDRESS, nr21);
    map->register_load(NR22_ADDRESS, nr22);
    map->register_load(NR23_ADDRESS, nr23);
    map->register_load(NR24_ADDRESS, nr24);

    sq2Length = nr21 & 0x3Fu;

    sq2EvpVolume = nr22 >> 4;
    sq2EvpDirection = (nr22 >> 3) & 0x1u;
    sq2EvpSweepPace = nr22 & 0x7u;

    sq2HasCounter = !!(nr24 & BIT_6);
    sq2Wavelength = make_reg16(nr24 & 0x7u, nr23);

    map->register_store(NR52_ADDRESS, nr52);
}

void APU::initialize_wave(void)
{
    Reg8 nr30, nr31, nr32, nr33, nr34, nr52;

    map->register_load(NR30_ADDRESS, nr30);
    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_7) || !(nr30 & BIT_7)) return;

    nr52 |= BIT_2;

    map->register_load(NR31_ADDRESS, nr31);
    map->register_load(NR32_ADDRESS, nr32);
    map->register_load(NR33_ADDRESS, nr33);
    map->register_load(NR34_ADDRESS, nr34);

    pwtLength = nr31;

    pwtHasCounter = !!(nr34 & BIT_6);
    pwtWavelength = make_reg16(nr34 & 0x7u, nr33);

    map->register_store(NR52_ADDRESS, nr52);
}

void APU::initialize_noise(void)
{
    Reg8 nr41, nr42, nr43, nr44, nr52;

    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_7)) return;

    nr52 |= BIT_3;

    map->register_load(NR41_ADDRESS, nr41);
    map->register_load(NR42_ADDRESS, nr42);
    map->register_load(NR43_ADDRESS, nr43);
    map->register_load(NR44_ADDRESS, nr44);

    noiseLength = nr41 & 0x3Fu;

    noiseEvpVolume = nr42 >> 4;
    noiseEvpDirection = (nr42 >> 3) & 0x1u;
    noiseEvpSweepPace = nr42 & 0x7u;

    noiseClockShift = nr43 >> 4;
    noiseLfsrWidth = (nr43 >> 3) & 0x1u;
    noiseClockDivider = nr43 & 0x7u;

    noiseHasCounter = !!(nr44 & BIT_6);

    map->register_store(NR52_ADDRESS, nr52);
}

void APU::increase_div_apu()
{
    Reg8 nr52;

    ++divAPU;
    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_7)) return;
    if (!(divAPU & 0x07u)) trigger_envelope_sweep();
    if (!(divAPU & 0x01u)) trigger_sound_length();
    if (!(divAPU & 0x03u)) trigger_sq1_freq_sweep();
}

void APU::trigger_sq1_freq_sweep(void)
{
    Reg8 nr52;

    map->register_load(NR52_ADDRESS, nr52);

    if (!(nr52 & BIT_0) || !sq1SwpPace) return;

    ++sq1SwpCounter;

    if (sq1SwpCounter != sq1SwpPace) return;

    sq1SwpCounter = 0;

    Reg16 wlenDiff = sq1Wavelength >> sq1SwpSlope;

    if (sq1SwpDirection == 1)
        sq1Wavelength -= wlenDiff;
    else
    {
        if (sq1Wavelength + wlenDiff > 0x7FFu)
        {
            nr52 &= ~BIT_0;
            map->register_store(NR52_ADDRESS, nr52);
        }
        else
            sq1Wavelength += wlenDiff;
    }
}

void APU::trigger_sound_length(void)
{
    Reg8 nr52;

    map->register_load(NR52_ADDRESS, nr52);

    if ((nr52 & BIT_0) && sq1HasCounter)
    {
        sq1Length = std::min(sq1Length + 1, 64);
        if (sq1Length == 64) nr52 &= ~BIT_0;
    }

    if ((nr52 & BIT_1) && sq2HasCounter)
    {
        sq2Length = std::min(sq2Length + 1, 64);
        if (sq2Length == 64) nr52 &= ~BIT_1;
    }

    if ((nr52 & BIT_2) && pwtHasCounter)
    {
        pwtLength = std::min(pwtLength + 1, 256);
        if (pwtLength == 256) nr52 &= ~BIT_2;
    }

    if ((nr52 & BIT_3) && noiseHasCounter)
    {
        noiseLength = std::min(noiseLength + 1, 64);
        if (noiseLength == 64) nr52 &= ~BIT_3;
    }

    map->register_store(NR52_ADDRESS, nr52);
}

void APU::trigger_envelope_sweep(void) {}
