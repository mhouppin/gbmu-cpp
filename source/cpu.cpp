#include "cpu.h"

CPU::CPU(bool cgb) { reset(cgb); }

void CPU::reset(bool cgb)
{
    SP = 0xFFFEu;
    PC = 0x0000u;
    A = cgb ? 0x11u : 0x01u;
    F = 0xB0u;
    B = 0x00u;
    C = 0x13u;
    D = 0x00u;
    E = 0xD8u;
    H = 0x01u;
    L = 0x4Du;
}

void CPU::load_state(const CpuState& state)
{
    A = state.A;
    F = state.F;
    B = state.B;
    C = state.C;
    D = state.D;
    E = state.E;
    H = state.H;
    L = state.L;
    SP = state.SP;
    PC = state.PC;
}

void CPU::save_state(CpuState& state) const
{
    state.A = A;
    state.F = F;
    state.B = B;
    state.C = C;
    state.D = D;
    state.E = E;
    state.H = H;
    state.L = L;
    state.SP = SP;
    state.PC = PC;
}

Reg16 CPU::load_cpu_register(CpuRegister reg)
{
    switch (reg)
    {
        case CpuRegister::AF: return make_reg16(A, F);
        case CpuRegister::BC: return make_reg16(B, C);
        case CpuRegister::DE: return make_reg16(D, E);
        case CpuRegister::HL: return make_reg16(H, L);
        case CpuRegister::SP: return SP;
        case CpuRegister::PC: return PC;
        default: return 0;
    }
}

Cycle CPU::load_r8_d8(Reg8& r, Reg8 d)
{
    r = d;
    PC += 2;
    return 2 * ONE_CYCLE;
}

Cycle CPU::load_r16_d16(Reg8& hi, Reg8& lo, Reg8 dhi, Reg8 dlo)
{
    hi = dhi;
    lo = dlo;
    PC += 3;
    return 3 * ONE_CYCLE;
}

Cycle CPU::load_r8_r8(Reg8& dst, Reg8 src)
{
    dst = src;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::load_hl_sp_r8(Reg8 r)
{
    Reg16 r16 = (Reg16(r) ^ 0x80u) - 0x80u;
    Reg16 result = SP + r16;

    F = 0;

    if (Reg8(result) < Reg8(SP)) F |= FLAG_CY;
    if ((result & BIT4_LO) < (SP & BIT4_LO)) F |= FLAG_H;

    H = reg16_hi(result);
    L = reg16_lo(result);
    PC += 2;
    return 3 * ONE_CYCLE;
}

Cycle CPU::inc_r8(Reg8& r)
{
    r += 1;
    F &= ~(FLAG_N | FLAG_H | FLAG_Z);
    if ((r & BIT4_LO) == 0) F |= FLAG_H;
    if (r == 0) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::dec_r8(Reg8& r)
{
    r -= 1;
    F &= ~(FLAG_H | FLAG_Z);
    F |= FLAG_N;
    if ((r & BIT4_LO) == BIT4_LO) F |= FLAG_H;
    if (r == 0) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::inc_r16(Reg8& hi, Reg8& lo)
{
    lo += 1;
    hi += !lo;
    PC += 1;
    return 2 * ONE_CYCLE;
}

Cycle CPU::dec_r16(Reg8& hi, Reg8& lo)
{
    hi -= !lo;
    lo -= 1;
    PC += 1;
    return 2 * ONE_CYCLE;
}

Cycle CPU::add_a_r8(Reg8 r)
{
    Reg8 result = A + r;

    F = 0;
    if (result < A) F |= FLAG_CY;
    if ((result & BIT4_LO) < (A & BIT4_LO)) F |= FLAG_H;
    if (!result) F |= FLAG_Z;
    A = result;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::sub_a_r8(Reg8 r)
{
    Reg8 result = A - r;

    F = FLAG_N;
    if (result > A) F |= FLAG_CY;
    if ((result & BIT4_LO) > (A & BIT4_LO)) F |= FLAG_H;
    if (!result) F |= FLAG_Z;
    A = result;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::add_hl_r16(Reg16 r)
{
    Reg16 HL = make_reg16(H, L);
    Reg16 result = HL + r;

    F &= FLAG_Z;
    if (result < HL) F |= FLAG_CY;
    if ((result & BIT12_LO) < (HL & BIT12_LO)) F |= FLAG_H;
    H = reg16_hi(result);
    L = reg16_lo(result);
    PC += 1;
    return 2 * ONE_CYCLE;
}

Cycle CPU::add_sp_r8(Reg8 r)
{
    Reg16 r16 = (Reg16(r) ^ 0x80u) - 0x80u;
    Reg16 result = SP + r16;

    F = 0;

    if (Reg8(result) < Reg8(SP)) F |= FLAG_CY;
    if ((result & BIT4_LO) < (SP & BIT4_LO)) F |= FLAG_H;

    SP = result;
    PC += 2;
    return 4 * ONE_CYCLE;
}

Cycle CPU::adc_a_r8(Reg8 r)
{
    bool carry = !!(F & FLAG_CY);
    Reg8 result = A + r + carry;

    F = 0;
    if (result < A || (result == A && carry)) F |= FLAG_CY;
    if ((result & BIT4_LO) < (A & BIT4_LO) || ((result & BIT4_LO) == (A & BIT4_LO) && carry))
        F |= FLAG_H;
    if (!result) F |= FLAG_Z;
    A = result;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::sbc_a_r8(Reg8 r)
{
    bool carry = !!(F & FLAG_CY);
    Reg8 result = A - r - carry;

    F = FLAG_N;
    if (result > A || (result == A && carry)) F |= FLAG_CY;
    if ((result & BIT4_LO) > (A & BIT4_LO) || ((result & BIT4_LO) == (A & BIT4_LO) && carry))
        F |= FLAG_H;
    if (!result) F |= FLAG_Z;
    A = result;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::and_a_r8(Reg8 r)
{
    F = FLAG_H;
    A &= r;
    if (!A) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::xor_a_r8(Reg8 r)
{
    F = 0;
    A ^= r;
    if (!A) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::or_a_r8(Reg8 r)
{
    F = 0;
    A |= r;
    if (!A) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::cmp_a_r8(Reg8 r)
{
    F = FLAG_N;
    if (A == r) F |= FLAG_Z;
    if (A < r) F |= FLAG_CY;
    if ((A & BIT4_LO) < (r & BIT4_LO)) F |= FLAG_H;
    PC += 1;
    return ONE_CYCLE;
}

Cycle CPU::rlc_r8(Reg8& r)
{
    Reg8 carryBit = (r & BIT_7) >> 7;

    F = carryBit << 4;
    r = (r << 1) | carryBit;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::rrc_r8(Reg8& r)
{
    Reg8 carryBit = (r & BIT_0) << 7;

    F = carryBit >> 3;
    r = (r >> 1) | carryBit;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::rl_r8(Reg8& r)
{
    Reg8 carryBit = (F & FLAG_CY) >> 4;

    F = (r & BIT_7) >> 3;
    r = (r << 1) | carryBit;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::rr_r8(Reg8& r)
{
    Reg8 carryBit = (F & FLAG_CY) << 3;

    F = (r & BIT_0) << 4;
    r = (r >> 1) | carryBit;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::sla_r8(Reg8& r)
{
    F = (r & BIT_7) ? FLAG_CY : 0;
    r <<= 1;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::sra_r8(Reg8& r)
{
    Reg8 carryBit = (r & BIT_7);

    F = (r & BIT_0) ? FLAG_CY : 0;
    r = (r >> 1) | carryBit;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::swap_r8(Reg8& r)
{
    r = (r >> 4) | (r << 4);
    F = (!r) ? FLAG_Z : 0;
    return 2 * ONE_CYCLE;
}

Cycle CPU::srl_r8(Reg8& r)
{
    F = (r & BIT_0) ? FLAG_CY : 0;
    r >>= 1;
    if (!r) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::test_bit_r8(Reg8 r, Reg8 bit)
{
    F = (F & FLAG_CY) | FLAG_H;
    if (!(r & bit)) F |= FLAG_Z;
    return 2 * ONE_CYCLE;
}

Cycle CPU::reset_bit_r8(Reg8& r, Reg8 bit)
{
    r &= ~bit;
    return 2 * ONE_CYCLE;
}

Cycle CPU::set_bit_r8(Reg8& r, Reg8 bit)
{
    r |= bit;
    return 2 * ONE_CYCLE;
}

Cycle CPU::jump_relative_if(Reg8 r8, bool cond)
{
    PC += 2;
    if (cond)
    {
        PC += int16_t(int8_t(r8));
        return 3 * ONE_CYCLE;
    }
    return 2 * ONE_CYCLE;
}

Cycle CPU::jump_absolute_if(Reg16 a16, bool cond)
{
    if (cond)
    {
        PC = a16;
        return 4 * ONE_CYCLE;
    }
    PC += 3;
    return 3 * ONE_CYCLE;
}

Cycle CPU::daa(void)
{
    Reg8 oldF = F;
    Reg8 correction = !!(oldF & FLAG_CY) ? 0x60u : 0x00u;

    F &= FLAG_N;
    if (oldF & FLAG_H) correction |= 0x06u;

    if (!(oldF & FLAG_N))
    {
        if ((A & BIT4_LO) >= 0x0Au) correction |= 0x06u;
        if (A > 0x99u) correction |= 0x60u;

        A += correction;
    }
    else
        A -= correction;

    if (correction & 0x60u) F |= FLAG_CY;
    if (!A) F |= FLAG_Z;
    PC += 1;
    return ONE_CYCLE;
}
