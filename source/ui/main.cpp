#include "gameboy_ui.hpp"
#include <QApplication>
#include <QColor>
#include <QLocale>
#include <QPalette>
#include <QTranslator>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    // Useful informations : https://doc.qt.io/qt-6/qtlinguist-hellotr-example.html
    // Load the embedded translation module.
    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();

    for (const QString& locale : uiLanguages)
    {
        const QString baseName = "gbmu_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName))
        {
            app.installTranslator(&translator);
            break;
        }
    }

    app.setQuitOnLastWindowClosed(true);

    GameBoyWindow window(&app);
    window.show();
    return app.exec();
}
