#include "gameboy_ui.hpp"
#include <QActionGroup>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QMimeData>

GameBoyWindow::GameBoyWindow(QApplication *app, QWidget *parent) : QMainWindow(parent), app(app)
{
    // Create the main window
    setupUi();
    setupEvents();

    setAcceptDrops(true);

    screenBuffer = new QImage(160, 144, QImage::Format_ARGB32);

    // Set dark mode by default
    actionDarkMode->setChecked(true);
    darkModeToggled(true);

    // Disable buttons
    actionLoadState->setEnabled(false);
    actionSaveState->setEnabled(false);
    actionSaveGame->setEnabled(false);
    debuggerButton->setEnabled(false);
    saveButton->setEnabled(false);

    // Disable pad buttons
    startButton->setEnabled(false);
    selectButton->setEnabled(false);
    A_Button->setEnabled(false);
    B_Button->setEnabled(false);
    upButton->setEnabled(false);
    downButton->setEnabled(false);
    leftButton->setEnabled(false);
    rightButton->setEnabled(false);

    if (app->arguments().size() > 1)
    {
        QString fileName = app->arguments().at(1);
        openRomFile(fileName);
    }
}

GameBoyWindow::~GameBoyWindow()
{
    if (gameboy != nullptr)
    {
        gameboy->write_game_save();
        delete gameboy;
    }

    if (screenBuffer != nullptr) delete screenBuffer;
}

void GameBoyWindow::setupUi()
{
    if (objectName().isEmpty()) setObjectName("gameboy");
    resize(700, 950);
    setMinimumSize(QSize(350, 475));

    centralwidget = new QWidget(this);
    centralwidget->setObjectName("centralwidget");

    mainVerticalLayout = new QVBoxLayout(centralwidget);
    mainVerticalLayout->setObjectName("mainVerticalLayout");

    // Screen
    screen = new QLabel(/*centralwidget*/);
    screen->setObjectName("screen");
    screen->setStyleSheet("background-color: rgb(0, 0, 0); color: rgb(255, 255, 255); font: 75 "
                          "14pt \"MS Shell Dlg 2\";");
    screen->setAlignment(Qt::AlignCenter);
    // screen->setScaledContents(true);

    mainVerticalLayout->addWidget(screen);

    // Toolbar
    toolbarHorizontalLayout = new QHBoxLayout();
    toolbarHorizontalLayout->setObjectName("toolbarHorizontalLayout");

    romNameLabel = new QLabel(/*centralwidget*/);
    romNameLabel->setObjectName("padCrossLabel");

    startPauseButton = new QPushButton(/*centralwidget*/);
    startPauseButton->setObjectName("startPauseButton");

    saveButton = new QPushButton(/*centralwidget*/);
    saveButton->setObjectName("saveButton");

    debuggerButton = new QPushButton(/*centralwidget*/);
    debuggerButton->setObjectName("debuggerButton");

    hideGamePadButton = new QPushButton(/*centralwidget*/);
    hideGamePadButton->setObjectName("hideGamePadButton");

    toolbarHorizontalLayout->addWidget(romNameLabel);
    toolbarHorizontalLayout->addWidget(startPauseButton);
    toolbarHorizontalLayout->addWidget(saveButton);
    toolbarHorizontalLayout->addWidget(debuggerButton);
    toolbarHorizontalLayout->addWidget(hideGamePadButton);

    mainVerticalLayout->addLayout(toolbarHorizontalLayout);

    // Gamepad
    gamePadWidget = new QWidget(/*centralwidget*/);

    gamePadGridLayout = new QGridLayout();
    gamePadGridLayout->setObjectName("gamePadGridLayout");
    gamePadGridLayout->setContentsMargins(5, 5, 5, 5);

    gamePadWidget->setLayout(gamePadGridLayout);

    ABGridLayout = new QGridLayout();
    ABGridLayout->setObjectName("ABGridLayout");

    A_Button = new QPushButton(/*centralwidget*/);
    A_Button->setObjectName("A_Button");

    B_Button = new QPushButton(/*centralwidget*/);
    B_Button->setObjectName("B_Button");

    ABGridLayout->addWidget(A_Button, 1, 1, 1, 1);
    ABGridLayout->addWidget(B_Button, 2, 0, 1, 1);

    gamePadGridLayout->addLayout(ABGridLayout, 1, 5, 1, 1);

    // Directional Cross
    padCrossLabel = new QLabel(/*centralwidget*/);
    padCrossLabel->setObjectName("padCrossLabel");
    padCrossLabel->setAlignment(Qt::AlignCenter);

    rightButton = new QPushButton(/*centralwidget*/);
    rightButton->setObjectName("rightButton");

    upButton = new QPushButton(/*centralwidget*/);
    upButton->setObjectName("upButton");

    leftButton = new QPushButton(/*centralwidget*/);
    leftButton->setObjectName("leftButton");

    downButton = new QPushButton(/*centralwidget*/);
    downButton->setObjectName("downButton");

    gamePadGridLayout->addWidget(padCrossLabel, 1, 1, 1, 1);
    gamePadGridLayout->addWidget(upButton, 0, 1, 1, 1);
    gamePadGridLayout->addWidget(downButton, 2, 1, 1, 1);
    gamePadGridLayout->addWidget(rightButton, 1, 2, 1, 1);
    gamePadGridLayout->addWidget(leftButton, 1, 0, 1, 1);

    selectButton = new QPushButton(/*centralwidget*/);
    selectButton->setObjectName("selectButton");

    startButton = new QPushButton(/*centralwidget*/);
    startButton->setObjectName("startButton");

    gamePadGridLayout->addWidget(selectButton, 3, 4, 1, 1);
    gamePadGridLayout->addWidget(startButton, 3, 3, 1, 1);

    mainVerticalLayout->addWidget(gamePadWidget);

    setCentralWidget(centralwidget);

    // Add Menubar
    menubar = new QMenuBar(this);
    menubar->setObjectName("menubar");

    menuGBmu = new QMenu(menubar);
    menuGBmu->setObjectName("menuGBmu");

    actionOpenRom = new QAction(menuGBmu);
    actionOpenRom->setObjectName("actionOpenRom");
    actionOpenRom->setShortcut(QKeySequence::Open);

    actionSaveGame = new QAction(menuGBmu);
    actionSaveGame->setObjectName("actionSaveGame");
    actionSaveGame->setShortcut(QKeySequence::Save);

    menuGBmu->addAction(actionOpenRom);
    menuGBmu->addAction(actionSaveGame);

    // Menu State
    menuState = new QMenu(menubar);
    menuState->setObjectName("menuState");

    actionLoadState = new QAction(menuState);
    actionLoadState->setObjectName("actionLoadState");
    actionLoadState->setShortcut(QKeySequence(Qt::CTRL | Qt::SHIFT | Qt::Key_L));

    actionSaveState = new QAction(menuState);
    actionSaveState->setObjectName("actionSaveState");
    actionSaveState->setShortcut(QKeySequence(Qt::CTRL | Qt::SHIFT | Qt::Key_S));

    menuState->addAction(actionLoadState);
    menuState->addAction(actionSaveState);

    QActionGroup *slotActionGroup = new QActionGroup(menuState);

    for (int i = 1; i < STATE_SLOTS_COUNTS; i++)
    {
        actionStateSlot[i - 1] = new QAction(menuState);
        actionStateSlot[i - 1]->setObjectName("actionLoadStateSlot" + QString::number(i));
        actionStateSlot[i - 1]->setShortcut(QKeySequence(Qt::CTRL | (Qt::Key_1 + i - 1)));
        actionStateSlot[i - 1]->setCheckable(true);
        menuState->addAction(actionStateSlot[i - 1]);
        slotActionGroup->addAction(actionStateSlot[i - 1]);
    }
    actionStateSlot[0]->setChecked(true);

    // Menu Edits
    menuEdit = new QMenu(menubar);
    menuEdit->setObjectName("menuEdit");

    menuSpeed = new QMenu(menuEdit);
    menuSpeed->setObjectName("menuSpeed");

    QActionGroup *speedActionGroup = new QActionGroup(menuSpeed);

    actionHalfSpeed = new QAction(menuSpeed);
    actionHalfSpeed->setObjectName("actionHalfSpeed");
    actionHalfSpeed->setCheckable(true);

    actionNormalSpeed = new QAction(menuSpeed);
    actionNormalSpeed->setObjectName("actionNormalSpeed");
    actionNormalSpeed->setCheckable(true);
    actionNormalSpeed->setChecked(true);

    actionDoubleSpeed = new QAction(menuSpeed);
    actionDoubleSpeed->setObjectName("actionDoubleSpeed");
    actionDoubleSpeed->setCheckable(true);

    actionQuadSpeed = new QAction(menuSpeed);
    actionQuadSpeed->setObjectName("actionQuadSpeed");
    actionQuadSpeed->setCheckable(true);

    menuSpeed->addAction(actionHalfSpeed);
    menuSpeed->addAction(actionNormalSpeed);
    menuSpeed->addAction(actionDoubleSpeed);
    menuSpeed->addAction(actionQuadSpeed);

    speedActionGroup->addAction(actionHalfSpeed);
    speedActionGroup->addAction(actionNormalSpeed);
    speedActionGroup->addAction(actionDoubleSpeed);
    speedActionGroup->addAction(actionQuadSpeed);

    // Emulation Mode Settings
    menuEmulationMode = new QMenu(menuEdit);
    menuEmulationMode->setObjectName("menuEmulation");

    QActionGroup *emulationModeActionGroup = new QActionGroup(menuEmulationMode);

    actionDMG = new QAction(menuEmulationMode);
    actionDMG->setObjectName("actionDMG");
    actionDMG->setCheckable(true);

    actionCGB = new QAction(menuEmulationMode);
    actionCGB->setObjectName("actionCGB");
    actionCGB->setCheckable(true);
    actionCGB->setChecked(true);

    emulationModeActionGroup->addAction(actionDMG);
    emulationModeActionGroup->addAction(actionCGB);

    menuEmulationMode->addAction(actionDMG);
    menuEmulationMode->addAction(actionCGB);

    // Dark Mode`
    actionDarkMode = new QAction(menuEdit);
    actionDarkMode->setObjectName("actionDarkMode");
    actionDarkMode->setCheckable(true);

    // Help Menu
    menuHelp = new QMenu(menubar);
    menuHelp->setObjectName("menuHelp");

    actionAbout = new QAction(menuHelp);
    actionAbout->setObjectName("actionAbout");
    actionAbout->setMenuRole(QAction::AboutRole);

    actionAboutQT = new QAction(menuHelp);
    actionAboutQT->setObjectName("actionAboutQT");
    actionAboutQT->setMenuRole(QAction::AboutQtRole);

    menuHelp->addAction(actionAbout);
    menuHelp->addAction(actionAboutQT);

    setMenuBar(menubar);

    statusbar = new QStatusBar(this);
    statusbar->setObjectName("statusbar");
    setStatusBar(statusbar);

    menubar->addAction(menuGBmu->menuAction());
    menubar->addAction(menuState->menuAction());
    menubar->addAction(menuEdit->menuAction());
    menuEdit->addAction(menuSpeed->menuAction());
    menuEdit->addAction(menuEmulationMode->menuAction());
    menuEdit->addAction(actionDarkMode);
    menubar->addAction(menuHelp->menuAction());

    retranslateUi();
    refreshStartPauseButton();
} // setupUi

void GameBoyWindow::retranslateUi()
{
    setWindowTitle(QCoreApplication::translate("gameboy", "Gameboy", nullptr));
    screen->setText(QCoreApplication::translate("gameboy", "No game loaded", nullptr));
    romNameLabel->setText(QCoreApplication::translate("gameboy", "No game loaded", nullptr));
    saveButton->setText(QCoreApplication::translate("gameboy", "Save", nullptr));
    debuggerButton->setText(QCoreApplication::translate("gameboy", "Debugger", nullptr));
    hideGamePadText = QCoreApplication::translate("gameboy", "Hide Gamepad", nullptr);
    hideGamePadButton->setText(hideGamePadText);
    B_Button->setText(QCoreApplication::translate("gameboy", "B", nullptr));
    A_Button->setText(QCoreApplication::translate("gameboy", "A", nullptr));
    padCrossLabel->setText(QCoreApplication::translate("gameboy", "+", nullptr));
    rightButton->setText(QCoreApplication::translate("gameboy", "Right", nullptr));
    upButton->setText(QCoreApplication::translate("gameboy", "Up", nullptr));
    leftButton->setText(QCoreApplication::translate("gameboy", "Left", nullptr));
    downButton->setText(QCoreApplication::translate("gameboy", "Down", nullptr));
    selectButton->setText(QCoreApplication::translate("gameboy", "Select", nullptr));
    startButton->setText(QCoreApplication::translate("gameboy", "Start", nullptr));

    menuGBmu->setTitle(QCoreApplication::translate("gameboy", "File", nullptr));
    menuState->setTitle(QCoreApplication::translate("gameboy", "State", nullptr));
    menuHelp->setTitle(QCoreApplication::translate("gameboy", "Help", nullptr));
    menuEdit->setTitle(QCoreApplication::translate("gameboy", "Edit", nullptr));
    menuSpeed->setTitle(QCoreApplication::translate("gameboy", "Speed", nullptr));
    menuEmulationMode->setTitle(QCoreApplication::translate("gameboy", "Emulation Mode", nullptr));

    for (int i = 0; i < STATE_SLOTS_COUNTS - 1; i++)
        actionStateSlot[i]->setText(QCoreApplication::translate("gameboy", "Slot", nullptr)
                                    + QString(" ") + QString::number(i + 1));

    actionOpenRom->setText(QCoreApplication::translate("gameboy", "Open rom", nullptr));
    actionLoadState->setText(QCoreApplication::translate("gameboy", "Load state", nullptr));
    actionSaveState->setText(QCoreApplication::translate("gameboy", "Save state", nullptr));
    actionSaveGame->setText(QCoreApplication::translate("gameboy", "Save game", nullptr));
    actionAbout->setText(QCoreApplication::translate("gameboy", "About", nullptr));
    actionAboutQT->setText(QCoreApplication::translate("gameboy", "About Qt", nullptr));
    actionHalfSpeed->setText(QCoreApplication::translate("gameboy", "0.5x", nullptr));
    actionNormalSpeed->setText(QCoreApplication::translate("gameboy", "1x", nullptr));
    actionDoubleSpeed->setText(QCoreApplication::translate("gameboy", "2x", nullptr));
    actionQuadSpeed->setText(QCoreApplication::translate("gameboy", "4x", nullptr));
    actionDMG->setText(QCoreApplication::translate("gameboy", "GameBoy", nullptr));
    actionCGB->setText(QCoreApplication::translate("gameboy", "GameBoy Color", nullptr));
    actionDarkMode->setText(QCoreApplication::translate("gameboy", "Dark Mode", nullptr));
} // retranslateUi

void GameBoyWindow::setupEvents()
{
    // Menu Bar
    connect(actionOpenRom, SIGNAL(triggered()), this, SLOT(openRomFileTriggered()));
    connect(actionSaveGame, SIGNAL(triggered()), this, SLOT(saveGameTriggered()));
    connect(actionSaveState, SIGNAL(triggered()), this, SLOT(saveStateTriggered()));
    connect(actionLoadState, SIGNAL(triggered()), this, SLOT(loadStateTriggered()));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(aboutTriggered()));
    connect(actionAboutQT, SIGNAL(triggered()), this, SLOT(aboutQtTriggered()));

    // Speed Menu
    connect(actionHalfSpeed, SIGNAL(triggered()), this, SLOT(speedHalfSpeedTriggered()));
    connect(actionNormalSpeed, SIGNAL(triggered()), this, SLOT(speedNormalTriggered()));
    connect(actionDoubleSpeed, SIGNAL(triggered()), this, SLOT(speedDoubleTriggered()));
    connect(actionQuadSpeed, SIGNAL(triggered()), this, SLOT(speedQuadTriggered()));

    // Emulation Mode Menu
    connect(actionDMG, SIGNAL(triggered()), this, SLOT(emulationModeDMGTriggered()));
    connect(actionCGB, SIGNAL(triggered()), this, SLOT(emulationModeCGBTriggered()));

    // Dark Mode
    connect(actionDarkMode, SIGNAL(toggled(bool)), this, SLOT(darkModeToggled(bool)));

    // Menu buttons
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveButtonClicked()));
    connect(startPauseButton, SIGNAL(clicked()), this, SLOT(startPauseButtonClicked()));
    connect(debuggerButton, SIGNAL(clicked()), this, SLOT(debuggerButtonClicked()));
    connect(hideGamePadButton, SIGNAL(clicked()), this, SLOT(hideGamePadButtonClicked()));

    // Pad buttons
    connect(selectButton, SIGNAL(pressed()), this, SLOT(selectButtonPressed()));
    connect(startButton, SIGNAL(pressed()), this, SLOT(startButtonPressed()));

    connect(A_Button, SIGNAL(pressed()), this, SLOT(A_ButtonPressed()));
    connect(B_Button, SIGNAL(pressed()), this, SLOT(B_ButtonPressed()));

    connect(upButton, SIGNAL(pressed()), this, SLOT(upButtonPressed()));
    connect(downButton, SIGNAL(pressed()), this, SLOT(downButtonPressed()));
    connect(rightButton, SIGNAL(pressed()), this, SLOT(rightButtonPressed()));
    connect(leftButton, SIGNAL(pressed()), this, SLOT(leftButtonPressed()));

    // Realease buttons
    connect(selectButton, SIGNAL(released()), this, SLOT(selectButtonReleased()));
    connect(startButton, SIGNAL(released()), this, SLOT(startButtonReleased()));

    connect(A_Button, SIGNAL(released()), this, SLOT(A_ButtonReleased()));
    connect(B_Button, SIGNAL(released()), this, SLOT(B_ButtonReleased()));

    connect(upButton, SIGNAL(released()), this, SLOT(upButtonReleased()));
    connect(downButton, SIGNAL(released()), this, SLOT(downButtonReleased()));
    connect(rightButton, SIGNAL(released()), this, SLOT(rightButtonReleased()));
    connect(leftButton, SIGNAL(released()), this, SLOT(leftButtonReleased()));
}

void GameBoyWindow::updateScreen()
{
    int w = screen->width();
    int h = screen->height();

    QPixmap pixmap = QPixmap::fromImage(*screenBuffer);
    pixmap = pixmap.scaled(w, h, Qt::KeepAspectRatio);

    screen->setPixmap(pixmap);
}

void GameBoyWindow::requestStart()
{
    // Check if the timer is already running or is paused
    if ((timerId != 0 && isRunning) || gameboy == nullptr) return;

    timerId = startTimer(timerInterval);
    if (timerId == 0)
    {
        QMessageBox::critical(this, tr("Error"), tr("Failed to start the emulator timer"));
        exit(1);
    }

    isRunning = true;
    emit start();
    refreshStartPauseButton();
}

void GameBoyWindow::requestStop()
{
    if (timerId == 0) return;

    killTimer(timerId);
    timerId = 0;

    isRunning = false;
    emit stop();
    refreshStartPauseButton();
}

// Timer to update the screen
void GameBoyWindow::timerEvent(QTimerEvent *event)
{
    (void)event;
    Cycle cycle = ZERO_CYCLE;

    while (cycle < 19760 * ONE_CYCLE)
    {
        cycle += gameboy->next_it();

        if (gameboy->breakpoint_reach())
        {
            gameboy->reset_breakpoint_reach();
            // Stop the emulator if breakpoint is reached
            requestStop();
            return;
        }

        if (gameboy->can_update_image())
        {
            gameboy->image_updated();
            updateScreen();
            emit screenUpdated();
            break;
        }
    }
}

void GameBoyWindow::saveButtonClicked() { gameboy->write_game_save(); }

void GameBoyWindow::startPauseButtonClicked()
{
    if (isRunning)
        requestStop();
    else
        requestStart();
}

void GameBoyWindow::debuggerButtonClicked()
{
    requestStop();

    if (debuggerWindow == nullptr)
    {
        debuggerWindow = new DebuggerWindow(this);

        connect(this, SIGNAL(screenUpdated()), debuggerWindow, SLOT(screenUpdated()));
        connect(this, SIGNAL(start()), debuggerWindow, SLOT(gameRunning()));
        connect(this, SIGNAL(stop()), debuggerWindow, SLOT(gameStopped()));

        connect(debuggerWindow, SIGNAL(requestScreenUpdate()), this, SLOT(updateScreen()));
        connect(debuggerWindow, SIGNAL(requestStart()), this, SLOT(requestStart()));
        connect(debuggerWindow, SIGNAL(requestStop()), this, SLOT(requestStop()));
    }

    debuggerWindow->show();
}

void GameBoyWindow::hideGamePadButtonClicked()
{
    if (isFullScreen)
    {
        hideGamePadButton->setText(hideGamePadText);
        gamePadWidget->show();
        isFullScreen = false;
    }
    else
    {
        hideGamePadButton->setText(tr("Show gamepad"));
        gamePadWidget->hide();
        isFullScreen = true;
    }
}

void GameBoyWindow::openRomFile(const QString &fileName)
{
    Gameboy *newGame = new Gameboy();

    newGame->open_game(fileName.toStdString(), (int32_t *)screenBuffer->bits());

    if (!newGame->is_loaded())
    {
        // Throw error
        QMessageBox::critical(this, tr("Error"),
            tr("Could not load ROM file : ") + QString::fromStdString(newGame->get_load_error()));
        return;
    }

    // Set the game title label
    romNameLabel->setText(QString::fromStdString(newGame->get_cartinfo().gameTitle));

    if (gameboy != nullptr) delete gameboy;
    gameboy = newGame;

    if (isRunning) requestStart();

    // Enable menu buttons
    actionLoadState->setEnabled(true);
    actionSaveState->setEnabled(true);
    actionSaveGame->setEnabled(true);

    debuggerButton->setEnabled(true);
    saveButton->setEnabled(true);

    // Enable pad buttons
    startButton->setEnabled(true);
    selectButton->setEnabled(true);
    A_Button->setEnabled(true);
    B_Button->setEnabled(true);
    upButton->setEnabled(true);
    downButton->setEnabled(true);
    leftButton->setEnabled(true);
    rightButton->setEnabled(true);
}

// Menu Bar
// To fix error : "Numeric mode unsupported in the posix collation implementation"
// https://forum.qt.io/topic/102262/unsupported-in-the-posix-collation-implementation/9
// https://forum.qt.io/post/589860
void GameBoyWindow::openRomFileTriggered()
{
    QString fileName =
        QFileDialog::getOpenFileName(this, tr("Open ROM"), "", tr("ROM Files (*.gb *.gbc)"));

    if (fileName.isEmpty()) return;
    openRomFile(fileName);

    if (isActiveWindow()) activateWindow();
}

void GameBoyWindow::saveGameTriggered() { gameboy->write_game_save(); }

int GameBoyWindow::getSelectedSlot()
{
    for (size_t i = 0; i < STATE_SLOTS_COUNTS - 1; i++)
    {
        if (actionStateSlot[i]->isChecked()) return (i + 1);
    }
    return (0);
}

void GameBoyWindow::saveStateTriggered()
{
    int slot = getSelectedSlot();

    if (slot == 0) return;

    if (!gameboy->save_state(slot))
        QMessageBox::critical(this, tr("Error"), tr("Failed to save in the selected save state!"));
}

void GameBoyWindow::loadStateTriggered()
{
    int slot = getSelectedSlot();

    if (slot == 0) return;

    if (!gameboy->load_state(slot))
        QMessageBox::critical(this, tr("Error"), tr("Failed to load the selected save state!"));
}

void GameBoyWindow::aboutTriggered()
{
    QMessageBox::about(
        this, tr("About GameBoy Emulator"), tr("GameBoy Emulator made by mhouppin & fgalaup."));
}

void GameBoyWindow::aboutQtTriggered() { QMessageBox::aboutQt(this, tr("About Qt")); }

void GameBoyWindow::setSpeed(int speed)
{
    timerInterval = speed;

    if (isRunning)
    {
        requestStop();
        requestStart();
    }
}

void GameBoyWindow::speedHalfSpeedTriggered() { setSpeed(34); }

void GameBoyWindow::speedNormalTriggered() { setSpeed(17); }

void GameBoyWindow::speedDoubleTriggered() { setSpeed(8); }

void GameBoyWindow::speedQuadTriggered() { setSpeed(4); }

void GameBoyWindow::emulationModeDMGTriggered() { gameboy->set_cgb_priority(false); }

void GameBoyWindow::emulationModeCGBTriggered() { gameboy->set_cgb_priority(true); }

void GameBoyWindow::darkModeToggled(bool checked)
{
    if (checked)
    {
        app->setStyle("Fusion");

        // Now use a palette to switch to dark colors:
        QPalette palette = QPalette();
        palette.setColor(QPalette::Window, QColor(53, 53, 53));
        palette.setColor(QPalette::WindowText, Qt::white);
        palette.setColor(QPalette::Base, QColor(25, 25, 25));
        palette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
        palette.setColor(QPalette::ToolTipBase, Qt::black);
        palette.setColor(QPalette::ToolTipText, Qt::white);
        palette.setColor(QPalette::Text, Qt::white);
        palette.setColor(QPalette::Button, QColor(53, 53, 53));
        palette.setColor(QPalette::Disabled, QPalette::Button, QColor(33, 33, 33));
        palette.setColor(QPalette::ButtonText, Qt::white);
        palette.setColor(QPalette::BrightText, Qt::red);
        palette.setColor(QPalette::Link, QColor(42, 130, 218));
        palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        palette.setColor(QPalette::HighlightedText, Qt::black);

        app->setPalette(palette);
    }
    else
    {
        app->setStyle("Fusion");
        app->setPalette(QApplication::style()->standardPalette());
    }
}

// https://stackoverflow.com/questions/14895302/qt-drag-drop-add-support-for-dragging-files-to-the-applications-main-window
void GameBoyWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls()) { event->acceptProposedAction(); }
}

void GameBoyWindow::dropEvent(QDropEvent *event)
{
    QString fileName = event->mimeData()->urls()[0].toLocalFile();
    openRomFile(fileName);
}

// Keyboard events
Event::GB_Key QtKeyToGBKey(const int Qtkey)
{
    // TODO: Use dynamic mapping
    switch (Qtkey)
    {
        case Qt::Key_W:
        case Qt::Key_Z: return Event::GB_KEY_UP;
        case Qt::Key_S: return Event::GB_KEY_DOWN;
        case Qt::Key_A:
        case Qt::Key_Q: return Event::GB_KEY_LEFT;
        case Qt::Key_D: return Event::GB_KEY_RIGHT;
        case Qt::Key_L: return Event::GB_KEY_A;
        case Qt::Key_K: return Event::GB_KEY_B;
        case Qt::Key_B: return Event::GB_KEY_START;
        case Qt::Key_N: return Event::GB_KEY_SELECT;
    }
    return Event::GB_KEY_NONE;
}

void GameBoyWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat()) return;

    if (!gameboy) return;

    gameboy->event->set_key_state(QtKeyToGBKey(event->key()), true);
}

void GameBoyWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat()) return;

    if (!gameboy) return;

    gameboy->event->set_key_state(QtKeyToGBKey(event->key()), false);
}

void GameBoyWindow::closeEvent(QCloseEvent *event)
{
    (void)event;
    qApp->closeAllWindows();
    qApp->quit();
}

void GameBoyWindow::refreshStartPauseButton()
{
    startPauseButton->setText(isRunning ? tr("Pause") : tr("Resume"));
}

// Pressed buttons events
void GameBoyWindow::A_ButtonPressed() { gameboy->event->set_button_state(Event::GB_KEY_A, true); }

void GameBoyWindow::B_ButtonPressed() { gameboy->event->set_button_state(Event::GB_KEY_B, true); }

void GameBoyWindow::selectButtonPressed()
{
    gameboy->event->set_button_state(Event::GB_KEY_SELECT, true);
}

void GameBoyWindow::startButtonPressed()
{
    gameboy->event->set_button_state(Event::GB_KEY_START, true);
}

void GameBoyWindow::upButtonPressed() { gameboy->event->set_button_state(Event::GB_KEY_UP, true); }

void GameBoyWindow::downButtonPressed()
{
    gameboy->event->set_button_state(Event::GB_KEY_DOWN, true);
}

void GameBoyWindow::rightButtonPressed()
{
    gameboy->event->set_button_state(Event::GB_KEY_RIGHT, true);
}

void GameBoyWindow::leftButtonPressed()
{
    gameboy->event->set_button_state(Event::GB_KEY_LEFT, true);
}

// Release buttons events
void GameBoyWindow::A_ButtonReleased() { gameboy->event->set_button_state(Event::GB_KEY_A, false); }

void GameBoyWindow::B_ButtonReleased() { gameboy->event->set_button_state(Event::GB_KEY_B, false); }

void GameBoyWindow::selectButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_SELECT, false);
}

void GameBoyWindow::startButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_START, false);
}

void GameBoyWindow::upButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_UP, false);
}

void GameBoyWindow::downButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_DOWN, false);
}

void GameBoyWindow::rightButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_RIGHT, false);
}

void GameBoyWindow::leftButtonReleased()
{
    gameboy->event->set_button_state(Event::GB_KEY_LEFT, false);
}
