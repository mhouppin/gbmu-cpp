#pragma once

#include "cpu.h"
#include "gameboy_ui.hpp"
#include "memory_image_widget.hpp"
#include <QWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

class GameBoyWindow;

struct UiRegister
{
    const char *name;
    Reg16 address;
};

class DebuggerWindow : public QWidget
{
    Q_OBJECT

  private:
    // User Interface
    QVBoxLayout *mainVerticalLayout;
    QHBoxLayout *controlHorizontalLayout;
    QPushButton *nextInstructionButton;
    QPushButton *nextFrameButton;
    QPushButton *runOneSecondButton;
    QPushButton *resetButton;
    QHBoxLayout *bodyHorizontalLayout;
    QGroupBox *disassemblerGroupBox;
    QVBoxLayout *disassemblerLayout;
    QTableWidget *disassemblerTableWidget;
    QTabWidget *tabWidget;
    QWidget *memoryTab;
    QVBoxLayout *memoryVerticalLayout;
    QHBoxLayout *memoryOffsetHorizontalLayout;
    QLabel *memoryStartAddressLabel;
    QSpinBox *memoryStartAddressSpinBox;
    QPushButton *loadMemoryButton;
    QTableWidget *memoryTableWidget;
    MemoryImageWidget *memoryImage;
    QWidget *registerTab;
    QGridLayout *registerGridLayout;
    QTableWidget *otherRegisterTableWidget;
    QTableWidget *registerTableWidget;
    QLabel *CPURegisterLabel;
    QLabel *otherRegisterLabel;
    QLabel *audioRegisterLabel;
    QTableWidget *audioRegisterTableWidget;
    QWidget *videoRegisterTab;
    QTableWidget *videoRegisterTableWidget;
    QHBoxLayout *videoRegisterHorizontalLayout;
    QWidget *breakpointTab;
    QVBoxLayout *breakpointVerticalLayout;
    QLabel *breakPointLabel;
    QSpinBox *breakPointAddressSpinBox;
    QPushButton *addBreakPointButton;
    QPushButton *removeBreakPointButton;
    QListWidget *breakPointListWidget;
    QPushButton *continueButton;

    // Emulator
    GameBoyWindow *gameboyWindow;

  public:
    DebuggerWindow(GameBoyWindow *gameboyWindow, QWidget *parent = nullptr);

  private:
    void setupUi();
    void retranslateUi();
    void setupEvents();

    void updateDebugInfo();
    QString toHex(QString format, uint64_t value, int width);
    QString getMemoryCellText(uint16_t address, int width = 2, QString format = "0x%1");
    QString getRegisterCellText(CpuRegister reg);
    void updateMemoryImage(Reg16 address);
    void updateMemory();
    void updateRegisters();
    void updateDisassembly();

  protected:
    void closeEvent(QCloseEvent *event) override;

  public:
  signals:
    void requestScreenUpdate();
    void requestStart();
    void requestStop();

  public slots:
    void screenUpdated();
    void gameRunning();
    void gameStopped();

  private slots:
    void editMemoryCell(int row, int column);
    void changeAddress(Reg16 value);
    void continueButtonClicked();
    void nextInstructionButtonClicked();
    void nextFrameButtonClicked();
    void runOneSecondButtonClicked();
    void resetButtonClicked();
    void loadMemoryButtonClicked();
    void addBreakPointButtonClicked();
    void removeBreakPointButtonClicked();
};
