#include "debugger_ui.hpp"

// clang-format off

const UiRegister registerList[] = {
    { "AF", AF },
    { "BC", BC },
    { "DE", DE },
    { "HL", HL },
    { "SP", SP },
    { "PC", PC },
    { NULL, 0 }
};

const UiRegister otherRegisterList[] = {
    { "P1", P1_ADDRESS },
    { "SB", SB_ADDRESS },
    { "SC", SC_ADDRESS },
    { "DIV", DIV_ADDRESS },
    { "TIMA", TIMA_ADDRESS },
    { "TMA", TMA_ADDRESS },
    { "TAC", TAC_ADDRESS },
    { "KEY1", KEY1_ADDRESS },
    { "RP", RP_ADDRESS },
    { "SVBK", SVBK_ADDRESS },
    { "IF", IF_ADDRESS },
    { "IE", IE_ADDRESS },
    { NULL, 0 }
};

const UiRegister audioRegisterList[] = {
    { "NR10", NR10_ADDRESS },
    { "NR11", NR11_ADDRESS },
    { "NR12", NR12_ADDRESS },
    { "NR13", NR13_ADDRESS },
    { "NR14", NR14_ADDRESS },
    { "NR21", NR21_ADDRESS },
    { "NR22", NR22_ADDRESS },
    { "NR23", NR23_ADDRESS },
    { "NR24", NR24_ADDRESS },
    { "NR30", NR30_ADDRESS },
    { "NR31", NR31_ADDRESS },
    { "NR32", NR32_ADDRESS },
    { "NR33", NR33_ADDRESS },
    { "NR34", NR34_ADDRESS },
    { "NR41", NR41_ADDRESS },
    { "NR42", NR42_ADDRESS },
    { "NR43", NR43_ADDRESS },
    { "NR44", NR44_ADDRESS },
    { "NR50", NR50_ADDRESS },
    { "NR51", NR51_ADDRESS },
    { "NR52", NR52_ADDRESS },
    { NULL, 0 }
};

const UiRegister videoRegisterList[] = {
    { "LCDC", LCDC_ADDRESS },
    { "STAT", STAT_ADDRESS },
    { "SCY", SCY_ADDRESS },
    { "SCX", SCX_ADDRESS },
    { "LY", LY_ADDRESS },
    { "LYC", LYC_ADDRESS },
    { "DMA", DMA_ADDRESS },
    { "BGP", BGP_ADDRESS },
    { "OBP0", OBP0_ADDRESS },
    { "OBP1", OBP1_ADDRESS },
    { "WY", WY_ADDRESS },
    { "WX", WX_ADDRESS },
    { "VBK", VBK_ADDRESS },
    { "HDMA1", HDMA1_ADDRESS },
    { "HDMA2", HDMA2_ADDRESS },
    { "HDMA3", HDMA3_ADDRESS },
    { "HDMA4", HDMA4_ADDRESS },
    { "HDMA5", HDMA5_ADDRESS },
    { "BCPS", BCPS_ADDRESS },
    { "BCPD", BCPD_ADDRESS },
    { "OCPS", OCPS_ADDRESS },
    { "OCPD", OCPD_ADDRESS },
    { NULL, 0 }
};

// clang-format on

DebuggerWindow::DebuggerWindow(GameBoyWindow *gameboyWindow, QWidget *parent)
    : QWidget(parent), gameboyWindow(gameboyWindow)
{
    // Create window
    setupUi();
    setupEvents();

    updateDebugInfo();
}

void DebuggerWindow::setupUi()
{
    // Window
    if (objectName().isEmpty()) setObjectName("Debugger");
    resize(1014, 493);

    mainVerticalLayout = new QVBoxLayout(this);
    mainVerticalLayout->setObjectName("mainVerticalLayout");
    mainVerticalLayout->setContentsMargins(5, 5, 5, 5);

    // Toolbar
    controlHorizontalLayout = new QHBoxLayout();
    controlHorizontalLayout->setObjectName("controlHorizontalLayout");

    nextInstructionButton = new QPushButton(/*mainVerticalLayoutWidget*/);
    nextInstructionButton->setObjectName("nextInstructionButton");

    nextFrameButton = new QPushButton(/*mainVerticalLayoutWidget*/);
    nextFrameButton->setObjectName("nextFrameButton");

    runOneSecondButton = new QPushButton(/*mainVerticalLayoutWidget*/);
    runOneSecondButton->setObjectName("runOneSecondButton");

    resetButton = new QPushButton(/*mainVerticalLayoutWidget*/);
    resetButton->setObjectName("resetButton");

    controlHorizontalLayout->addWidget(nextInstructionButton);
    controlHorizontalLayout->addWidget(nextFrameButton);
    controlHorizontalLayout->addWidget(runOneSecondButton);
    controlHorizontalLayout->addWidget(resetButton);
    mainVerticalLayout->addLayout(controlHorizontalLayout);

    // Main debugger layout
    bodyHorizontalLayout = new QHBoxLayout();
    bodyHorizontalLayout->setObjectName("bodyHorizontalLayout");

    // Disassembler
    disassemblerGroupBox = new QGroupBox(/*mainVerticalLayoutWidget*/);
    disassemblerLayout = new QVBoxLayout(disassemblerGroupBox);
    disassemblerGroupBox->setObjectName("disassemblerGroupBox");
    disassemblerTableWidget = new QTableWidget(/*disassemblerGroupBox*/);
    disassemblerTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    if (disassemblerTableWidget->columnCount() < 3) disassemblerTableWidget->setColumnCount(3);
    if (disassemblerTableWidget->rowCount() < 16) disassemblerTableWidget->setRowCount(16);

    disassemblerTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem());
    disassemblerTableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem());
    disassemblerTableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem());

    disassemblerTableWidget->setObjectName("disassemblerTableWidget");

    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            disassemblerTableWidget->setItem(i, j, new QTableWidgetItem());
        }
    }

    disassemblerLayout->addWidget(disassemblerTableWidget);
    disassemblerGroupBox->setLayout(disassemblerLayout);
    bodyHorizontalLayout->addWidget(disassemblerGroupBox);

    // Tool Tabs
    tabWidget = new QTabWidget(/*mainVerticalLayoutWidget*/);
    tabWidget->setObjectName("tabWidget");
    tabWidget->setAutoFillBackground(true);
    tabWidget->setTabPosition(QTabWidget::North);
    tabWidget->setElideMode(Qt::ElideNone);
    tabWidget->setTabsClosable(false);
    tabWidget->setMovable(false);
    tabWidget->setTabBarAutoHide(false);

    // Memory tab
    memoryTab = new QWidget();
    memoryTab->setObjectName("memoryTab");

    memoryVerticalLayout = new QVBoxLayout(memoryTab);
    memoryVerticalLayout->setObjectName("memoryVerticalLayout");
    memoryVerticalLayout->setContentsMargins(5, 5, 5, 5);

    // Memory toolbar
    memoryOffsetHorizontalLayout = new QHBoxLayout();
    memoryOffsetHorizontalLayout->setObjectName("memoryOffsetHorizontalLayout");

    memoryStartAddressLabel = new QLabel(/*mainVerticalLayoutWidget_2*/);
    memoryStartAddressLabel->setObjectName("offsetLabel");

    // Memory offset input spinbox
    memoryStartAddressSpinBox = new QSpinBox(/*mainVerticalLayoutWidget_2*/);
    memoryStartAddressSpinBox->setObjectName("memoryStartAddressSpinBox");
    memoryStartAddressSpinBox->setPrefix("0x");
    memoryStartAddressSpinBox->setDisplayIntegerBase(16);
    memoryStartAddressSpinBox->setSingleStep(16 * 16); // TODO Auto ceiling to 16 multiple
    memoryStartAddressSpinBox->setMinimum(0);
    memoryStartAddressSpinBox->setMaximum(65535);

    loadMemoryButton = new QPushButton(/*mainVerticalLayoutWidget_2*/);
    loadMemoryButton->setObjectName("loadMemoryButton");

    memoryOffsetHorizontalLayout->addWidget(memoryStartAddressLabel);
    memoryOffsetHorizontalLayout->addWidget(memoryStartAddressSpinBox);
    memoryOffsetHorizontalLayout->addWidget(loadMemoryButton);

    // Memory dumper table
    memoryTableWidget = new QTableWidget(/*mainVerticalLayoutWidget_2*/);
    memoryTableWidget->setObjectName("memoryTableWidget");
    memoryTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    if (memoryTableWidget->columnCount() < 17) memoryTableWidget->setColumnCount(17);
    if (memoryTableWidget->rowCount() < 16) memoryTableWidget->setRowCount(16);
    memoryTableWidget->verticalHeader()->setCascadingSectionResizes(false);
    memoryTableWidget->verticalHeader()->setStretchLastSection(false);

    const char *memoryTableWidgetHeaderLabels = "Address";

    for (int i = 0; i < 17; i++)
    {
        if (i == 0)
            memoryTableWidget->setHorizontalHeaderItem(
                i, new QTableWidgetItem(memoryTableWidgetHeaderLabels));
        else
            memoryTableWidget->setHorizontalHeaderItem(
                i, new QTableWidgetItem(toHex("0x%1", i - 1, 1)));

        for (int j = 0; j < 16; j++)
        {
            memoryTableWidget->setItem(j, i, new QTableWidgetItem("00"));
            memoryTableWidget->item(j, i)->setTextAlignment(Qt::AlignCenter);
        }
    }

    // Add memory image
    memoryImage = new MemoryImageWidget();

    memoryVerticalLayout->addLayout(memoryOffsetHorizontalLayout);
    memoryVerticalLayout->addWidget(memoryTableWidget);
    memoryVerticalLayout->addWidget(memoryImage);
    memoryVerticalLayout->setStretch(1, 1);
    memoryVerticalLayout->setStretch(2, 1);

    tabWidget->addTab(memoryTab, QString());

    // Registers tab
    registerTab = new QWidget();
    registerTab->setObjectName("registerTab");

    registerGridLayout = new QGridLayout(registerTab);
    registerGridLayout->setObjectName("registerGridLayout");
    registerGridLayout->setContentsMargins(5, 5, 5, 5);

    // CPU registers
    CPURegisterLabel = new QLabel(/*registerGridLayoutWidget*/);
    CPURegisterLabel->setObjectName("CPURegistersLabel");

    registerTableWidget = new QTableWidget(/*registerGridLayoutWidget*/);
    registerTableWidget->setObjectName("registerTableWidget");
    registerTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    if (registerTableWidget->columnCount() < 1) registerTableWidget->setColumnCount(1);
    if (registerTableWidget->rowCount() < 6) registerTableWidget->setRowCount(6);

    registerTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem());

    for (int i = 0; registerList[i].name; i++)
    {
        registerTableWidget->setVerticalHeaderItem(i, new QTableWidgetItem(registerList[i].name));
        registerTableWidget->setItem(i, 0, new QTableWidgetItem("0x0000"));
    }

    registerGridLayout->addWidget(CPURegisterLabel, 0, 1, 1, 1);
    registerGridLayout->addWidget(registerTableWidget, 1, 0, 1, 1);

    // Other registers
    otherRegisterLabel = new QLabel(/*registerGridLayoutWidget*/);
    otherRegisterLabel->setObjectName("OtherRegistersLabel");

    otherRegisterTableWidget = new QTableWidget(/*registerGridLayoutWidget*/);
    otherRegisterTableWidget->setObjectName("otherRegisterTableWidget");
    otherRegisterTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    if (otherRegisterTableWidget->columnCount() < 1) otherRegisterTableWidget->setColumnCount(1);
    if (otherRegisterTableWidget->rowCount() < 12) otherRegisterTableWidget->setRowCount(12);

    otherRegisterTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem());

    for (int i = 0; otherRegisterList[i].name; i++)
    {
        otherRegisterTableWidget->setVerticalHeaderItem(
            i, new QTableWidgetItem(otherRegisterList[i].name));
        otherRegisterTableWidget->setItem(i, 0, new QTableWidgetItem("0x00"));
    }

    registerGridLayout->addWidget(otherRegisterLabel, 0, 0, 1, 1);
    registerGridLayout->addWidget(otherRegisterTableWidget, 1, 1, 1, 1);

    // Audio registers
    audioRegisterLabel = new QLabel(/*registerGridLayoutWidget*/);
    audioRegisterLabel->setObjectName("AudioRegistersLabel");

    audioRegisterTableWidget = new QTableWidget(/*registerGridLayoutWidget*/);
    audioRegisterTableWidget->setObjectName("audioRegisterTableWidget");
    audioRegisterTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    if (audioRegisterTableWidget->columnCount() < 1) audioRegisterTableWidget->setColumnCount(1);
    if (audioRegisterTableWidget->rowCount() < 21) audioRegisterTableWidget->setRowCount(21);

    audioRegisterTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem());

    for (int i = 0; audioRegisterList[i].name; i++)
    {
        audioRegisterTableWidget->setVerticalHeaderItem(
            i, new QTableWidgetItem(audioRegisterList[i].name));
        audioRegisterTableWidget->setItem(i, 0, new QTableWidgetItem("0x00"));
    }

    registerGridLayout->addWidget(audioRegisterLabel, 0, 2, 1, 1);
    registerGridLayout->addWidget(audioRegisterTableWidget, 1, 2, 1, 1);

    tabWidget->addTab(registerTab, QString());

    // Video registers tab
    videoRegisterTab = new QWidget();
    videoRegisterTab->setObjectName("videoRegisterTab");

    videoRegisterHorizontalLayout = new QHBoxLayout(videoRegisterTab);
    videoRegisterHorizontalLayout->setObjectName("memoryVerticalLayout");
    videoRegisterHorizontalLayout->setContentsMargins(5, 5, 5, 5);

    videoRegisterTableWidget = new QTableWidget(videoRegisterTab);
    videoRegisterTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    videoRegisterTableWidget->setObjectName("videoRegisterTableWidget");
    if (videoRegisterTableWidget->columnCount() < 1) videoRegisterTableWidget->setColumnCount(1);
    if (videoRegisterTableWidget->rowCount() < 22) videoRegisterTableWidget->setRowCount(22);

    videoRegisterTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem());

    for (int i = 0; videoRegisterList[i].name; i++)
    {
        videoRegisterTableWidget->setVerticalHeaderItem(
            i, new QTableWidgetItem(videoRegisterList[i].name));
        videoRegisterTableWidget->setItem(i, 0, new QTableWidgetItem("0x00"));
    }

    videoRegisterHorizontalLayout->addWidget(videoRegisterTableWidget);

    tabWidget->addTab(videoRegisterTab, QString());

    // Breakpoint tab
    breakpointTab = new QWidget();
    breakpointTab->setObjectName("breakpointTab");

    breakpointVerticalLayout = new QVBoxLayout(breakpointTab);
    breakpointVerticalLayout->setObjectName("breakpointVerticalLayout");
    breakpointVerticalLayout->setContentsMargins(5, 5, 5, 5);

    breakPointLabel = new QLabel(/*mainVerticalLayoutWidget_3*/);
    breakPointLabel->setObjectName("BreakPointLabel");

    breakPointAddressSpinBox = new QSpinBox(/*mainVerticalLayoutWidget_3*/);
    breakPointAddressSpinBox->setObjectName("breakPointAddressSpinBox");
    breakPointAddressSpinBox->setPrefix("0x");
    breakPointAddressSpinBox->setDisplayIntegerBase(16);
    breakPointAddressSpinBox->setMaximum(65535);

    addBreakPointButton = new QPushButton(/*mainVerticalLayoutWidget_3*/);
    addBreakPointButton->setObjectName("addBreakPointButton");

    removeBreakPointButton = new QPushButton(/*mainVerticalLayoutWidget_3*/);
    removeBreakPointButton->setObjectName("removeBreakPointButton");

    breakPointListWidget = new QListWidget(/*mainVerticalLayoutWidget_3*/);
    breakPointListWidget->setObjectName("breakPointListWidget");

    breakpointVerticalLayout->addWidget(breakPointLabel);
    breakpointVerticalLayout->addWidget(breakPointAddressSpinBox);
    breakpointVerticalLayout->addWidget(addBreakPointButton);
    breakpointVerticalLayout->addWidget(removeBreakPointButton);
    breakpointVerticalLayout->addWidget(breakPointListWidget);

    tabWidget->addTab(breakpointTab, QString());

    bodyHorizontalLayout->addWidget(tabWidget);
    bodyHorizontalLayout->setStretch(0, 1);
    bodyHorizontalLayout->setStretch(1, 1);

    mainVerticalLayout->addLayout(bodyHorizontalLayout);

    // Pause/Continue button
    continueButton = new QPushButton(/*mainVerticalLayoutWidget*/);
    continueButton->setObjectName("continueButton");

    mainVerticalLayout->addWidget(continueButton);

    retranslateUi();

    tabWidget->setCurrentIndex(0);

    gameStopped();
} // setupUi

void DebuggerWindow::retranslateUi()
{
    setWindowTitle(QCoreApplication::translate("Debugger", "Debugger", nullptr));

    // Tool bar
    nextInstructionButton->setText(
        QCoreApplication::translate("Debugger", "Next Instruction", nullptr));
    nextFrameButton->setText(QCoreApplication::translate("Debugger", "Next Frame", nullptr));
    runOneSecondButton->setText(QCoreApplication::translate("Debugger", "Run 1 Second", nullptr));
    resetButton->setText(QCoreApplication::translate("Debugger", "Reset", nullptr));

    // Disassembler tab
    disassemblerGroupBox->setTitle(
        QCoreApplication::translate("Debugger", "Disassembler", nullptr));
    disassemblerTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Address", nullptr));
    disassemblerTableWidget->horizontalHeaderItem(1)->setText(
        QCoreApplication::translate("Debugger", "Instruction", nullptr));
    disassemblerTableWidget->horizontalHeaderItem(2)->setText(
        QCoreApplication::translate("Debugger", "Data", nullptr));

    // Memory tab
    memoryStartAddressLabel->setText(QCoreApplication::translate("Debugger", "Offset", nullptr));
    loadMemoryButton->setText(QCoreApplication::translate("Debugger", "Load", nullptr));
    memoryTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Address", nullptr));
    tabWidget->setTabText(
        tabWidget->indexOf(memoryTab), QCoreApplication::translate("Debugger", "Memory", nullptr));

    otherRegisterTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Value", nullptr));

    // Registers tab
    otherRegisterLabel->setText(QCoreApplication::translate("Debugger", "CPU Register", nullptr));
    registerTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Value", nullptr));
    CPURegisterLabel->setText(QCoreApplication::translate("Debugger", "Other Register", nullptr));
    audioRegisterLabel->setText(QCoreApplication::translate("Debugger", "Audio Register", nullptr));
    audioRegisterTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Value", nullptr));
    tabWidget->setTabText(tabWidget->indexOf(registerTab),
        QCoreApplication::translate("Debugger", "Registers", nullptr));
    videoRegisterTableWidget->horizontalHeaderItem(0)->setText(
        QCoreApplication::translate("Debugger", "Value", nullptr));
    tabWidget->setTabText(tabWidget->indexOf(videoRegisterTab),
        QCoreApplication::translate("Debugger", "Video Registers", nullptr));

    // Breakpoint tab
    breakPointLabel->setText(
        QCoreApplication::translate("Debugger", "Break Point Address", nullptr));
    addBreakPointButton->setText(QCoreApplication::translate("Debugger", "Add", nullptr));
    removeBreakPointButton->setText(QCoreApplication::translate("Debugger", "Remove", nullptr));
    tabWidget->setTabText(tabWidget->indexOf(breakpointTab),
        QCoreApplication::translate("Debugger", "Break Point", nullptr));
} // retranslateUi

void DebuggerWindow::setupEvents()
{
    connect(continueButton, SIGNAL(clicked()), this, SLOT(continueButtonClicked()));
    connect(nextInstructionButton, SIGNAL(clicked()), this, SLOT(nextInstructionButtonClicked()));
    connect(nextFrameButton, SIGNAL(clicked()), this, SLOT(nextFrameButtonClicked()));
    connect(runOneSecondButton, SIGNAL(clicked()), this, SLOT(runOneSecondButtonClicked()));
    connect(resetButton, SIGNAL(clicked()), this, SLOT(resetButtonClicked()));

    connect(loadMemoryButton, SIGNAL(clicked()), this, SLOT(loadMemoryButtonClicked()));
    connect(memoryImage, SIGNAL(areaClicked(Reg16)), this, SLOT(changeAddress(Reg16)));
    connect(memoryTableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(editMemoryCell(int, int)));

    connect(addBreakPointButton, SIGNAL(clicked()), this, SLOT(addBreakPointButtonClicked()));
    connect(removeBreakPointButton, SIGNAL(clicked()), this, SLOT(removeBreakPointButtonClicked()));
}

// #include <QMessageBox>
// QMessageBox::information(this, "Not Implemented", "This feature is not implemented yet.");

void DebuggerWindow::changeAddress(Reg16 address)
{
    memoryStartAddressSpinBox->setValue(address);
    updateMemory();
}

void DebuggerWindow::continueButtonClicked()
{
    if (gameboyWindow->getIsRunning())
        emit requestStop();
    else
        emit requestStart();
}

void DebuggerWindow::nextFrameButtonClicked()
{
    gameboyWindow->getGameboy()->next_frame();
    emit requestScreenUpdate();
    updateDebugInfo();
}

void DebuggerWindow::nextInstructionButtonClicked()
{
    gameboyWindow->getGameboy()->next_it();
    if (gameboyWindow->getGameboy()->can_update_image())
    {
        emit requestScreenUpdate();
        gameboyWindow->getGameboy()->image_updated();
    }
    updateDebugInfo();
}

void DebuggerWindow::runOneSecondButtonClicked()
{
    gameboyWindow->getGameboy()->run_multiple_cycle(1048576 * ONE_CYCLE); // 1 second
    if (gameboyWindow->getGameboy()->can_update_image())
    {
        emit requestScreenUpdate();
        gameboyWindow->getGameboy()->image_updated();
    }
    updateDebugInfo();
}

void DebuggerWindow::resetButtonClicked()
{
    gameboyWindow->getGameboy()->reset_game();
    updateDebugInfo();
}

void DebuggerWindow::loadMemoryButtonClicked() { updateMemory(); }

void DebuggerWindow::addBreakPointButtonClicked()
{
    if (gameboyWindow->getGameboy()->breakpoint_add(breakPointAddressSpinBox->value()))
        breakPointListWidget->addItem(
            QString("0x%1").arg(breakPointAddressSpinBox->value(), 4, 16, QChar('0')).toUpper());
}

void DebuggerWindow::removeBreakPointButtonClicked()
{
    QList<QListWidgetItem *> items = breakPointListWidget->selectedItems();
    for (int i = 0; i < items.size(); i++)
    {
        gameboyWindow->getGameboy()->breakpoint_remove(
            items[i]->text().remove(0, 2).toUInt(nullptr, 16));
        breakPointListWidget->removeItemWidget(items[i]);
        delete items[i];
    }
}

void DebuggerWindow::closeEvent(QCloseEvent *event)
{
    (void)event;
    emit requestStart();
}

void DebuggerWindow::screenUpdated() { updateMemoryImage(memoryStartAddressSpinBox->value()); }

void DebuggerWindow::gameRunning()
{
    continueButton->setText(tr("Pause"));
    // Disable step running
    nextFrameButton->setEnabled(false);
    nextInstructionButton->setEnabled(false);
    runOneSecondButton->setEnabled(false);
}

void DebuggerWindow::gameStopped()
{
    continueButton->setText(tr("Continue"));
    // Enable step running
    nextFrameButton->setEnabled(true);
    nextInstructionButton->setEnabled(true);
    runOneSecondButton->setEnabled(true);

    updateDebugInfo();
}

void DebuggerWindow::editMemoryCell(int row, int column)
{
    Reg16 startAddress = memoryStartAddressSpinBox->value();

    Reg16 address = startAddress + row * 16 + (column - 1);
    Reg8 value = memoryTableWidget->item(row, column)->text().toUInt(nullptr, 16);

    memoryTableWidget->blockSignals(true);
    memoryTableWidget->item(row, column)->setText(toHex("%1", value, 2));
    memoryTableWidget->blockSignals(false);

    gameboyWindow->getGameboy()->mem_store(address, value);
    updateMemoryImage(startAddress);
}

QString DebuggerWindow::toHex(QString format, uint64_t value, int width)
{
    return QString(format).arg(value, width, 16, QChar('0')).toUpper();
}

QString DebuggerWindow::getRegisterCellText(CpuRegister reg)
{
    return toHex("0x%1", gameboyWindow->getGameboy()->cpu_load_register(reg), 4);
}

QString DebuggerWindow::getMemoryCellText(Reg16 address, int width, QString format)
{
    Reg8 value;

    gameboyWindow->getGameboy()->mem_load(address, value);
    return toHex(format, value, width);
}

void DebuggerWindow::updateDebugInfo()
{
    updateMemory();
    updateRegisters();
    updateDisassembly();
}

void DebuggerWindow::updateMemory()
{
    Reg16 startAddress = memoryStartAddressSpinBox->value();

    memoryTableWidget->blockSignals(true);
    for (int i = 0; i < 16; i++)
    {
        memoryTableWidget->item(i, 0)->setText(toHex("0x%1", Reg16(startAddress + i * 16), 4));

        for (int j = 1; j < 17; j++)
        {
            memoryTableWidget->item(i, j)->setText(
                getMemoryCellText(startAddress + i * 16 + j - 1, 2, "%1"));
        }
    }
    memoryTableWidget->blockSignals(false);

    updateMemoryImage(startAddress);
}

void DebuggerWindow::updateMemoryImage(Reg16 startAddress)
{
    memoryImage->update(gameboyWindow->getGameboy(), startAddress);
}

void DebuggerWindow::updateRegisters()
{
    // Main CPU Registers
    for (int i = 0; registerList[i].name; i++)
    {
        registerTableWidget->item(i, 0)->setText(
            getRegisterCellText((CpuRegister)registerList[i].address));
    }

    // Other Registers
    for (int i = 0; otherRegisterList[i].name; i++)
    {
        otherRegisterTableWidget->item(i, 0)->setText(
            getMemoryCellText(otherRegisterList[i].address));
    }

    // Audio Registers
    for (int i = 0; audioRegisterList[i].name; i++)
    {
        audioRegisterTableWidget->item(i, 0)->setText(
            getMemoryCellText(audioRegisterList[i].address));
    }

    // Video Registers
    for (int i = 0; videoRegisterList[i].name; i++)
    {
        videoRegisterTableWidget->item(i, 0)->setText(
            getMemoryCellText(videoRegisterList[i].address));
    }
}

void DebuggerWindow::updateDisassembly()
{
    uint16_t address = gameboyWindow->getGameboy()->cpu_load_register(PC);
    std::string instructionString;

    for (int i = 0; i < 16; i++)
    {
        Reg16 instructionSize = 0;
        instructionString.clear();

        instructionSize =
            gameboyWindow->getGameboy()->disassemble_instruction(address, instructionString);

        disassemblerTableWidget->item(i, 0)->setText(toHex("0x%1", address, 4));
        disassemblerTableWidget->item(i, 1)->setText(QString::fromStdString(instructionString));

        // Data
        QString dataString = QString("0x");

        for (int j = 0; j < instructionSize; j++)
        {
            Reg8 data;

            gameboyWindow->getGameboy()->mem_load(address + j, data);
            dataString.append(toHex(" %1", data, 2));
        }

        disassemblerTableWidget->item(i, 2)->setText(dataString);

        address += instructionSize;
    }
}
