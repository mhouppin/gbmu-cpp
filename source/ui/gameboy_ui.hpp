#pragma once

#include "debugger_ui.hpp"
#include "gameboy.h"
#include <QAction>
#include <QFileInfo>
#include <QKeyEvent>
#include <QWidget>
#include <QtCore/QVariant>
#include <QtGui/QPixmap>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

class DebuggerWindow;
class QGraphicsScene;
class QSplitter;

class GameBoyWindow : public QMainWindow
{
    Q_OBJECT

    static const int STATE_SLOTS_COUNTS = 10;

  private:
    // Application
    QApplication *app;

    // User Interface
    QWidget *centralwidget;
    QVBoxLayout *mainVerticalLayout;
    QLabel *screen;
    QHBoxLayout *toolbarHorizontalLayout;
    QLabel *romNameLabel;
    QPushButton *startPauseButton;
    QPushButton *saveButton;
    QPushButton *debuggerButton;
    QPushButton *hideGamePadButton;
    QWidget *gamePadWidget;
    QGridLayout *gamePadGridLayout;
    QGridLayout *ABGridLayout;
    QPushButton *B_Button;
    QPushButton *A_Button;
    QLabel *padCrossLabel; // Pad number
    QPushButton *rightButton;
    QPushButton *upButton;
    QPushButton *leftButton;
    QPushButton *downButton;
    QPushButton *selectButton;
    QPushButton *startButton;

    QStatusBar *statusbar;
    QMenuBar *menubar;
    QMenu *menuGBmu;
    QMenu *menuState;
    QMenu *menuHelp;
    QMenu *menuEdit;
    QMenu *menuSpeed;
    QMenu *menuEmulationMode;

    QAction *actionOpenRom;
    QAction *actionSaveGame;
    QAction *actionAbout;
    QAction *actionAboutQT;
    QAction *actionHalfSpeed;
    QAction *actionNormalSpeed;
    QAction *actionDoubleSpeed;
    QAction *actionQuadSpeed;
    QAction *actionDMG;
    QAction *actionCGB;
    QAction *actionDarkMode;
    QAction *actionLoadState;
    QAction *actionSaveState;
    QAction *actionStateSlot[STATE_SLOTS_COUNTS];

    // Translation
    QString hideGamePadText;

    // Debugger
    DebuggerWindow *debuggerWindow = nullptr;

    // Graphics
    bool isFullScreen = false;

    // Emulator
    Gameboy *gameboy = nullptr;
    QImage *screenBuffer;

    // Timer
    bool isRunning = true;
    int timerId = 0;
    int timerInterval = 17; // 60 fps

  public:
    GameBoyWindow(QApplication *app, QWidget *parent = nullptr);
    ~GameBoyWindow();

  private:
    void setupUi();
    void retranslateUi();
    void setupEvents();

  protected:
    void openRomFile(const QString &fileName);

    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void timerEvent(QTimerEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

  private:
    int getSelectedSlot();
    void refreshStartPauseButton();

  public:
  signals:
    void screenUpdated();
    void start();
    void stop();

  private slots:
    void updateScreen();
    void requestStart();
    void requestStop();

    void openRomFileTriggered();
    void loadStateTriggered();
    void saveStateTriggered();
    void saveGameTriggered();
    void aboutTriggered();
    void aboutQtTriggered();
    void setSpeed(int speed);
    void speedHalfSpeedTriggered();
    void speedNormalTriggered();
    void speedDoubleTriggered();
    void speedQuadTriggered();
    void emulationModeDMGTriggered();
    void emulationModeCGBTriggered();
    void darkModeToggled(bool checked);

    void saveButtonClicked();
    void startPauseButtonClicked();
    void debuggerButtonClicked();
    void hideGamePadButtonClicked();

    void A_ButtonPressed();
    void B_ButtonPressed();

    void selectButtonPressed();
    void startButtonPressed();

    void upButtonPressed();
    void downButtonPressed();
    void rightButtonPressed();
    void leftButtonPressed();

    void A_ButtonReleased();
    void B_ButtonReleased();

    void selectButtonReleased();
    void startButtonReleased();

    void upButtonReleased();
    void downButtonReleased();
    void rightButtonReleased();
    void leftButtonReleased();

  public:
    Gameboy *getGameboy() { return gameboy; };
    bool getIsRunning() { return isRunning; };
};
