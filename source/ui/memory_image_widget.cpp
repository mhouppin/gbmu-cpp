#include "memory_image_widget.hpp"

MemoryImageWidget::MemoryImageWidget(/* args */)
{
    memoryImageBuffer = new QImage(273, 273, QImage::Format_ARGB32);
    setObjectName("memoryImage");
    setStyleSheet("background-color: rgb(0, 0, 0);");
    setAlignment(Qt::AlignCenter);
}

MemoryImageWidget::~MemoryImageWidget() { delete memoryImageBuffer; }

void MemoryImageWidget::mousePressEvent(QMouseEvent *mouseEvent)
{
    QPoint mousePosition = mouseEvent->pos();

    int width = this->pixmap(Qt::ReturnByValue).rect().width();
    int height = this->pixmap(Qt::ReturnByValue).rect().height();
    int x = (this->width() - width) / 2;

    if (mousePosition.x() <= x + width && mousePosition.x() > x)
    {
        size_t areaX = (mousePosition.x() - x) * 273 / width / 17;
        size_t areaY = mousePosition.y() * 273 / height / 17;

        size_t address = areaX * 16 * 16 + areaY * 16 * 16 * 16;

        emit areaClicked(address);
    }
}

void MemoryImageWidget::createImage(Gameboy *gameboy, Reg16 showAddress)
{
    int32_t *grid = (int32_t *)this->memoryImageBuffer->bits();
    Reg8 value;

    for (Reg16 chunk = 0; chunk < 256; ++chunk)
    {
        size_t chunkX = (chunk % 16) * 17 + 1;
        size_t chunkY = (chunk / 16) * 17 + 1;

        for (Reg16 offset = 0; offset < 256; ++offset)
        {
            size_t x = chunkX + (offset % 16);
            size_t y = chunkY + (offset / 16);

            gameboy->mem_load(chunk * 256 + offset, value);
            grid[y * 273 + x] = colorTable[value];
        }
    }

    for (size_t gridLine = 0; gridLine < 17; ++gridLine)
        for (size_t x = 0; x < 273; ++x) grid[(gridLine * 17) * 273 + x] = 0xFFFFFFu;

    for (size_t gridColumn = 0; gridColumn < 17; ++gridColumn)
        for (size_t y = 0; y < 273; ++y) grid[y * 273 + gridColumn * 17] = 0xFFFFFFu;

    size_t chunkX = (showAddress / 256) % 16;
    size_t chunkY = (showAddress / 256) / 16;

    size_t offsetX = chunkX * 17;
    size_t offsetY = chunkY * 17;

    for (size_t x = offsetX; x < offsetX + 18; ++x)
        for (size_t y = offsetY; y < offsetY + 18; ++y)
            if (x == offsetX || x == offsetX + 17 || y == offsetY || y == offsetY + 17)
                grid[y * 273 + x] = 0xFF0000u;

    // Fix rgb order + alpha for Qt
    for (size_t i = 0; i < 273 * 273; ++i) grid[i] |= 0xFF000000u;
}

void MemoryImageWidget::update(Gameboy *gameboy, Reg16 showAddress)
{
    int w = width();
    int h = height();

    createImage(gameboy, showAddress);
    QPixmap pixmap = QPixmap::fromImage(*memoryImageBuffer);
    pixmap = pixmap.scaled(w, h, Qt::KeepAspectRatio);

    setPixmap(pixmap);
}