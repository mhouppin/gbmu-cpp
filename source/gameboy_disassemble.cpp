#include "gameboy.h"

const char *RegEncoding[8] = {"B", "C", "D", "E", "H", "L", "[HL]", "A"};

const char *RegPairEncoding[4] = {"BC", "DE", "HL", "SP"};

const char *RegPairEncoding2[4] = {"BC", "DE", "HL", "AF"};

const char *RegAddrEncoding[4] = {"[BC]", "[DE]", "[HL+]", "[HL-]"};

const char *AccOpEncoding[8] = {"ADD", "ADC", "SUB", "SBC", "AND", "XOR", "OR", "CMP"};

const char *BitOpEncoding[8] = {"RLC", "RRC", "RL", "RR", "SLA", "SRA", "SWAP", "SRL"};

const char *LargeBitOpEncoding[3] = {"BIT", "RES", "SET"};

const char *ConditionEncoding[4] = {"NZ", "Z", "NC", "C"};

const char *AccManipEncoding[8] = {"RLCA", "RRCA", "RLA", "RRA", "DAA", "CPL", "SCF", "CCF"};

inline char hexchar(Reg8 digit)
{
    return (digit < 10) ? '0' + char(digit) : 'a' + char(digit - 10);
}

const char *encode_hex_value(Reg16 val, bool isWord)
{
    static char buf[7] = "0x";

    if (isWord)
    {
        buf[2] = hexchar(val >> 12);
        buf[3] = hexchar((val >> 8) & 0xFu);
        buf[4] = hexchar((val >> 4) & 0xFu);
        buf[5] = hexchar(val & 0xFu);
        buf[6] = '\0';
    }
    else
    {
        buf[2] = hexchar(val >> 4);
        buf[3] = hexchar(val & 0xFu);
        buf[4] = '\0';
    }

    return buf;
}

Reg16 Gameboy::disassemble_top(
    Reg16 addr, Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string &insnString)
{
    switch (opcode & 0xFu)
    {
        case 0x1u:
            insnString = "MOV ";
            insnString += RegPairEncoding[opcode >> 4];
            insnString += ", ";
            insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
            return 3;

        case 0x2u:
            insnString = "MOV ";
            insnString += RegAddrEncoding[opcode >> 4];
            insnString += ", A";
            return 1;

        case 0x3u:
            insnString = "INC ";
            insnString += RegPairEncoding[opcode >> 4];
            return 1;

        case 0x4u:
        case 0xCu:
            insnString = "INC ";
            insnString += RegEncoding[opcode >> 3];
            return 1;

        case 0x5u:
        case 0xDu:
            insnString = "DEC ";
            insnString += RegEncoding[opcode >> 3];
            return 1;

        case 0x6u:
        case 0xEu:
            insnString = "MOV ";
            insnString += RegEncoding[opcode >> 3];
            insnString += ", ";
            insnString += encode_hex_value(imm8, false);
            return 2;

        case 0x7u:
        case 0xFu: insnString = AccManipEncoding[opcode >> 3]; return 1;

        case 0x9u:
            insnString = "ADD HL, ";
            insnString += RegPairEncoding[opcode >> 4];
            return 1;

        case 0xAu:
            insnString = "MOV A, ";
            insnString += RegAddrEncoding[opcode >> 4];
            return 1;

        case 0xBu:
            insnString = "DEC ";
            insnString += RegPairEncoding[opcode >> 4];
            return 1;
    }

    if (opcode == 0x00u)
    {
        insnString = "NOP";
        return 1;
    }

    if (opcode == 0x08u)
    {
        insnString = "MOV [";
        insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
        insnString += "], SP";
        return 3;
    }

    if (opcode == 0x10u)
    {
        insnString = "STOP";
        return 1;
    }

    if (opcode == 0x18u)
    {
        insnString = "JR ";
        insnString += encode_hex_value(addr + Reg16(int16_t(int8_t(imm8))), true);
        return 2;
    }

    insnString = "JR";
    insnString += ConditionEncoding[(opcode >> 3) - 4];
    insnString += ' ';
    insnString += encode_hex_value(addr + Reg16(int16_t(int8_t(imm8))), true);
    return 2;
}

Reg16 Gameboy::disassemble_load(Reg8 opcode, std::string &insnString)
{
    if (opcode == 0x76u)
        insnString = "HALT";

    else
    {
        insnString = "MOV ";
        insnString += RegEncoding[(opcode >> 3) - 8];
        insnString += ", ";
        insnString += RegEncoding[opcode & 7];
    }

    return 1;
}

Reg16 Gameboy::disassemble_acc(Reg8 opcode, std::string &insnString)
{
    insnString = AccOpEncoding[(opcode >> 3) - 16];
    insnString += " A, ";
    insnString += RegEncoding[opcode & 7];

    return 1;
}

Reg16 Gameboy::disassemble_cb(Reg8 opcodeCb, std::string &insnString)
{
    if (opcodeCb < 0x40u)
    {
        insnString = BitOpEncoding[opcodeCb >> 3];
        insnString += ' ';
        insnString += RegEncoding[opcodeCb & 7];
    }
    else
    {
        insnString = LargeBitOpEncoding[(opcodeCb >> 6) - 1];
        insnString += ' ';
        insnString += '0' + char((opcodeCb >> 3) & 7);
        insnString += ", ";
        insnString += RegEncoding[opcodeCb & 7];
    }

    return 2;
}

Reg16 Gameboy::disassemble_opcode_cd(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string &insnString)
{
    switch (opcode & 0xFu)
    {
        case 0x0u:
        case 0x8u:
            insnString = "RET";
            insnString += ConditionEncoding[(opcode >> 3) - 24];
            return 1;

        case 0x2u:
        case 0xAu:
            insnString = "J";
            insnString += ConditionEncoding[(opcode >> 3) - 24];
            insnString += ' ';
            insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
            return 3;

        case 0x4u:
        case 0xCu:
            insnString = "CALL";
            insnString += ConditionEncoding[(opcode >> 3) - 24];
            insnString += ' ';
            insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
            return 3;
    }

    if (opcode == 0xC3u)
    {
        insnString = "JMP ";
        insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
        return 3;
    }

    if (opcode == 0xC9u)
    {
        insnString = "RET";
        return 1;
    }

    if (opcode == 0xCDu)
    {
        insnString = "CALL ";
        insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
        return 3;
    }

    if (opcode == 0xD9u)
    {
        insnString = "RETI";
        return 1;
    }

    insnString = "ILL";
    return 1;
}

Reg16 Gameboy::disassemble_opcode_ef(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string &insnString)
{
    switch (opcode)
    {
        case 0xE0u:
            insnString = "MOVH [";
            insnString += encode_hex_value(imm8, false);
            insnString += "], A";
            return 2;

        case 0xE2u: insnString = "MOVH [C], A"; return 1;

        case 0xE8u:
            insnString = "ADD SP, ";
            insnString += std::to_string(int8_t(imm8));
            return 2;

        case 0xE9u: insnString = "JMP *HL"; return 1;

        case 0xEAu:
            insnString = "MOV [";
            insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
            insnString += "], A";
            return 3;

        case 0xF0u:
            insnString = "MOVH A, [";
            insnString += encode_hex_value(imm8, false);
            insnString += ']';
            return 2;

        case 0xF2u: insnString = "MOVH A, [C]"; return 1;

        case 0xF3u: insnString = "DI"; return 1;

        case 0xF8u:
            insnString = "MOV HL, SP ";
            if (imm8 >= 0x80)
            {
                insnString += '-';
                imm8 = ~imm8 + 1;
            }
            else
                insnString += '+';

            insnString + ' ';
            insnString += std::to_string(imm8);
            return 2;

        case 0xF9u: insnString = "MOV SP, HL"; return 1;

        case 0xFAu:
            insnString = "MOV A, [";
            insnString += encode_hex_value(make_reg16(imm8h, imm8), true);
            insnString += ']';
            return 3;

        case 0xFBu: insnString = "EI"; return 1;
    }

    insnString = "ILL";
    return 1;
}

Reg16 Gameboy::disassemble_bot(Reg8 opcode, Reg8 imm8, Reg8 imm8h, std::string &insnString)
{
    switch (opcode & 0xFu)
    {
        case 0x1u:
            insnString = "POP ";
            insnString += RegPairEncoding2[(opcode >> 4) - 12];
            return 1;

        case 0x5u:
            insnString = "PUSH ";
            insnString += RegPairEncoding2[(opcode >> 4) - 12];
            return 1;

        case 0x6u:
        case 0xEu:
            insnString = AccOpEncoding[(opcode >> 3) - 24];
            insnString += " A, ";
            insnString += encode_hex_value(imm8, false);
            return 2;

        case 0x7u:
        case 0xFu:
            insnString = "RST ";
            insnString += encode_hex_value(opcode - 0xC7u, false);
            return 1;
    }

    if (opcode < 0xE0)
        return disassemble_opcode_cd(opcode, imm8, imm8h, insnString);
    else
        return disassemble_opcode_ef(opcode, imm8, imm8h, insnString);
}

Reg16 Gameboy::disassemble_instruction(Reg16 addr, std::string &insnString)
{
    Reg8 opcode, imm8, imm8h;

    map->load(addr + 0, opcode);
    map->load(addr + 1, imm8);
    map->load(addr + 2, imm8h);

    insnString.clear();

    if (opcode < 0x40u)
        return disassemble_top(addr, opcode, imm8, imm8h, insnString);

    else if (opcode < 0x80u)
        return disassemble_load(opcode, insnString);

    else if (opcode < 0xC0u)
        return disassemble_acc(opcode, insnString);

    else if (opcode == 0xCBu)
        return disassemble_cb(imm8, insnString);

    else
        return disassemble_bot(opcode, imm8, imm8h, insnString);
}
