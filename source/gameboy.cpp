#include "gameboy.h"
#include "sound.h"
#include <cctype>
#include <fstream>
#include <iostream>

bool Gameboy::prioritizeCgb = true;

Gameboy::Gameboy(void)
{
    cpu = nullptr;
    map = nullptr;
    screen = nullptr;
    event = nullptr;
    apu = nullptr;
    error = NO_RUN_ERROR;
    status = STATUS_NORMAL;
    gameLoaded = false;
    gamePath = "";
    gameLoadError = "";
    breakpointReach = false;
    breakpointList = std::set<Reg16>();
    saveStates.resize(9);
}

Gameboy::~Gameboy(void) { close_game(); }

void Gameboy::close_game(void)
{
    if (gameLoaded) write_game_save();

    gameLoaded = false;
    gamePath = "";
    gameLoadError = "";

    if (map != nullptr)
    {
        delete map;
        map = nullptr;
    }
    if (screen != nullptr)
    {
        delete screen;
        screen = nullptr;
    }
    if (cpu != nullptr)
    {
        delete cpu;
        cpu = nullptr;
    }
    if (event != nullptr)
    {
        delete event;
        event = nullptr;
    }
    if (apu != nullptr)
    {
        delete apu;
        apu = nullptr;
    }
}

void Gameboy::open_game(const std::string& filename, int32_t* windowPixels)
{
    static std::string NintendoLogo("\xCE\xED\x66\x66\xCC\x0D\x00\x0B\x03\x73\x00\x83"
                                    "\x00\x0C\x00\x0D\x00\x08\x11\x1F\x88\x89\x00\x0E"
                                    "\xDC\xCC\x6E\xE6\xDD\xDD\xD9\x99\xBB\xBB\x67\x63"
                                    "\x6E\x0E\xEC\xCC\xDD\xDC\x99\x9F\xBB\xB9\x33\x3E",
        0x30);

    close_game();

    std::ifstream file(filename, std::ios_base::binary);
    std::vector<char> content;

    file.seekg(0, std::ios_base::end);
    size_t length = file.tellg();
    file.seekg(0, std::ios_base::beg);

    if (length < 4096)
    {
        gameLoadError = "Unable to load game: Contents too small";
        return;
    }

    content.reserve(length);
    std::copy(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(),
        std::back_inserter(content));

    // Check if the game does a nop + absolute jump at startup

    if (content[0x100] != '\0' && content[0x101] != '\xC3')
    {
        gameLoadError = "GB Error: Invalid jump address";
        return;
    }

    // Check if the Nintendo Logo is present

    for (size_t i = 0; i < 0x30; ++i)
        if (content[0x104 + i] != NintendoLogo[i])
        {
            gameLoadError = "GB Error: Missing Nintendo Logo";
            return;
        }

    // Check if the game title doesn't contain unallowed characters

    cartInfo.gameTitle.assign(content.begin() + 0x134, content.begin() + 0x13F);
    for (unsigned int i = 0; i < 11; ++i)
    {
        char c = cartInfo.gameTitle[i];

        if (c && (c < 0x20 || c > 0x5F))
        {
            gameLoadError = "GB Error: wrongly formatted game title";
            return;
        }
    }

    // Check if the game code doesn't contain unallowed characters

    cartInfo.gameCode.assign(content.begin() + 0x13F, content.begin() + 0x143);
    for (unsigned int i = 0; i < 4; ++i)
    {
        char c = cartInfo.gameCode[i];

        if (c && !isupper(c) && !isdigit(c))
        {
            gameLoadError = "GB Error: wrongly formatted game code";
            return;
        }
    }

    // Check if the game is a CGB or DMG one, and select the running mode based
    // on the support code and the prio flag

    if (content[0x143] == '\x00')
    {
        cartInfo.suppCode = CGB_INCOMPATIBLE;
        cartInfo.mode = DMG_MODE;
    }
    else if (content[0x143] == '\x80')
    {
        cartInfo.suppCode = CGB_COMPATIBLE;
        cartInfo.mode = prioritizeCgb ? CGB_MODE : DMG_MODE;
    }
    else if (content[0x143] == '\xC0')
    {
        cartInfo.suppCode = CGB_EXCLUSIVE;
        cartInfo.mode = CGB_MODE;
    }
    else
    {
        gameLoadError = "GB Error: invalid CGB support code";
        return;
    }

    // Check if SGB support code are valid

    if (content[0x146] != '\x00' && content[0x146] != '\x03')
    {
        gameLoadError = "GB Error: invalid SGB support code";
        return;
    }

    // Get the software type

    cartInfo.MBC = 0;
    cartInfo.hasRTC = false;
    cartInfo.hasSRAM = false;
    switch (content[0x147])
    {
        case 0x00: break;

        case 0x01: cartInfo.MBC = 1; break;

        case 0x02 ... 0x03:
            cartInfo.MBC = 1;
            cartInfo.hasSRAM = true;
            break;

        case 0x05 ... 0x06: cartInfo.MBC = 2; break;

        case 0x08 ... 0x09: cartInfo.hasSRAM = true; break;

        case 0x0F:
            cartInfo.MBC = 3;
            cartInfo.hasRTC = true;
            break;

        case 0x10:
            cartInfo.MBC = 3;
            cartInfo.hasRTC = true;
            cartInfo.hasSRAM = true;
            break;

        case 0x11: cartInfo.MBC = 3; break;

        case 0x12 ... 0x13:
            cartInfo.MBC = 3;
            cartInfo.hasSRAM = true;
            break;

        case 0x19: cartInfo.MBC = 5; break;

        case 0x1A ... 0x1B:
            cartInfo.MBC = 5;
            cartInfo.hasSRAM = true;
            break;

        default: gameLoadError = "GB Error: invalid software type"; return;
    }

    // Get the ROM size

    if (Reg8(content[0x148]) > 0x08)
    {
        gameLoadError = "GB Error: invalid ROM size";
        return;
    }
    cartInfo.sizeROM = 0x8000u << content[0x148];

    // Get the external RAM size
    switch (content[0x149])
    {
        case 0x00: cartInfo.sizeExternalRAM = 0; break;

        case 0x02: cartInfo.sizeExternalRAM = 0x2000u; break;

        case 0x03: cartInfo.sizeExternalRAM = 0x8000u; break;

        case 0x04: cartInfo.sizeExternalRAM = 0x20000u; break;

        default: gameLoadError = "GB Error: invalid external RAM size"; return;
    }

    // Check if the destination code is valid
    if (content[0x14A] != '\x00' && content[0x14A] != '\x01')
    {
        gameLoadError = "GB Error: invalid destination code";
        return;
    }

    // Verify the complement

    Reg8 sum = 0x19;
    for (unsigned int i = 0x134; i <= 0x14D; ++i) sum += Reg8(content[i]);
    if (sum != 0)
    {
        gameLoadError = "GB Error: invalid complement check";
        return;
    }

    if (cartInfo.sizeROM != uint32_t(content.size()))
    {
        gameLoadError = "GB Error: invalid ROM size";
        return;
    }

    // TODO: Reduce circular dependencies between MemoryMap and Event
    map = new MemoryMap(this);
    apu = new APU(map);
    event = new Event(this, map);
    map->load_cartridge(content);
    screen = new Screen(this, map, windowPixels);
    cpu = new CPU(cartInfo.mode == CGB_MODE);

    gameLoaded = true;
    gamePath = filename;
    clock = InternClock{};

    read_game_save();
}

void Gameboy::reset_game(void)
{
    cartInfo.mode = (cartInfo.suppCode == CGB_EXCLUSIVE)
                            || (cartInfo.suppCode == CGB_COMPATIBLE && prioritizeCgb)
                        ? CGB_MODE
                        : DMG_MODE;
    cpu->reset(cartInfo.mode == CGB_MODE);
    map->reset();
    screen->reset();
    error = NO_RUN_ERROR;
    status = STATUS_NORMAL;
    clock.divCycles = 0 * ONE_CYCLE;
    clock.timerCycles = 0 * ONE_CYCLE;

    for (auto& saveState : saveStates) saveState.inUse = false;
}

// TODO: code SRAM load + save functions

void Gameboy::read_game_save(void)
{
    std::string saveFilename = gamePath;
    saveFilename += ".sav";

    std::ifstream file(saveFilename, std::ios_base::binary);

    if (!file.is_open()) return;

    std::copy(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(),
        map->external_ram_begin());
}

void Gameboy::write_game_save(void)
{
    std::string saveFilename = gamePath;
    saveFilename += ".sav";

    std::ofstream file(saveFilename, std::ios_base::binary);

    std::copy(
        map->external_ram_begin(), map->external_ram_end(), std::ostreambuf_iterator<char>(file));
}

bool Gameboy::load_state(int stateIdx)
{
    if (stateIdx <= 0 || stateIdx >= 10) return false;

    const GameboyState& saveState = saveStates[stateIdx - 1];

    if (!saveState.inUse) return false;

    load_internal_state(saveState.internalState);
    cpu->load_state(saveState.cpuState);
    map->load_state(saveState.memoryState);
    apu->load_state(saveState.apuState);
    screen->load_state(saveState.screenState);

    return true;
}

bool Gameboy::save_state(int stateIdx)
{
    if (stateIdx <= 0 || stateIdx >= 10) return false;

    GameboyState& saveState = saveStates[stateIdx - 1];

    saveState.inUse = true;
    save_internal_state(saveState.internalState);
    cpu->save_state(saveState.cpuState);
    map->save_state(saveState.memoryState);
    apu->save_state(saveState.apuState);
    screen->save_state(saveState.screenState);

    return true;
}

void Gameboy::load_internal_state(const InternalState& internalState)
{
    error = internalState.error;
    status = internalState.status;
    clock = internalState.clock;
}

void Gameboy::save_internal_state(InternalState& internalState)
{
    internalState.error = error;
    internalState.status = status;
    internalState.clock = clock;
}

Cycle Gameboy::run_multiple_cycle(Cycle numerOfCycles)
{
    Cycle cycles = ZERO_CYCLE;

    while (cycles < numerOfCycles)
    {
        cycles += next_it();

        if (breakpointReach)
        {
            breakpointReach = false;
            break;
        }
    }

    return (cycles);
}

Cycle Gameboy::next_frame(void)
{
    Cycle cycles = ZERO_CYCLE;

    while (cycles < 19760 * ONE_CYCLE)
    {
        cycles += next_it();

        // Stop the emulator if breakpoint is reached
        if (breakpointReach)
        {
            breakpointReach = false;
            break;
        }

        if (can_update_image())
        {
            image_updated();
            break;
        }
    }

    return (cycles);
}

Cycle Gameboy::next_it(void)
{
    Reg8 key1Reg;

    if (breakpointList.find(cpu->PC) != breakpointList.end() && !breakpointReach)
    {
        breakpointReach = true;
        return ZERO_CYCLE;
    }

    Cycle insnCycles = (status == STATUS_NORMAL) ? execute_one_instruction() : ONE_CYCLE;
    Cycle realCycles = insnCycles;

    // Check if the game is running at double clock speed
    map->register_load(KEY1_ADDRESS, key1Reg);
    if (key1Reg & BIT_7) realCycles /= 2;

    screen->update(realCycles);
    if (status != STATUS_STOPPED) update_timers(insnCycles);
    // TODO: fix the call
    // update_audio(insnCycles);

    check_control_events(insnCycles);
    check_interrupts();
    return (realCycles);
}

void Gameboy::update_timers(Cycle elapsed)
{
    clock.divCycles += elapsed;

    if (clock.divCycles >= 64 * ONE_CYCLE)
    {
        Reg8 divReg;

        clock.divCycles -= 64 * ONE_CYCLE;
        map->register_load(DIV_ADDRESS, divReg);
        ++divReg;
        map->register_store(DIV_ADDRESS, divReg);

        Reg8 key1Reg;

        map->register_load(KEY1_ADDRESS, key1Reg);

        if (!(divReg & (!!(key1Reg & BIT_7) ? 0x3Fu : 0x1Fu))) apu->increase_div_apu();
    }

    Reg8 tacReg;

    map->register_load(TAC_ADDRESS, tacReg);

    if (tacReg & BIT_2)
    {
        Reg8 shift = ((tacReg - 1) & (BIT_0 | BIT_1)) << 1;

        clock.timerCycles += elapsed * ((65536 * ONE_CYCLE) >> shift);

        if (clock.timerCycles >= 1048576 * ONE_CYCLE)
        {
            Reg8 timaReg;

            clock.timerCycles -= 1048576 * ONE_CYCLE;
            map->register_load(TIMA_ADDRESS, timaReg);
            ++timaReg;

            if (timaReg == 0)
            {
                Reg8 tmaReg;

                map->register_load(TMA_ADDRESS, tmaReg);
                timaReg = tmaReg;
                map->set_interrupt(BIT_2);
            }

            map->register_store(TIMA_ADDRESS, timaReg);
        }
    }
}

void Gameboy::check_control_events(Cycle elapsed)
{
    clock.eventCycles += elapsed;

    // We check gameboy events about 100 times per second. This can be easily
    // adjusted by changing the constant here

    if (clock.eventCycles >= 10000 * ONE_CYCLE)
    {
        clock.eventCycles -= 10000 * ONE_CYCLE;

        if (status == STATUS_NORMAL)
            event->check_normal();
        else
            event->check_halted();
    }
}

void Gameboy::check_interrupts(void)
{
    if (map->IME == false && status == STATUS_NORMAL) return;

    Reg8 ifReg, ieReg;

    map->register_load(IF_ADDRESS, ifReg);
    map->register_load(IE_ADDRESS, ieReg);

    Reg8 enabled = ifReg & ieReg;

    if ((enabled & BIT_0) && status != STATUS_STOPPED)
        auto_sri(0x40u, BIT_0);

    else if ((enabled & BIT_1) && status != STATUS_STOPPED)
        auto_sri(0x48u, BIT_1);

    else if ((enabled & BIT_2) && status != STATUS_STOPPED)
        auto_sri(0x50u, BIT_2);

    else if (enabled & BIT_4)
        auto_sri(0x60u, BIT_4);
}

void Gameboy::auto_sri(Reg16 newPC, Reg8 ifMask)
{
    status = STATUS_NORMAL;

    if (map->IME == false) return;

    Reg8 ifReg;

    map->IME = false;
    map->register_load(IF_ADDRESS, ifReg);
    ifReg &= ~ifMask;
    map->register_store(IF_ADDRESS, ifReg);

    push_stack(reg16_hi(cpu->PC), reg16_lo(cpu->PC));
    cpu->PC = newPC;
}
